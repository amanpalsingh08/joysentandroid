package com.xperienceclientnurture.networks

import com.xperienceclientnurture.tools.GeneratedCodeConverters
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

class RetrofitApi {

    private val baseUrl get() = "https://xperienceclientnurture.com/api/v1/"

    private var loggingInterceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    fun <T> createService(service: Class<T>, token: String? = null): T {
        val okHttpClient: OkHttpClient = OkHttpClient().newBuilder().addInterceptor(loggingInterceptor).addInterceptor(Interceptor { chain ->
            val builder = chain.request().newBuilder()
            if (token != null) builder.header("Authorization", "Bearer $token")
            builder.header("Accept", "application/json")
            return@Interceptor chain.proceed(builder.build())
        }).connectTimeout(120, TimeUnit.SECONDS)
            .readTimeout(120, TimeUnit.SECONDS)
            .writeTimeout(90, TimeUnit.SECONDS).build()

        val retrofit = Retrofit.Builder().client(okHttpClient).addConverterFactory(GeneratedCodeConverters.converterFactory()).baseUrl(baseUrl).build()
        return retrofit.create(service)
    }

}