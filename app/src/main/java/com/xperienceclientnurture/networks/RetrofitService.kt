package com.xperienceclientnurture.networks

import com.xperienceclientnurture.ResponseModel
import com.xperienceclientnurture.ui.commonmodel.*
import com.xperienceclientnurture.ui.forgotpassword.ForgotDataModel
import com.xperienceclientnurture.ui.home.ui.addeditclient.ClientDetailsDataModel
import com.xperienceclientnurture.ui.home.ui.addeditclient.ClientRequestDataModel
import com.xperienceclientnurture.ui.home.ui.addeditclient.ClientsDataModel
import com.xperienceclientnurture.ui.home.ui.addeditclient.PaymentDetailsResponseModel
import com.xperienceclientnurture.ui.home.ui.cards.CardsDataModels
import com.xperienceclientnurture.ui.home.ui.clientconnection.GetClientConnectionDataModel
import com.xperienceclientnurture.ui.home.ui.clientnotes.ResultClientNotes
import com.xperienceclientnurture.ui.home.ui.delivered.DeliveredClientDataModel
import com.xperienceclientnurture.ui.home.ui.disabled.GetDisabledClientDataModel
import com.xperienceclientnurture.ui.home.ui.feedback.GetFeedbackDataModel
import com.xperienceclientnurture.ui.home.ui.gallery.GalleryDataModel
import com.xperienceclientnurture.ui.home.ui.gifts.GetPaymentsDataModel
import com.xperienceclientnurture.ui.home.ui.gifts.GiftsTypeDataModel
import com.xperienceclientnurture.ui.home.ui.gifts.anniversay.AnniversaryDetailDataModel
import com.xperienceclientnurture.ui.home.ui.gifts.anniversay.AnniversayDataModel
import com.xperienceclientnurture.ui.home.ui.gifts.anniversay.DateListDataModel
import com.xperienceclientnurture.ui.home.ui.importclients.ImportClientDataModel
import com.xperienceclientnurture.ui.home.ui.myaccount.GetUserDataModel
import com.xperienceclientnurture.ui.home.ui.myaccount.UpdateUserDataModel
import com.xperienceclientnurture.ui.home.ui.notifications.GetRequestReceivedDataModel
import com.xperienceclientnurture.ui.home.ui.notifications.GetRequestSendDataModel
import com.xperienceclientnurture.ui.home.ui.queueclients.GetQueueClientsDataModel
import com.xperienceclientnurture.ui.home.ui.recentclients.GetRecentClientsConnectionDataModel
import com.xperienceclientnurture.ui.home.ui.reminders.ResultReminders
import com.xperienceclientnurture.ui.home.ui.subscriptions.ResultActiveSubscriptions
import com.xperienceclientnurture.ui.login.LoginDataModel
import com.xperienceclientnurture.ui.register.RegisterDataModel
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface RetrofitService {
    @FormUrlEncoded
    @POST("register")
    suspend fun postRegister(@FieldMap params: HashMap<String, String>): Response<RegisterDataModel>

    @FormUrlEncoded
    @POST("login")
    suspend fun postLogin(@FieldMap params: HashMap<String, String>): Response<LoginDataModel>

    @FormUrlEncoded
    @POST("forgot-password")
    suspend fun postForgotPassword(@FieldMap params: HashMap<String, String>): Response<ForgotDataModel>

    @GET("get-user")
    suspend fun getUser(@Query("id") ID: String): Response<GetUserDataModel>

    @GET("get-state")
    suspend fun getState(): Response<GetStateDataModel>

    @Multipart
    @POST("update-user")
    suspend fun postUpdateUser(
        @PartMap userDetail: HashMap<String, RequestBody>,
        @Part profileImage: MultipartBody.Part
    ): Response<UpdateUserDataModel>

    @Headers("Content-Type: application/json")
    @Multipart
    @POST("UpdateUserProfile")
    suspend fun UpdateUserProfile(
        @PartMap userDetail: HashMap<String, RequestBody>,
        @Part profileImage: MultipartBody.Part
    ): Response<ResponseModel>

    @Multipart
    @POST("import-client")
    suspend fun importClients(
        @PartMap userDetail: HashMap<String, RequestBody>,
        @Part profileImage: MultipartBody.Part
    ): Response<ImportClientDataModel>

    @Multipart
    @POST("update-user")
    suspend fun postUpdateUserNoImage(@PartMap userDetail: HashMap<String, RequestBody>): Response<UpdateUserDataModel>

    @FormUrlEncoded
    @POST("update-password")
    suspend fun postUpdatePassword(@FieldMap params: HashMap<String, String>): Response<CommonDataModel>

    @Multipart
    @POST("add-client")
    suspend fun postAddClient(
        @PartMap params: HashMap<String, RequestBody>,
        @Part profileImage: MultipartBody.Part
    ): Response<CommonDataModel>

    @Multipart
    @POST("add-client")
    suspend fun postAddClientNoImage(@PartMap params: HashMap<String, RequestBody>): Response<CommonDataModel>

    @Multipart
    @POST("update-client")
    suspend fun postEditClient(
        @PartMap params: HashMap<String, RequestBody>,
        @Part profileImage: MultipartBody.Part
    ): Response<CommonDataModel>

    @Multipart
    @POST("update-client")
    suspend fun postEditClientNoImage(@PartMap params: HashMap<String, RequestBody>): Response<CommonDataModel>


    @GET("get-stage")
    suspend fun getStages(@FieldMap params: HashMap<String, String>): Response<StagesDataModel>

    @GET("get-lead-source")
    suspend fun getLeadSource(@FieldMap params: HashMap<String, String>): Response<LeadSourcesDataModel>

    @GET("get-client")
    suspend fun getClientDetails(@Query("client_id") ID: String): Response<ClientDetailsDataModel>


    @GET("get-clients")
    suspend fun getClients(@Query("user_id") ID: String): Response<ClientsDataModel>

    @GET("get-active-clients")
    suspend fun getActiveClients(@Query("user_id") ID: String): Response<ClientsDataModel>

    @GET("get-card")
    suspend fun getCards(@Query("user_id") ID: String): Response<CardsDataModels>

    @GET("get-client-request")
    suspend fun getClientRequest(@Query("client_id") ID: String): Response<ClientRequestDataModel>

    @GET("payment-detail")
    suspend fun getPaymentDetails(): Response<PaymentDetailsResponseModel>

    @GET("get-users")
    suspend fun getUsersConnection(): Response<GetUsersConnectionDataModel>

    @FormUrlEncoded
    @POST("add-edit-card")
    suspend fun postAddEditCard(@FieldMap params: HashMap<String, String>): Response<CommonDataModel>

    @FormUrlEncoded
    @POST("add-edit-feedback")
    suspend fun postAddEditFeedback(@FieldMap params: HashMap<String, String>): Response<CommonDataModel>

    @FormUrlEncoded
    @POST("request-connections")
    suspend fun postRequestConnection(@FieldMap params: HashMap<String, String>): Response<CommonDataModel>

    @GET("feedback-list")
    suspend fun getFeedbackList(@Query("user_id") ID: String): Response<GetFeedbackDataModel>

    @GET("delivered-client")
    suspend fun getDeliveredClients(@Query("user_id") ID: String): Response<DeliveredClientDataModel>

    @GET("delete-feedback")
    suspend fun deleteFeedback(@Query("feedback_id") ID: String): Response<CommonDataModel>

    @GET("get-gallery")
    suspend fun getGalleryList(): Response<GalleryDataModel>

    @GET("requests-send")
    suspend fun getRequestsSend(@Query("user_id") ID: String): Response<GetRequestSendDataModel>

    @GET("requests-received")
    suspend fun getRequestsReceived(@Query("user_id") ID: String): Response<GetRequestReceivedDataModel>

    @GET("client-connections")
    suspend fun getClientConnections(@Query("user_id") ID: String): Response<GetClientConnectionDataModel>

    @GET("subscribed-clients")
    suspend fun getSubscribedClients(@Query("user_id") ID: String): Response<CommonDataModel>

    @GET("onetime-paid")
    suspend fun getOneTimePaid(@Query("user_id") ID: String): Response<CommonDataModel>

    @GET("pay-all")
    suspend fun getPayAllRequest(
        @Query("request_id") ID: String,
        @Query("user_id") UserID: String
    ): Response<CommonDataModel>

    @GET("destroy-request")
    suspend fun getDestroyRequest(@Query("request_id") ID: String): Response<CommonDataModel>

    @GET("send-request")
    suspend fun getSentRequest(
        @Query("sender_id") senderID: String,
        @Query("receiver_id") receiverID: String,
        @Query("client_id") clientID: String
    ): Response<CommonDataModel>

    @GET("cancel-request")
    suspend fun getCancelRequest(
        @Query("user_id") userID: String,
        @Query("request_id") requestID: String
    ): Response<CommonDataModel>

    @GET("accept-request")
    suspend fun getAcceptRequest(
        @Query("user_id") UserID: String, @Query("request_id") requestID: String
    ): Response<CommonDataModel>

    @GET("disabled-clients")
    suspend fun getDisabledClients(@Query("user_id") UserID: String): Response<GetDisabledClientDataModel>

    @GET("queue-client")
    suspend fun getQueueClients(@Query("user_id") UserID: String): Response<GetQueueClientsDataModel>

    @GET("recent-connections")
    suspend fun getRecentClients(@Query("user_id") UserID: String): Response<GetRecentClientsConnectionDataModel>

    /**
     * user_id:3
    gifts:5
     */
    @FormUrlEncoded
    @POST("get-total")
    suspend fun postTotal(@FieldMap params: HashMap<String, String>): Response<GetPaymentsDataModel>


    /**
     * user_id:3
    gifts:5
     */
    @GET("get-years")
    suspend fun getYears(): Response<CommonDataModel>

    /**
     * user_id:3
    client_id:1
    frequency:M
    gifts:10
    total:267.5
    months:
     */
    @FormUrlEncoded
    @POST("pay-now")
    suspend fun postPayNow(@FieldMap params: HashMap<String, String>): Response<CommonDataModel>

    /**
     * user_id:3
    client_id:1
     */
    @FormUrlEncoded
    @POST("subscribe-payment")
    suspend fun postSubscribePayment(@FieldMap params: HashMap<String, String>): Response<CommonDataModel>


    @FormUrlEncoded
    @POST("request-connection1")
    suspend fun postRequestConnection1(@FieldMap params: HashMap<String, String>): Response<CommonDataModel>

    @FormUrlEncoded
    @POST("pay-100-percent")
    suspend fun pay100Percent(@FieldMap params: HashMap<String, String>): Response<CommonDataModel>


    @FormUrlEncoded
    @POST("request-connection2")
    suspend fun postRequestConnection2(@FieldMap params: HashMap<String, String>): Response<CommonDataModel>

    @FormUrlEncoded
    @POST("request-connection")
    suspend fun requestConnection(@FieldMap params: HashMap<String, String>): Response<CommonDataModel>

    @FormUrlEncoded
    @POST("request-connections")
    suspend fun requestConnections(@FieldMap params: HashMap<String, String>): Response<CommonDataModel>

    /**
     * user_id:3
    request_id:1
     */
    @FormUrlEncoded
    @POST("accept-sub-request")
    suspend fun postAcceptSubRequest(@FieldMap params: HashMap<String, String>): Response<CommonDataModel>


    @GET("get-gifts")
    suspend fun getAnniversaryList(@Query("gift_type_id") giftTypeId: String): Response<AnniversayDataModel>

    @GET("logout")
    suspend fun logout(@Query("user_id") userID: String): Response<CommonDataModel>

    @GET("get-dates")
    suspend fun getDateList(): Response<DateListDataModel>

    @GET("gift-shipping-price")
    suspend fun getAnniversaryDetail(
        @Query("gift_id") giftId: String,
        @Query("client_id") clientId: String,
        @Query("quantity") quantity: String,
        @Query("message") message: String
    ): Response<AnniversaryDetailDataModel>

    @GET("customer-gift-shipping-price")
    suspend fun getCustomerShippingPrice(
        @Query("gift_id") giftId: String,
        @Query("post_code") message: String
    ): Response<AnniversaryDetailDataModel>

    @FormUrlEncoded
    @POST("purchase-gift")
    suspend fun purchaseGift(@FieldMap params: HashMap<String, String>): Response<CommonDataModel>

    @GET("purchase-gift")
    suspend fun purchaseGiftDecor(
        @Query("gift_id") giftId: String,
        @Query("user_id") userID: String,
        @Query("client_id") clientId: String,
        @Query("quantity") quantity: String,
        @Query("message") message: String
    ): Response<CommonDataModel>

    @FormUrlEncoded
    @POST("customer-purchase-gift")
    suspend fun purchaseCustomer(@FieldMap params: HashMap<String, String>): Response<CommonDataModel>

    @GET("get-gift-types")
    suspend fun getGiftsType(): Response<GiftsTypeDataModel>

    @GET("notes")
    suspend fun getClientNotes(@Query("user_id") userID: String): ResultClientNotes

    @GET("delete-notes")
    suspend fun deleteClientNote(@Query("user_id") userID: String, @Query("note_id") noteID: String): ResultClientNotes

    @FormUrlEncoded
    @POST("add-note")
    suspend fun addNoteData(@FieldMap params: HashMap<String, String>): ResultClientNotes

    @FormUrlEncoded
    @POST("update-note")
    suspend fun updateNote(@FieldMap params: HashMap<String, String>): ResultClientNotes

    @GET("reminders")
    suspend fun getReminders(@Query("user_id") userID: String): ResultReminders

    @GET("delete-reminder")
    suspend fun deleteReminder(@Query("user_id") userID: String, @Query("reminder_id") reminderID: String): ResultReminders

    @FormUrlEncoded
    @POST("add-reminder")
    suspend fun addReminderData(@FieldMap params: HashMap<String, String>): ResultReminders

    @FormUrlEncoded
    @POST("update-reminder")
    suspend fun updateReminders(@FieldMap params: HashMap<String, String>): ResultReminders

    @GET("homeBuyer-subscribed-clients")
    suspend fun homeBuyerSubscriptions(@Query("user_id") userID: String): ResultActiveSubscriptions

    @GET("nurture-subscribed-clients")
    suspend fun clientNurtureSubscriptions(@Query("user_id") userID: String): ResultActiveSubscriptions


}