package com.xperienceclientnurture

import android.os.Bundle
import com.xperienceclientnurture.base.BaseActivity
import com.xperienceclientnurture.ui.login.LoginFragment
import com.xperienceclientnurture.ui.register.RegisterFragment
import com.xperienceclientnurture.utils.Constants

class MainActivity : BaseActivity() {

    override val containerId: Int
        get() = R.id.container

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (intent.getBooleanExtra(Constants.IS_LOGIN, false))
            replaceFragment(LoginFragment::class.java)
        else
            replaceFragment(RegisterFragment::class.java)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}
