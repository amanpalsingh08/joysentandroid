package com.xperienceclientnurture.interfaces

import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.textfield.TextInputEditText
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog

interface OnDateTimeSelectionListener {
    fun onDateSet(
        view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int,
        editView: ConstraintLayout? = null, editText: TextInputEditText? = null
    )

    fun onTimeSet(view: TimePickerDialog?, hourOfDay: Int, minute: Int, second: Int)
}