package com.xperienceclientnurture.interfaces

interface OnItemClickListener {

    fun onClick(position: Int, type: String)

    interface OnTitleClickListener{
        fun onTitleClick(position: Int, type: String)
    }

}