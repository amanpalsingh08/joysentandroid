package com.xperienceclientnurture.interfaces

interface DialogClickListener {

    fun onDialogYesClick(click: String = "")

    fun onDialogNoClick(click: String = "")
}
