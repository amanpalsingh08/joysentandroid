package com.xperienceclientnurture.bottomsheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.xperienceclientnurture.R

class BottomSheet(listener: BottomSheetListener) : BottomSheetDialogFragment() {


    private var mBottomSheetListener: BottomSheetListener? = null

    init {
        mBottomSheetListener = listener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showsDialog = true
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.bottom_sheet_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<ImageView>(R.id.ivCamera).setOnClickListener {
            mBottomSheetListener!!.onOptionClick("camera clicked")
        }

        view.findViewById<ImageView>(R.id.ivGallery).setOnClickListener {
            mBottomSheetListener!!.onOptionClick("gallary clicked")
        }

        view.findViewById<TextView>(R.id.tvOk).setOnClickListener {
            this.dialog!!.cancel()
        }
    }


    interface BottomSheetListener {
        fun onOptionClick(text: String)

    }

}
