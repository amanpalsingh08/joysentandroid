package com.xperienceclientnurture

data class ResponseModel(
    val Data: Data,
    val Message: String,
    val StatusCode: Int
)

data class Data(
    val UserId: String
)