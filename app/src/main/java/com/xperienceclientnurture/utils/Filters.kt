package com.xperienceclientnurture.utils

import com.xperienceclientnurture.ui.home.ui.clientconnection.Request

object Filters {

    fun getData(query: String, allList: List<Request>): List<Request> {
        val list: MutableList<Request> = arrayListOf()
        for (s in allList.indices) {
            if (allList[s].client_name.contains(query, ignoreCase = true)) {
                list.add(allList[s])
            }
        }
        return list
    }
}