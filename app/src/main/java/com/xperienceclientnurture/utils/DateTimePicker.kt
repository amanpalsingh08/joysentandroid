package com.xperienceclientnurture.utils

import android.content.Context
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.xperienceclientnurture.base.BaseActivity
import com.google.android.material.textfield.TextInputEditText
import com.xperienceclientnurture.R
import com.xperienceclientnurture.interfaces.OnDateTimeSelectionListener
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import com.wdullaer.materialdatetimepicker.time.Timepoint
import java.util.Calendar

object DateTimePicker : DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private var dpd: DatePickerDialog? = null
    private var tpd: TimePickerDialog? = null
    private var editView: ConstraintLayout? = null
    private var editText: TextInputEditText? = null
    lateinit var listener: OnDateTimeSelectionListener

    fun showDatePicker(
        context: Context, listener: OnDateTimeSelectionListener,
        editView: ConstraintLayout? = null, editText: TextInputEditText? = null
    ) {
        this.listener = listener
        this.editView = editView
        this.editText = editText
        val now = Calendar.getInstance()
        if (dpd == null) {
            dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
            )
        } else {
            dpd!!.initialize(
                this,
                now[Calendar.YEAR],
                now[Calendar.MONTH],
                now[Calendar.DAY_OF_MONTH]
            )
        }

        if (editView == null && editText == null)
            dpd?.minDate = now

        dpd?.accentColor = ContextCompat.getColor(context, R.color.colorPrimary)

        dpd?.setOnCancelListener {
            dpd = null
        }
        dpd?.show((context as BaseActivity).supportFragmentManager, DateTimePicker::class.java.simpleName)

    }

    fun showTimePicker(context: Context, listener: OnDateTimeSelectionListener) {
        this.listener = listener
        val now = Calendar.getInstance()
        if (tpd == null) {
            tpd = TimePickerDialog.newInstance(
                this,
                now[Calendar.HOUR_OF_DAY],
                now[Calendar.MINUTE],
                false
            )
        } else {
            tpd!!.initialize(
                this,
                now[Calendar.HOUR_OF_DAY],
                now[Calendar.MINUTE],
                now[Calendar.SECOND],
                false
            )
        }
        val timePoint = Timepoint(
            now[Calendar.HOUR_OF_DAY],
            now[Calendar.MINUTE],
            now[Calendar.SECOND],
        )
        tpd?.setMinTime(timePoint)

        tpd?.accentColor = ContextCompat.getColor(context, R.color.colorPrimary)

        tpd?.setOnCancelListener {
            tpd = null
        }
        tpd?.show((context as BaseActivity).supportFragmentManager, DateTimePicker::class.java.simpleName)

    }

    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        listener.onDateSet(view, year, monthOfYear, dayOfMonth, editView, editText)
    }

    override fun onTimeSet(view: TimePickerDialog?, hourOfDay: Int, minute: Int, second: Int) {
        listener.onTimeSet(view, hourOfDay, minute, second)
    }

}