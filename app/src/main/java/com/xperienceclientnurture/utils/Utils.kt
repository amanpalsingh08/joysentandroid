package com.xperienceclientnurture.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.SpannableString
import android.view.Gravity
import android.view.View
import android.widget.TextView
import com.xperienceclientnurture.R
import com.xperienceclientnurture.interfaces.DialogClickListener

class Utils {

    fun getList(contribution: Int): List<String> {
        val total = 100 - contribution
        val list: MutableList<String> = arrayListOf()
        val size: Int = total / 10
        println("Cal $size")
        val array = IntArray(size)
        for (k in 0 until size) {
            list.add((10 * (k + 1)).toString() + "%")
        }

        if (list.isEmpty()) {
            list.add("0%")
        }

        return list
    }

}


//*function for custom dialog
fun showCustomDialog(
    context: Context,
    dialogType: String,
    dialogClickType: String,
    dialogClickListener: DialogClickListener,
    title: String? = "",
    message: String? = "",
    no: String? = "No",
    yes: String? = "Yes",
    isCancelable: Boolean = true,
    spannableString: SpannableString = SpannableString(message)

) {
    val dialog = Dialog(context)


    //*set Dialog Layout
    when (dialogType) {
        Constants.LOGOUT -> {
            dialog.setContentView(R.layout.dialog_logout)
        }

    }
    val tvNo: TextView = dialog.findViewById(R.id.tvNo)
    tvNo.text = no

    val tvTitle: TextView = dialog.findViewById(R.id.tv_title)
    tvTitle.text = title

    val tvTitleMessage: TextView = dialog.findViewById(R.id.tv_message)
    tvTitleMessage.text = spannableString
    tvTitleMessage.gravity = Gravity.CENTER

    val tvYes: TextView = dialog.findViewById(R.id.tvYes)
    tvYes.text = yes

    //*set Dialog click
    when (dialogClickType) {
        Constants.ONE_CLICK_LISTENER -> {
            tvNo.visibility = View.GONE
            tvYes.setOnClickListener {
                dialogClickListener.onDialogYesClick(yes!!)
                dialog.dismiss()
            }
        }
        Constants.TWO_CLICK_LISTENER -> {
            tvYes.setOnClickListener {
                dialogClickListener.onDialogYesClick(yes!!)
                dialog.dismiss()
            }
            tvNo.setOnClickListener {
                dialogClickListener.onDialogNoClick(no!!)
                dialog.dismiss()
            }
        }
    }
    dialog.setCancelable(isCancelable)
    dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    if (dialog.isShowing) {
        dialog.dismiss()
    }
    dialog.show()


}

