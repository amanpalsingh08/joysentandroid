package com.xperienceclientnurture.utils

import android.text.TextUtils
import java.util.regex.Pattern

class Validators {

    companion object {
        var errorMessage: String? = ""
    }


    private fun isValidEmailId(email: String): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }


    fun validateEmail(email: String): Boolean = if (TextUtils.isEmpty(email)) {
        errorMessage = ""
        false
    } else if (!isValidEmailId(email)) {
        errorMessage = ""
        false
    } else {
        true
    }

    fun validateLoginData(email: String, password: String): Boolean {

        if (TextUtils.isEmpty(email) && TextUtils.isEmpty(password)) {
            errorMessage = "Please enter email and password."
            return false
        } else if (TextUtils.isEmpty(email)) {
            errorMessage = "Please enter email."
            return false
        } else if (!isValidEmailId(email)) {
            errorMessage = "Please enter a valid email id."
            return false
        } else if (TextUtils.isEmpty(password)) {
            errorMessage = "Please enter password."
            return false
        } else if (password.length < 6) {
            errorMessage = "Please enter a valid password"
            return false
        } else {
            return true
        }
    }


    fun validateForgotPassEmail(email: String): Boolean = if (TextUtils.isEmpty(email)) {
        errorMessage = "Please enter email address."
        false
    } else if (!isValidEmailId(email)) {
        errorMessage = "Please enter valid email address."
        false
    } else {
        true
    }

    fun validateSignUpData(
        firstName: String,
        lastName: String,
        emailAddress: String,
        password: String,
        confirmPassword: String
    ): Boolean {

        if (TextUtils.isEmpty(firstName)) {
            errorMessage = "Please enter the first name."
            return false
        } else if (TextUtils.isEmpty(lastName)) {
            errorMessage = "Please enter the name."
            return false
        } else if (TextUtils.isEmpty(emailAddress)) {
            errorMessage = "Please enter email address."
            return false
        } else if (!isValidEmailId(emailAddress)) {
            errorMessage = "Please enter valid email address."
            return false
        } else if (TextUtils.isEmpty(password)) {
            errorMessage = "Please enter password."
            return false
        } else if (password.length < 6) {
            errorMessage = "Password should be at least 6 characters long."
            return false
        } else if (TextUtils.isEmpty(confirmPassword)) {
            errorMessage = "Please enter the confirm password."
            return false
        } else if (password != confirmPassword) {
            errorMessage = "Password and confirm password should be same."
            return false
        } else {
            return true
        }
    }


    fun validateMyAccountData(
        firstName: String,
        lastName: String,
        emailAddress: String,
        phone: String,
        address: String,
        zipCode: String,
        city: String,
        state: String,
        userType: String
    ): Boolean {
        when {
            TextUtils.isEmpty(firstName) -> {
                errorMessage = "Please enter the first name."
                return false
            }
            TextUtils.isEmpty(lastName) -> {
                errorMessage = "Please enter the name."
                return false
            }
            TextUtils.isEmpty(emailAddress) -> {
                errorMessage = "Please enter email address."
                return false
            }
            !isValidEmailId(emailAddress) -> {
                errorMessage = "Please enter valid email address."
                return false
            }
            TextUtils.isEmpty(phone) -> {
                errorMessage = "Please enter phone number."
                return false
            }
            TextUtils.isEmpty(address) -> {
                errorMessage = "Please enter address."
                return false
            }
           /* TextUtils.isEmpty(zipCode) -> {
                errorMessage = "Please enter zip code."
                return false
            }*/
            TextUtils.isEmpty(city) -> {
                errorMessage = "Please enter city."
                return false
            }
            TextUtils.isEmpty(state) -> {
                errorMessage = "Please enter state."
                return false
            }
            TextUtils.isEmpty(userType) -> {
                errorMessage = "Please enter user type."
                return false
            }
            else -> {
                return true
            }
        }
    }

    fun isValidPhoneNumber(target: CharSequence): Boolean {
        return if (target.length != 10) {
            false
        } else {
            android.util.Patterns.PHONE.matcher(target).matches()
        }
    }

    fun validateChangePassword(password: String, confirmPassword: String): Boolean {
        when {
            TextUtils.isEmpty(password) -> {
                errorMessage = "Please enter password."
                return false
            }
            password.length < 6 -> {
                errorMessage = "Password should be at least 6 characters long."
                return false
            }
            TextUtils.isEmpty(confirmPassword) -> {
                errorMessage = "Please enter the confirm password."
                return false
            }
            password != confirmPassword -> {
                errorMessage = "Password and confirm password should be same."
                return false
            }
            else -> {
                return true
            }
        }
    }

    fun validateCard(cardNumber: String, cvv: String, month: String, year: String): Boolean {
        when {
            TextUtils.isEmpty(cardNumber) -> {
                errorMessage = "Please enter card number."
                return false
            }

            TextUtils.isEmpty(cvv) -> {
                errorMessage = "Please enter cvv."
                return false
            }

            TextUtils.isEmpty(month) -> {
                errorMessage = "Please enter month."
                return false
            }
            TextUtils.isEmpty(year) -> {
                errorMessage = "Please enter year."
                return false
            }

            else -> {
                return true
            }
        }
    }

    fun validateRequestConnection(connection1: String, connection2: String): Boolean {
        return when {
            TextUtils.isEmpty(connection1) -> {
                errorMessage = "Please select connection 1."
                false
            }

            TextUtils.isEmpty(connection2) -> {
                errorMessage = "Please select connection 2."
                false
            }

            else -> {
                true
            }
        }
    }

    fun validateFeedback(clientID: String, feedback: String): Boolean {
        when {
            TextUtils.isEmpty(clientID) -> {
                errorMessage = "Please select clients."
                return false
            }

            TextUtils.isEmpty(feedback) -> {
                errorMessage = "Please enter feedback."
                return false
            }
            else -> {
                return true
            }
        }
    }

    fun validateClientNotes(clientID: String, notes: String): Boolean {
        return when {
            clientID.isEmpty() -> {
                errorMessage = "Please select clients."
                false
            }

            notes.isEmpty() -> {
                errorMessage = "Please enter notes."
                false
            }

            else -> {
                true
            }
        }
    }


    fun validateReminders(date: String,time:String, message: String): Boolean {
        return when {
            date.isEmpty() -> {
                errorMessage = "Please select date."
                false
            }
            time.isEmpty() -> {
                errorMessage = "Please select time."
                false
            }
            message.isEmpty() -> {
                errorMessage = "Please enter message."
                false
            }

            else -> {
                true
            }
        }
    }

    fun validateAddEditClientData(
        firstName: String,
        lastName: String,
        emailAddress: String,
        phone: String,
        phoneType: String,
        phone2: String,
        phoneType2: String,
        address: String,
        zipCode: String,
        city: String,
        state: String,
        country: String,
        addressShip: String,
        zipCodeShip: String,
        cityShip: String,
        stateShip: String,
        countryShip: String,
        frequency: String,
        group: String,
        status: String,
        leadSource: String,
        stage: String,
        closeDate: String
    ): Boolean {
        when {
            TextUtils.isEmpty(firstName) -> {
                errorMessage = "Please enter the first name."
                return false
            }
            TextUtils.isEmpty(lastName) -> {
                errorMessage = "Please enter the name."
                return false
            }
            TextUtils.isEmpty(emailAddress) -> {
                errorMessage = "Please enter email address."
                return false
            }
            !isValidEmailId(emailAddress) -> {
                errorMessage = "Please enter valid email address."
                return false
            }
            TextUtils.isEmpty(phone) -> {
                errorMessage = "Please enter phone number 1."
                return false
            }
            TextUtils.isEmpty(phoneType) -> {
                errorMessage = "Please enter phone Type 1."
                return false
            }
            TextUtils.isEmpty(phone2) -> {
                errorMessage = "Please enter phone number 2."
                return false
            }
            TextUtils.isEmpty(phoneType2) -> {
                errorMessage = "Please enter phone Type 2."
                return false
            }
            TextUtils.isEmpty(address) -> {
                errorMessage = "Please enter address."
                return false
            }
            TextUtils.isEmpty(zipCode) -> {
                errorMessage = "Please enter zip code."
                return false
            }
            TextUtils.isEmpty(city) -> {
                errorMessage = "Please enter city."
                return false
            }
            TextUtils.isEmpty(state) -> {
                errorMessage = "Please enter state."
                return false
            }
            TextUtils.isEmpty(country) -> {
                errorMessage = "Please enter country."
                return false
            }
            TextUtils.isEmpty(addressShip) -> {
                errorMessage = "Please enter shipping address."
                return false
            }
            TextUtils.isEmpty(zipCodeShip) -> {
                errorMessage = "Please enter shipping zip code."
                return false
            }
            TextUtils.isEmpty(cityShip) -> {
                errorMessage = "Please enter shipping city."
                return false
            }
            TextUtils.isEmpty(stateShip) -> {
                errorMessage = "Please enter shipping state."
                return false
            }
            TextUtils.isEmpty(countryShip) -> {
                errorMessage = "Please enter shipping country."
                return false
            }

            TextUtils.isEmpty(frequency) -> {
                errorMessage = "Please enter frequency."
                return false
            }
            TextUtils.isEmpty(group) -> {
                errorMessage = "Please enter group."
                return false
            }
            TextUtils.isEmpty(status) -> {
                errorMessage = "Please enter status."
                return false
            }
            TextUtils.isEmpty(leadSource) -> {
                errorMessage = "Please enter lead source."
                return false
            }
            TextUtils.isEmpty(stage) -> {
                errorMessage = "Please enter stage."
                return false
            }
            TextUtils.isEmpty(closeDate) -> {
                errorMessage = "Please select closing date."
                return false
            }
            else -> {
                return true
            }
        }
    }
}
