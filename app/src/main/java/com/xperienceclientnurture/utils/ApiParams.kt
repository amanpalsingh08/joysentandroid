package com.xperienceclientnurture.utils

enum class ApiParams {

    USER_ID,
    NOTE_ID,
    CLIENT_ID,
    MESSAGE,
    DATE,
    TIME,
    REMINDER_ID,
    CLIENT_NOTES;

    fun value() = name.lowercase()

}