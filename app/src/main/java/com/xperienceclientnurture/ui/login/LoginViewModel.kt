package com.xperienceclientnurture.ui.login

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.sharedpreference.Prefs
import com.google.gson.Gson
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.Validators
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class LoginViewModel : BaseViewModel() {
    val email = MutableLiveData<String>("")
    val password = MutableLiveData<String>("")

    /*val email = MutableLiveData<String>("devtester478@gmail.com")
    val password = MutableLiveData<String>("123456")*/

    /*val email = MutableLiveData<String>("developertester478@gmail.com")
    val password = MutableLiveData<String>("123456")*/
    val progress = SingleLiveData<Boolean>()
    val loginSuccess = SingleLiveData<Boolean>()
    val navigation = SingleLiveData<String>()
    val gson = Gson()

    fun login() {
        val em = email.value!!.trim()
        val ps = password.value!!.trim()
        if (!Validators().validateLoginData(em, ps)) {
            showToast(Validators.errorMessage.toString())
        } else {

            val params = HashMap<String, String>()

            params["email"] = em
            params["password"] = ps


            progress.postValue(true)
            val service = RetrofitFactory.makeRetrofitService()
            CoroutineScope(Dispatchers.IO).launch {
                val response = service.postLogin(params)
                try {

                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {
                            progress.postValue(false)

                            response.body()?.let {

                                when (it.status_code) {
                                    200 -> {
                                        if (it.status.equals(Constants.ERROR, ignoreCase = true)) {
                                            showToast(it.message)
                                        } else {
                                            Prefs.putString(
                                                Constants.USER_ID,
                                                it.data.user_id.toString()
                                            )
                                            Prefs.putString(Constants.USER_ROLE, it.data.role)
                                            Prefs.putString(Constants.USER_PHONE, "9876543210")
                                            Prefs.putString(Constants.USER_NAME, it.data.username)

                                            Prefs.putString(Constants.NAME, buildString { append(it.data.firstname).append(" ").append(it.data.lastname) })
                                            Prefs.putBoolean(Constants.IS_SESSION, true)


                                            Prefs.putString(
                                                Constants.KEY_LOGIN_OBJ,
                                                gson.toJson(it.data)
                                            )
                                            navigateToHome()
                                        }
                                    }
                                }
                            }
                        } else {
                            progress.postValue(false)
                            showToast("Error network operation failed with ${response.code()}")
                        }
                    }
                } catch (e: HttpException) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Exception ${e.message}")
                } catch (e: Throwable) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Ooops: Something else went wrong")
                }
            }
        }
    }

    private fun navigateToHome() {
        navigation.postValue(Constants.HOME)
    }

    fun navigateToForgot() {
        navigation.postValue(Constants.FORGOT)
    }

    fun backPress() {
        navigation.postValue(Constants.BACK)
    }

}
