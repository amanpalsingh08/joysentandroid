package com.xperienceclientnurture.ui.login

import com.google.gson.annotations.SerializedName


public data class LoginDataModel(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
) {

    data class Data(
        @SerializedName("address")
        val address: String,
        @SerializedName("api_token")
        val api_token: String,
        @SerializedName("city")
        val city: String,
        @SerializedName("email")
        val email: String,
        @SerializedName("firstname")
        val firstname: String,
        @SerializedName("image")
        val image: String,
        @SerializedName("lastname")
        val lastname: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("role")
        val role: String,
        @SerializedName("state")
        val state: String,
        @SerializedName("user_id")
        val user_id: Int,
        @SerializedName("username")
        val username: String,
        @SerializedName("zip_code")
        val zip_code: String
    )
}