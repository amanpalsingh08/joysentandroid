package com.xperienceclientnurture.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.xperienceclientnurture.MainActivity
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.ui.register.RegisterFragment
import com.xperienceclientnurture.R
import com.xperienceclientnurture.databinding.FragmentLoginBinding
import com.xperienceclientnurture.ui.forgotpassword.ForgotPasswordFragment
import com.xperienceclientnurture.ui.home.HomeScreenActivity
import com.xperienceclientnurture.utils.Constants

class LoginFragment :
    BaseFragment<FragmentLoginBinding, LoginViewModel>() {
    override val fragmentLayoutId: Int
        get() = R.layout.fragment_login
    override val viewModelClass: Class<LoginViewModel>
        get() = LoginViewModel::class.java


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewModel = viewModel
        binding.viewModel!!.loginSuccess.observe(viewLifecycleOwner) {
            if (it == true) {
                replaceFragment(RegisterFragment::class.java)

            }
        }

        binding.viewModel!!.progress.observe(viewLifecycleOwner) {
            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }
        }

        binding.viewModel!!.navigation.observe(viewLifecycleOwner) {

            if (it != null) {
                when (it) {
                    Constants.BACK -> {
                        (activity as MainActivity).onBackPressed()
                    }

                    Constants.HOME -> {
                        startActivity(
                            Intent(activity, HomeScreenActivity::class.java)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        )
                        requireActivity().finish()
                    }

                    Constants.FORGOT -> {
                        replaceFragment(ForgotPasswordFragment::class.java)
                    }
                }
                binding.viewModel!!.navigation.value = null
            }

        }


    }

}
