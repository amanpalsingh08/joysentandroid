package com.xperienceclientnurture.ui.forgotpassword

data class NotificationTitleModel(
    var isPressed: Boolean,
    val title: String
)