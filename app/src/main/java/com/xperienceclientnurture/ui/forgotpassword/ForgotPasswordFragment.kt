package com.xperienceclientnurture.ui.forgotpassword

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer

import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentForgotPasswordBinding
import com.xperienceclientnurture.ui.login.LoginFragment
import com.xperienceclientnurture.utils.Constants

class ForgotPasswordFragment(
) : BaseFragment<FragmentForgotPasswordBinding, ForgotPasswordViewModel>() {

    override val fragmentLayoutId: Int
        get() = R.layout.fragment_forgot_password
    override val viewModelClass: Class<ForgotPasswordViewModel>
        get() = ForgotPasswordViewModel::class.java


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewModel = viewModel

        binding.viewModel!!.progress.observe(this@ForgotPasswordFragment, Observer {
            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }
        })

        binding.viewModel!!.navigation.observe(this@ForgotPasswordFragment, Observer {

            if (it!=null) {
                when(it){
                    Constants.LOGIN->{
                        replaceFragment(LoginFragment::class.java)
                    }
                    Constants.BACK->{
                        activity!!.onBackPressed()
                    }
                }
                binding.viewModel!!.navigation.value=null
            }

        })
    }

}
