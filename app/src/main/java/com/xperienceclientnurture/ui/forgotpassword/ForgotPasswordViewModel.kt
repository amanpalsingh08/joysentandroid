package com.xperienceclientnurture.ui.forgotpassword

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.Validators
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class ForgotPasswordViewModel : BaseViewModel() {

    val email = MutableLiveData<String>("")

    val progress = SingleLiveData<Boolean>()
    val navigation = SingleLiveData<String>()


    fun forgotPassword() {
        val em = email.value!!.trim()
        if (!Validators().validateForgotPassEmail(em)) {
            showToast(Validators.errorMessage.toString())
        } else {

            val params = HashMap<String, String>()

            params["email"] = em


            progress.postValue(true)
            val service = RetrofitFactory.makeRetrofitService()
            CoroutineScope(Dispatchers.IO).launch {
                val response = service.postForgotPassword(params)
                try {

                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {
                            progress.postValue(false)
                            navigateToLogin()
                            response.body()?.let { showToast(it.message) }
                        } else {
                            progress.postValue(false)
                            showToast("Error network operation failed with ${response.code()}")
                        }
                    }
                } catch (e: HttpException) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Exception ${e.message}")
                } catch (e: Throwable) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Ooops: Something else went wrong")
                }
            }
        }
    }

    private fun navigateToLogin() {
        navigation.postValue(Constants.LOGIN)
    }

    fun backPress() {
        navigation.postValue(Constants.BACK)
    }

}
