package com.xperienceclientnurture.ui.forgotpassword
import com.google.gson.annotations.SerializedName


data class ForgotDataModel(
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val statusCode: Int
)