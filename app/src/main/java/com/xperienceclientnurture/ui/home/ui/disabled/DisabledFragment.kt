package com.xperienceclientnurture.ui.home.ui.disabled

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentDisabledBinding
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.home.HomeScreenActivity
import com.xperienceclientnurture.ui.home.ui.adapter.DisabledClientsAdapter
import com.xperienceclientnurture.utils.Constants

class DisabledFragment : BaseFragment<FragmentDisabledBinding, DisabledViewModel>(),
    OnItemClickListener {
    override val fragmentLayoutId: Int
        get() = R.layout.fragment_disabled
    override val viewModelClass: Class<DisabledViewModel>
        get() = DisabledViewModel::class.java

    private lateinit var list: List<DisabledClientDetails>
    private lateinit var adapter: DisabledClientsAdapter


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        setObservers()

        list = arrayListOf()
        adapter = DisabledClientsAdapter(list)

        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        adapter.setItemSelectedListener(this)
        viewModel.getDisabledClients()
    }

    private fun setObservers() {
        viewModel.updateUI.observe(viewLifecycleOwner) {
            if (it != null) {
                setData(it)
                viewModel.updateUI.value = null
            }
        }

        viewModel.progress.observe(viewLifecycleOwner) {

            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }

        }

        viewModel.noData.observe(viewLifecycleOwner) {

            if (it) {
                binding.noDataFound.root.visibility = View.VISIBLE
            } else {
                binding.noDataFound.root.visibility = View.GONE
            }

        }
    }

    private fun setData(it: List<DisabledClientDetails>) {
        list = it
        adapter.updateList(list)
    }


    override fun onClick(position: Int, type: String) {
        when (type) {
            Constants.SEND_EMAIL -> {
                (activity as HomeScreenActivity).sendEmail(
                    list[position].email,
                    "Xperience Client Nurture", "You are our potential client."
                )
            }

            Constants.SEND_CALL -> {
                (activity as HomeScreenActivity).call(list[position].phone)
            }
        }
    }

}
