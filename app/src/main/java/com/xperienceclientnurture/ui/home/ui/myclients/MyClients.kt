package com.xperienceclientnurture.ui.home.ui.myclients

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentMyClientsBinding
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.home.HomeScreenActivity
import com.xperienceclientnurture.ui.home.ui.HolderActivity
import com.xperienceclientnurture.ui.home.ui.adapter.MyClientsAdapter
import com.xperienceclientnurture.ui.home.ui.addeditclient.ClientDetails
import com.xperienceclientnurture.utils.Constants

class MyClients : BaseFragment<FragmentMyClientsBinding, MyClientsViewModel>(),
    OnItemClickListener {

    override val fragmentLayoutId: Int
        get() = R.layout.fragment_my_clients
    override val viewModelClass: Class<MyClientsViewModel>
        get() = MyClientsViewModel::class.java
    var list: List<ClientDetails> = arrayListOf()

    lateinit var adapter: MyClientsAdapter

    private val myClientsReceiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            viewModel.getClients()
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        requireActivity().registerReceiver(myClientsReceiver, IntentFilter(Constants.RECEIVER_MY_CLIENT))

        viewModel.updateUI.observe(viewLifecycleOwner) {
            if (it != null) {
                setData(it)
            }
        }

        viewModel.progress.observe(viewLifecycleOwner) {

            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }

        }

        viewModel.noData.observe(viewLifecycleOwner) {

            if (it) {
                binding.noDataFound.root.visibility = View.VISIBLE
            } else {
                binding.noDataFound.root.visibility = View.GONE
            }

        }

        adapter = MyClientsAdapter(list)

        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        adapter.setItemSelectedListener(this)
        viewModel.getClients()
    }

    private fun setData(it: List<ClientDetails>) {
        list = it
        adapter.updateList(list)
    }

    override fun onDestroy() {
        super.onDestroy()
        requireActivity().unregisterReceiver(myClientsReceiver)
    }

    override fun onClick(position: Int, type: String) {
        when (type) {
            /*Constants.EDIT_ITEM -> {
                activity!!.startActivity(
                    Intent(activity, HolderActivity::class.java)
                        .putExtra(Constants.SCREEN_NAME, Constants.SCREEN_ADD_CLIENT)
                        .putExtra(Constants.ARGS_IS_EDIT, true)
                        .putExtra(Constants.ARGS_CLIENT_ID, list[position].id.toString())
                )
            }*/
            Constants.REQUEST_CONNECTION -> {
                requireActivity().startActivity(
                    Intent(activity, HolderActivity::class.java)
                        .putExtra(Constants.SCREEN_NAME, Constants.SCREEN_REQUEST_CONNECTION)
                        .putExtra(Constants.ARGS_IS_EDIT, true)
                        .putExtra(Constants.ARGS_CLIENT_ID, list[position].id.toString())
                )
            }

            Constants.SEND_EMAIL -> {
                (activity as HomeScreenActivity).sendEmail(
                    list[position].email ?: "",
                    "Xperience Client Nurture",
                    "You are our potential client."
                )
            }

            Constants.SEND_CALL -> {
                (activity as HomeScreenActivity).call(list[position].phone ?: "")
            }

            Constants.SEARCH -> {
                startActivity(
                    Intent(activity, HolderActivity::class.java)
                        .putExtra(Constants.SCREEN_NAME, Constants.SEARCH)
                )
            }

            Constants.VIEW_CLIENT -> {
                startActivity(
                    Intent(activity, HolderActivity::class.java)
                        .putExtra(Constants.SCREEN_NAME, Constants.VIEW_CLIENT)
                        .putExtra(Constants.ARGS_CLIENT_ID, list[position].id.toString())
                )
            }
        }
    }

}
