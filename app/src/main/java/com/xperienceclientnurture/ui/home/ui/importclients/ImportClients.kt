package com.xperienceclientnurture.ui.home.ui.importclients

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.fondesa.kpermissions.coroutines.flow
import com.fondesa.kpermissions.extension.permissionsBuilder
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentImportClientsBinding
import kotlinx.coroutines.launch
//import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import java.io.File

class ImportClients :
    BaseFragment<FragmentImportClientsBinding, ImportClientsViewModel>(), View.OnClickListener {
    override val fragmentLayoutId: Int
        get() = R.layout.fragment_import_clients
    override val viewModelClass: Class<ImportClientsViewModel>
        get() = ImportClientsViewModel::class.java


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.viewmodel = viewModel


        binding.tieSelectFile.setOnClickListener(this)

        setObservers()
    }

    private fun setObservers() {
        viewModel.progress.observe(viewLifecycleOwner) {

            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }

        }
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.tie_select_file -> {
                selectFile()
            }
        }
    }


    private fun selectFile() {
        val request = permissionsBuilder(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ).build()
        lifecycleScope.launch {
            request.flow().collect { result ->
                println("ImportClients.selectProfileImage $result")
                // Handle the result.
                val intent = Intent(Intent.ACTION_GET_CONTENT)
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.type = "text/csv";
                startActivityForResult(Intent.createChooser(intent, "Open CSV"), 402)
            }
        }
        request.send()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            val file = File(data!!.data!!.path)
            binding.tieSelectFile.setText(file.name)
            viewModel.file = file
        }
    }


}
