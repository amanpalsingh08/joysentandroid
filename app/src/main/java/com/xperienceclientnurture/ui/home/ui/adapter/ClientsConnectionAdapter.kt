package com.xperienceclientnurture.ui.home.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.xperienceclientnurture.R
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.home.ui.clientconnection.Request
import com.xperienceclientnurture.utils.Constants

class ClientsConnectionAdapter(var list: List<Request>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var listener: OnItemClickListener


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).name.text = list[position].client_name
        holder.email.text = list[position].client_email
        holder.role.text = list[position].connection_role
        holder.connectionName.text = list[position].connection_name


        when {
            list[position].status.equals("request", ignoreCase = true) -> {
                holder.requestStatus.background = ContextCompat.getDrawable(
                    holder.requestStatus.context,
                    R.drawable.bg_blue_solid
                )
                holder.requestStatus.text = holder.requestStatus.context.getString(R.string.request)
                holder.setItemSelectedListener(listener)
            }
            list[position].status.equals("already requested", ignoreCase = true) -> {
                holder.requestStatus.text =
                    holder.requestStatus.context.getString(R.string.requested)

                holder.requestStatus.background = ContextCompat.getDrawable(
                    holder.requestStatus.context,
                    R.drawable.bg_red_solid
                )
            }
            list[position].status.equals("requested you", ignoreCase = true) -> {
                holder.requestStatus.text =
                    holder.requestStatus.context.getString(R.string.requested_you)

                holder.requestStatus.background = ContextCompat.getDrawable(
                    holder.requestStatus.context,
                    R.drawable.bg_red_solid
                )
            }list[position].status.equals("connected", ignoreCase = true) -> {
                holder.requestStatus.text =
                    holder.requestStatus.context.getString(R.string.connected)

                holder.requestStatus.background = ContextCompat.getDrawable(
                    holder.requestStatus.context,
                    R.drawable.bg_green_solid
                )
            }
        }
    }

    fun setItemSelectedListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_client_connection,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun updateList(it: List<Request>) {
        list = it
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val parentView: View = itemView.findViewById(R.id.parent_view)
        val name: TextView = itemView.findViewById(R.id.tv_client_name)
        val email: TextView = itemView.findViewById(R.id.tv_client_email)
        val connectionName: TextView = itemView.findViewById(R.id.tv_connection2_value)
        val role: TextView = itemView.findViewById(R.id.tv_role)
        val requestStatus: TextView = itemView.findViewById(R.id.tv_connection2_status)

        lateinit var listener: OnItemClickListener

        init {
            requestStatus.setOnClickListener(this)
        }

        fun setItemSelectedListener(listener: OnItemClickListener) {
            this.listener = listener
        }

        override fun onClick(p0: View?) {
            when (p0!!.id) {
                R.id.tv_request_status -> {
                    listener.onClick(adapterPosition, Constants.REQUESTED_ITEM)
                }
            }
        }

    }

}




