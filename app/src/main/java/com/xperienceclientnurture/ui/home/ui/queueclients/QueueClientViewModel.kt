package com.xperienceclientnurture.ui.home.ui.queueclients

import android.util.Log
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.utils.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class QueueClientViewModel : BaseViewModel() {

    val updateUI = SingleLiveData<List<QueueClientsDetails>>()
    val progress = SingleLiveData<Boolean>()
    val noData = SingleLiveData<Boolean>()


    fun getQueueClients() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getQueueClients(Prefs.getString(Constants.USER_ID, ""))
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            when (it.status_code) {
                                200 -> {
                                    val list = it.data
                                    if (list.isEmpty()) {
                                        noData.postValue(true)
                                    } else {
                                        noData.postValue(false)
                                    }
                                    updateUI.postValue(list)
                                }

                                400 -> {
                                    noData.postValue(true)
                                }
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

}
