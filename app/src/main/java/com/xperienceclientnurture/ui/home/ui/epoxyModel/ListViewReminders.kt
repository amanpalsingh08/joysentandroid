package com.xperienceclientnurture.ui.home.ui.epoxyModel

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.airbnb.epoxy.CallbackProp
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.xperienceclientnurture.databinding.ListItemReminderBinding
import com.xperienceclientnurture.ui.home.ui.reminders.Reminders

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
class ListViewReminders @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private val binding = ListItemReminderBinding.inflate(LayoutInflater.from(context), this)

    @ModelProp
    fun setData(dataObj: Reminders) {

        binding.tvRemindersFrom.text = getSpanData(buildString {
            append("Reminders From: ")
            append(dataObj.firstname)
            append(" ")
            append(dataObj.lastname)
        }, 0, 15)

        binding.tvMessage.text = dataObj.message?.trim()
    }

    @CallbackProp
    fun setClickListener(listener: OnClickListener?) {
        binding.ivEdit.setOnClickListener(listener)
    }

    @CallbackProp
    fun setDeleteListener(listener: OnClickListener?) {
        binding.ivDelete.setOnClickListener(listener)
    }

    private fun getSpanData(string: String, startIndex: Int, lastIndex: Int): SpannableString {
        val spannable = SpannableString(string)

        spannable.setSpan(StyleSpan(Typeface.BOLD), startIndex, lastIndex, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
        spannable.setSpan(ForegroundColorSpan(Color.BLACK), startIndex, lastIndex, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)

        return spannable
    }

}