package com.xperienceclientnurture.ui.home.ui.addeditclient

import android.Manifest
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.fondesa.kpermissions.coroutines.flow
import com.fondesa.kpermissions.extension.permissionsBuilder
import com.google.android.material.textfield.TextInputEditText
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.bottomsheet.BottomSheet
import com.xperienceclientnurture.databinding.FragmentAddEditClientBinding
import com.xperienceclientnurture.interfaces.OnDateTimeSelectionListener
import com.xperienceclientnurture.ui.home.ui.myaccount.MyAccountFragment
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.DateTimePicker
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.Calendar

class AddEditClient : BaseFragment<FragmentAddEditClientBinding, AddEditClientViewModel>(),
    BottomSheet.BottomSheetListener, AdapterView.OnItemSelectedListener, View.OnClickListener, OnDateTimeSelectionListener {

    var frequency = arrayOf("Quarterly")
    var gender = arrayOf("Please Select", "Male", "Female")
    var subscriptionType =
        arrayOf("Please Select", "Nurture Subscription", "Home Buyer Subscription")
    var groupQuarterly = arrayOf("Q1", "Q2", "Q3")
    var phoneType = arrayOf("Home", "Mobile")
    var status = arrayOf("Active", "Disabled")
    var leadSource = arrayOf(
        "Zillow",
        "Realtor.com",
        "Open House",
        "Geo-Farming",
        "Facebook",
        "Google PPC",
        "Advertising Online",
        "Advertising Offline",
        "SOI"
    )
    var stage = arrayOf(
        "SOI",
        "Past Client",
        "Lead",
        "Seller",
        "Buyer",
        "Nurture A",
        "Nurture B",
        "Nurture C",
        "Nurture D"
    )
    var states = arrayOfNulls<String>(0)
    var listChilds: MutableList<ConstraintLayout> = mutableListOf()

    private lateinit var selectProfileBottomSheet: BottomSheet
    private var profileImagePath: String? = ""

    override val fragmentLayoutId: Int
        get() = R.layout.fragment_add_edit_client
    override val viewModelClass: Class<AddEditClientViewModel>
        get() = AddEditClientViewModel::class.java


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        viewModel.isEdit = bundle!!.getBoolean(Constants.ARGS_IS_EDIT)
        when {
            viewModel.isEdit -> {
                viewModel.clientID = bundle!!.getString(Constants.ARGS_CLIENT_ID, "")
            }
        }

        setObservers()
        setClientTypeSpinner()
        setFrequencySpinner()
        setStageSpinner()
        setLeadSourceSpinner()
        setStatusSpinner()
        setPhoneTypeSpinner()
        setGroupSpinner()

        if (viewModel.isEdit) {
            viewModel.getClientDetails()
        } else {
            setChildData()
            viewModel.getStates()
        }

        binding.tvChooseImage.setOnClickListener(this)
        binding.tieClosingDate.setOnClickListener(this)

        setSpouseData()
        binding.rlBottom.setOnClickListener {
            verifyLocalData()
            viewModel.updateAccount()

        }
    }

    private fun verifyLocalData() {
        viewModel.spouseName = binding.layoutSpouse.tieSpouseName.text.toString().trim()
        viewModel.spouseAge = binding.layoutSpouse.tieSpouseAge.text.toString().trim()
        viewModel.listChildData.clear()
        listChilds.forEach { parentView ->
            val name = parentView.findViewById<TextInputEditText>(R.id.tie_name).text.toString().trim()
            val age = parentView.findViewById<TextInputEditText>(R.id.tie_age).text.toString().trim()
            val gender = (parentView.findViewById<Spinner>(R.id.spinner_gender).selectedView as TextView).text.toString().trim()
            val birthday = parentView.findViewById<TextInputEditText>(R.id.tie_birthday).text.toString().trim()

            viewModel.listChildData.add(
                ChildDataModel(
                    name = name,
                    age = age,
                    gender = if (gender == "Please") "" else gender.substring(0, 1),
                    birthday = birthday
                )
            )
        }
    }

    private fun setClientTypeSpinner() {
        val arrayAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, subscriptionType)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerSubscriptionType.adapter = arrayAdapter
        if (viewModel.isEdit)
            binding.spinnerSubscriptionType.isEnabled = false
        binding.spinnerSubscriptionType.onItemSelectedListener = this@AddEditClient
        binding.spinnerSubscriptionType.setSelection(0)
    }


    private fun setFrequencySpinner() {
        val arrayAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, frequency)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerFrequency.adapter = arrayAdapter
        binding.spinnerFrequency.onItemSelectedListener = this@AddEditClient
        binding.spinnerFrequency.setSelection(0)
    }

    private fun setStageSpinner() {
        val arrayAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, stage)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerStage.adapter = arrayAdapter
        binding.spinnerStage.onItemSelectedListener = this@AddEditClient
        binding.spinnerStage.setSelection(0)
    }

    private fun setStatusSpinner() {
        val arrayAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, status)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerStatus.adapter = arrayAdapter
        binding.spinnerStatus.onItemSelectedListener = this@AddEditClient
        binding.spinnerStatus.setSelection(0)
    }

    private fun setLeadSourceSpinner() {
        val arrayAdapter =
            ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, leadSource)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerLeadSource.adapter = arrayAdapter
        binding.spinnerLeadSource.onItemSelectedListener = this@AddEditClient
        binding.spinnerLeadSource.setSelection(0)
    }

    private fun setPhoneTypeSpinner() {
        val arrayAdapter =
            ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, phoneType)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerPhoneType.adapter = arrayAdapter
        binding.spinnerPhoneType2.adapter = arrayAdapter
        binding.spinnerPhoneType.onItemSelectedListener = this@AddEditClient
        binding.spinnerPhoneType2.onItemSelectedListener = this@AddEditClient
        binding.spinnerPhoneType.setSelection(0)
        binding.spinnerPhoneType2.setSelection(0)
    }

    private fun setGroupSpinner() {
        val arrayAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, groupQuarterly)

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerGroup.adapter = arrayAdapter
        binding.spinnerGroup.onItemSelectedListener = this@AddEditClient
        binding.spinnerGroup.setSelection(0)
    }

    private fun setObservers() {
        viewModel.progress.observe(viewLifecycleOwner) {

            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }
        }

        viewModel.listener.observe(viewLifecycleOwner) {

            if (it != null) {
                when (it) {
                    Constants.UPDATE_IMAGE -> {
                        selectProfileImage()
                    }

                    Constants.BACK -> {
                        requireActivity().sendBroadcast(Intent(Constants.RECEIVER_MY_CLIENT))
                        requireActivity().finish()
                    }
                }

                viewModel.listener.value = null
            }

        }
        viewModel.updateSpinner.observe(viewLifecycleOwner) {

            if (it != null) {
                states = arrayOfNulls(it.size)
                for (s in it.indices) {
                    states[s] = it[s].code
                }

                updateStateData()


                viewModel.updateSpinner.value = null
            }

        }

        viewModel.updateUI.observe(viewLifecycleOwner) {
            if (it != null) {
                setData(it)
                viewModel.updateUI.value = null
            }
        }
    }

    private fun updateStateData() {
        val arrayAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, states)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerState.adapter = arrayAdapter
        binding.spinnerStateShipping.adapter = arrayAdapter
        binding.spinnerState.onItemSelectedListener = this@AddEditClient
        binding.spinnerStateShipping.onItemSelectedListener = this@AddEditClient
        binding.spinnerState.setSelection(viewModel.statePos)
        binding.spinnerStateShipping.setSelection(viewModel.stateShipPos)
    }

    private fun setData(it: ClientDetails) {
        binding.tieFirstName.setText(it.firstname)
        binding.tieLastName.setText(it.lastname)
        binding.tieEmail.setText(it.email)
        binding.tiePhone.setText(it.phone)
        binding.tiePhone2.setText(it.phone2)
        binding.tieAddress.setText(it.address)
        binding.tieZipCode.setText(it.zipcode)
        binding.tieCity.setText(it.city)
        binding.tieCountry.setText(it.country)
        binding.tieAddressShipping.setText(it.shipping_address)
        binding.tieZipCodeShipping.setText(it.shipping_zipcode)
        binding.tieCityShipping.setText(it.shipping_city)
        binding.tieCountryShipping.setText(it.shipping_country)
        if (it.closingDate != null)
            binding.tieClosingDate.setText(it.closingDate)

        viewModel.state = it.state ?: ""
        viewModel.stateShip = it.shipping_state ?: ""
        viewModel.frequency = it.frequency ?: ""
        viewModel.group = it.group ?: ""
        viewModel.phoneType = it.phone_type ?: ""
        viewModel.phoneType2 = it.phone_type2 ?: ""
        viewModel.leadSource = it.leadSource ?: ""
        viewModel.status = it.status ?: ""
        viewModel.stage = it.stage ?: ""

        Glide
            .with(this)
            .load(it.profile_image)
            .into(binding.ivUserImage);


        for (index in frequency.indices) {
            if (it.frequency.contentEquals(frequency[index])) {
                binding.spinnerFrequency.setSelection(index)
                break
            }
        }

        binding.spinnerSubscriptionType.isEnabled = false
        if (it.clientType?.contains("nurture")==true) {
            binding.spinnerSubscriptionType.setSelection(1)
        }
        if (it.clientType?.contains("home")==true) {
            binding.spinnerSubscriptionType.setSelection(2)
        }

        for (index in stage.indices) {
            if (it.stage.contentEquals(stage[index])) {
                binding.spinnerStage.setSelection(index)
                break
            }
        }

        for (index in leadSource.indices) {
            if (it.leadSource.contentEquals(leadSource[index])) {
                binding.spinnerLeadSource.setSelection(index)
                break
            }
        }
        for (index in status.indices) {
            if (it.status.contentEquals(status[index])) {
                binding.spinnerStatus.setSelection(index)
                break
            }
        }

        for (index in phoneType.indices) {
            if (it.phone_type.contentEquals(phoneType[index])) {
                binding.spinnerPhoneType.setSelection(index)
                break
            }
        }
        for (index in phoneType.indices) {
            if (it.phone_type2.contentEquals(phoneType[index])) {
                binding.spinnerPhoneType2.setSelection(index)
                break
            }
        }


        when (it.frequency) {
            "Quarterly" -> {
                for (index in groupQuarterly.indices) {
                    if (it.group.contentEquals(groupQuarterly[index])) {
                        binding.spinnerGroup.setSelection(index)
                        break
                    }
                }
            }
        }

        //setSpouseData
        updateSpouseData(it.spouseChildren)
        updateChildData(it.childs)
    }


    private fun selectProfileImage() {
        val request = permissionsBuilder(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ).build()
        lifecycleScope.launch {
            request.flow().collect { result ->
                println("AddEditClient.selectProfileImage $result")
                // Handle the result.
                selectProfileBottomSheet = BottomSheet(this@AddEditClient)
                selectProfileBottomSheet.show(requireActivity().supportFragmentManager, "BottomSheet")
                selectProfileBottomSheet.isCancelable = false
            }
        }
        request.send()
    }

    private fun selectImageFromGallery() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, MyAccountFragment.GALLERY)
    }

    private fun selectImageFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, MyAccountFragment.CAMERA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != AppCompatActivity.RESULT_CANCELED) {
            if (requestCode == MyAccountFragment.GALLERY) {
                if (data != null) {
                    val contentURI = data.data
                    try {
                        val bitmap =
                            MediaStore.Images.Media.getBitmap(requireContext().contentResolver, contentURI)
                        profileImagePath = saveImage(bitmap)
                        binding.ivUserImage!!.setImageBitmap(bitmap)
                        selectProfileBottomSheet.dialog!!.cancel()
                    } catch (e: IOException) {
                        e.printStackTrace()
                        Toast.makeText(context, "Failed to save image", Toast.LENGTH_SHORT).show()
                    }
                }
            } else if (requestCode == MyAccountFragment.CAMERA) {
                val thumbnail = data!!.extras!!.get("data") as Bitmap
                binding.ivUserImage!!.setImageBitmap(thumbnail)
                selectProfileBottomSheet.dialog!!.cancel()
                profileImagePath = saveImage(thumbnail)
            }
        }
    }


    private fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 60, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString() + MyAccountFragment.IMAGE_DIRECTORY
        )
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            viewModel.imageFile = File(
                wallpaperDirectory, ((Calendar.getInstance()
                    .timeInMillis).toString() + ".jpg")
            )
            viewModel.imageFile!!.createNewFile()
            val fo = FileOutputStream(viewModel.imageFile!!)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                context,
                arrayOf(viewModel.imageFile!!.path),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            return viewModel.imageFile!!.absolutePath
        } catch (e1: IOException) {
            e1.printStackTrace()
            return ""
        }
    }

    override fun onOptionClick(text: String) {
        if (text == "gallary clicked") {
            selectImageFromGallery()
        }
        if (text == "camera clicked") {
            selectImageFromCamera()
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        when (p0!!.id) {
            R.id.spinner_frequency -> {
                viewModel.frequency = frequency[p2]
                setGroupSpinner()
            }

            R.id.spinner_subscription_type -> {
                when (p2) {
                    1 -> {
                        viewModel.clientType = "nurture"
                    }

                    2 -> {
                        viewModel.clientType = "home_buyer"
                    }

                    else -> {
                        viewModel.clientType = ""
                    }
                }
            }

            R.id.spinner_group -> {
                when (viewModel.frequency) {
                    "Quarterly" -> {
                        viewModel.group = groupQuarterly[p2]
                    }
                }
            }

            R.id.spinner_status -> {
                viewModel.status = status[p2]
            }

            R.id.spinner_lead_source -> {
                viewModel.leadSource = leadSource[p2]
            }

            R.id.spinner_stage -> {
                viewModel.stage = stage[p2]
            }

            R.id.spinner_phone_type -> {
                viewModel.phoneType = phoneType[p2]
            }

            R.id.spinner_phone_type_2 -> {
                viewModel.phoneType2 = phoneType[p2]
            }

            R.id.spinner_state -> {
                viewModel.state = states[p2]!!
            }

            R.id.spinner_state_shipping -> {
                viewModel.stateShip = states[p2]!!
            }

            R.id.spinner_spouse_gender -> {
                viewModel.spouseGender = if (p2 == 0) "" else gender[p2]
            }
        }
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.tv_choose_image -> {
                selectProfileImage()
            }

            R.id.tie_closing_date -> {
                DateTimePicker.showDatePicker(requireContext(), this)
            }

            R.id.tie_spouse_birthday -> {
                DateTimePicker.showDatePicker(requireContext(), this, editText = binding.layoutSpouse.tieSpouseBirthday)
            }

            R.id.tie_spouse_anniversary -> {
                DateTimePicker.showDatePicker(requireContext(), this, editText = binding.layoutSpouse.tieSpouseAnniversary)
            }
        }
    }

    override fun onDateSet(
        view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int, editView: ConstraintLayout?,
        editText: TextInputEditText?
    ) {
        var updateMonth = monthOfYear.plus(1).toString()
        if (updateMonth.toInt() < 10) {
            updateMonth = "0$updateMonth"
        }

        val date = "$year-$updateMonth-$dayOfMonth"
        println("onDateSet $date")

        if (editView == null) {
            when (editText?.id) {
                R.id.tie_spouse_birthday -> {
                    binding.layoutSpouse.tieSpouseBirthday.setText(date)
                    viewModel.spouseBirthday = date
                }

                R.id.tie_spouse_anniversary -> {
                    binding.layoutSpouse.tieSpouseAnniversary.setText(date)
                    viewModel.spouseAnniversary = date
                }

                else -> viewModel.closingDate.value = date
            }

        } else {
            listChilds.find { it == editView }?.findViewById<TextInputEditText>(R.id.tie_birthday)?.setText(date)
        }

    }

    override fun onTimeSet(view: TimePickerDialog?, hourOfDay: Int, minute: Int, second: Int) {

    }


}
