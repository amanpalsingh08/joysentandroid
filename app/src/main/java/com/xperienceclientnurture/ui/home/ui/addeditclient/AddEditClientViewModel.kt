package com.xperienceclientnurture.ui.home.ui.addeditclient

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.ui.commonmodel.Data
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.Validators
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.HttpException
import java.io.File

class AddEditClientViewModel : BaseViewModel() {

    var isEdit = false
    var clientID = ""
    var clientType = ""

    val firstName = MutableLiveData("")
    val lastName = MutableLiveData("")
    val email = MutableLiveData("")
    val phone = MutableLiveData("")
    val phone2 = MutableLiveData("")
    val address = MutableLiveData("")
    val zipCode = MutableLiveData("")
    val city = MutableLiveData("")
    val country = MutableLiveData("United States")

    val shipAddress = MutableLiveData("")
    val shipZipCode = MutableLiveData("")
    val shipCity = MutableLiveData("")
    var closingDate = MutableLiveData("")
    val shipCountry = MutableLiveData("United States")

    //Spouse Data
    var spouseName = ""
    var spouseAge = ""
    var spouseAnniversary = ""
    var spouseBirthday = ""
    var spouseGender = ""


    val progress = SingleLiveData<Boolean>()
    val listener = SingleLiveData<String>()
    val updateSpinner = SingleLiveData<List<Data>>()
    private var addEditClientData: HashMap<String, RequestBody> = HashMap()
    val updateUI = SingleLiveData<ClientDetails>()

    var phoneType = ""
    var phoneType2 = ""
    var state = ""
    var stateShip = ""
    var frequency = ""
    var group = ""
    var status = ""
    var leadSource = ""
    var stage = ""
    var statePos = 0
    var stateShipPos = 0
    var listChildData = mutableListOf<ChildDataModel>()


    var imageFile: File? = null

    fun updateAccount() {
        val fn = firstName.value!!.trim()
        val ln = lastName.value!!.trim()
        val em = email.value!!.trim()
        val ph = phone.value!!.trim()
        val ph2 = phone2.value!!.trim()
        val add = address.value!!.trim()
        val zc = zipCode.value!!.trim()
        val ct = city.value!!.trim()
        val st = state.trim()
        val cnty = country.value!!.trim()
        val adds = shipAddress.value!!.trim()
        val zcs = shipZipCode.value!!.trim()
        val cts = shipCity.value!!.trim()
        val sts = stateShip.trim()
        val cntys = shipCountry.value!!.trim()
        val closeDate = closingDate.value!!.trim()

        if (!Validators().validateAddEditClientData(
                fn, ln, em, ph, phoneType, ph2, phoneType2, add, zc, ct, st, cnty,
                adds, zcs, cts, sts, cntys, frequency, group, status, leadSource, stage, closeDate
            )
        ) {
            showToast(Validators.errorMessage.toString())
        } else {
            if (isEdit)
                addEditClientData["client_id"] = clientID.toRequestBody("text/plain".toMediaType())
            addEditClientData["user_id"] = Prefs.getString(Constants.USER_ID, "").toRequestBody("text/plain".toMediaType())
            addEditClientData["firstname"] = fn.toRequestBody("text/plain".toMediaType())
            addEditClientData["lastname"] = ln.toRequestBody("text/plain".toMediaType())
            addEditClientData["email"] = em.toRequestBody("text/plain".toMediaType())
            addEditClientData["phone"] = ph.toRequestBody("text/plain".toMediaType())
            addEditClientData["phone_type"] = phoneType.toRequestBody("text/plain".toMediaType())
            addEditClientData["phone2"] = ph2.toRequestBody("text/plain".toMediaType())
            addEditClientData["phone_type2"] = phoneType2.toRequestBody("text/plain".toMediaType())
            addEditClientData["address"] = add.toRequestBody("text/plain".toMediaType())
            addEditClientData["city"] = ct.toRequestBody("text/plain".toMediaType())
            addEditClientData["state"] = st.toRequestBody("text/plain".toMediaType())
            addEditClientData["zipcode"] = zc.toRequestBody("text/plain".toMediaType())
            addEditClientData["shipping_address"] = adds.toRequestBody("text/plain".toMediaType())
            addEditClientData["shipping_city"] = cts.toRequestBody("text/plain".toMediaType())
            addEditClientData["shipping_state"] = sts.toRequestBody("text/plain".toMediaType())
            addEditClientData["shipping_zipcode"] = zcs.toRequestBody("text/plain".toMediaType())
            addEditClientData["frequency"] = frequency.toRequestBody("text/plain".toMediaType())
            addEditClientData["group"] = group.toRequestBody("text/plain".toMediaType())
            addEditClientData["status"] = status.toRequestBody("text/plain".toMediaType())
            addEditClientData["stage"] = stage.toRequestBody("text/plain".toMediaType())
            addEditClientData["lead_source"] = leadSource.toRequestBody("text/plain".toMediaType())
            addEditClientData["closing_date"] = closeDate.toRequestBody("text/plain".toMediaType())
            addEditClientData["client_type"] = clientType.toRequestBody("text/plain".toMediaType())
            addEditClientData["spouse_name"] = spouseName.toRequestBody("text/plain".toMediaType())
            addEditClientData["spouse_age"] = spouseAge.toRequestBody("text/plain".toMediaType())
            addEditClientData["spouse_anniversary"] = spouseAnniversary.toRequestBody("text/plain".toMediaType())
            addEditClientData["spouse_birthday"] = spouseBirthday.toRequestBody("text/plain".toMediaType())
            addEditClientData["spouse_gender"] = spouseGender.toRequestBody("text/plain".toMediaType())

            val list = Gson().toJson(listChildData)

            println("AddEditClientViewModel.updateAccount $list")

            addEditClientData["client_Children"] = list.toRequestBody("text/plain".toMediaType())

            progress.postValue(true)
            val service = RetrofitFactory.makeRetrofitService()
            CoroutineScope(Dispatchers.IO).launch {

                val response: Any
                if (isEdit) {
                    response = when {
                        imageFile != null -> service.postEditClient(
                            addEditClientData,
                            getProfileImage()
                        )

                        else -> service.postEditClientNoImage(addEditClientData)
                    }
                } else {
                    response = when {
                        imageFile != null -> service.postAddClient(
                            addEditClientData,
                            getProfileImage()
                        )

                        else -> service.postAddClientNoImage(addEditClientData)
                    }
                }
                try {

                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {


                            progress.postValue(false)

                            response.body()?.let {

                                listener.postValue(Constants.BACK)

                            }
                        } else {
                            progress.postValue(false)
                            showToast("Error network operation failed with ${response.code()}")
                        }
                    }
                } catch (e: HttpException) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Exception ${e.message}")
                } catch (e: Throwable) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Ooops: Something else went wrong")
                }
            }

        }
    }


    private fun getProfileImage(): MultipartBody.Part {

        val requestBody = imageFile!!.asRequestBody()
        return MultipartBody.Part.createFormData("image", imageFile!!.name, requestBody)

    }


    fun getClientDetails() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getClientDetails(clientID)
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                getStates()
                                updateUI.value = it.data
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }


    fun getStates() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getState()
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {

                                for (i in it.data.indices) {
                                    if (it.data[i].code.contentEquals(state)) {
                                        statePos = i
                                    }
                                    if (it.data[i].code.contentEquals(stateShip)) {
                                        stateShipPos = i
                                    }
                                }
                                updateSpinner.postValue(it.data)
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }


}
