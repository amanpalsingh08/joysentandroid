package com.xperienceclientnurture.ui.home.ui.myaccount
import com.google.gson.annotations.SerializedName


data class UpdateUserDataModel(
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val statusCode: Int
)