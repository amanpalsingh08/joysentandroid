package com.xperienceclientnurture.ui.home.ui.cards

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentMyCardsBinding
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.home.ui.HolderActivity
import com.xperienceclientnurture.ui.home.ui.adapter.MyCardsAdapter
import com.xperienceclientnurture.utils.Constants

class MyCardsFragment : BaseFragment<FragmentMyCardsBinding, MyCardsViewModel>(),
    View.OnClickListener, OnItemClickListener {
    override val fragmentLayoutId: Int
        get() = R.layout.fragment_my_cards
    override val viewModelClass: Class<MyCardsViewModel>
        get() = MyCardsViewModel::class.java

    var list: MutableList<CardsDetails> = arrayListOf()

    lateinit var adapter: MyCardsAdapter

    private val cardsReceiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            viewModel.getCards()
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        requireActivity().registerReceiver(cardsReceiver, IntentFilter(Constants.RECEIVER_CARDS))

        setObservers()

        binding.fab.setOnClickListener(this)

        adapter = MyCardsAdapter(list)

        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        adapter.setItemSelectedListener(this)
        viewModel.getCards()
    }

    private fun setObservers() {
        viewModel.updateUI.observe(this, Observer {
            if (it != null) {
                val list: MutableList<CardsDetails> = ArrayList()
                list.add(it)
                setData(list)
            }


        })

        viewModel.progress.observe(this, Observer {

            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }

        })

        viewModel.noData.observe(this, Observer {

            if (it) {
                binding.noDataFound.root.visibility = View.VISIBLE
            } else {
                binding.noDataFound.root.visibility = View.GONE
            }

        })
    }

    private fun setData(it: MutableList<CardsDetails>) {
        list = it
        if (list.size > 0)
            binding.fab.hide()

        adapter.updateList(list)
    }

    override fun onDestroy() {
        super.onDestroy()
        requireActivity().unregisterReceiver(cardsReceiver)
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.fab -> {
                startActivity(
                    Intent(activity, HolderActivity::class.java)
                        .putExtra(Constants.SCREEN_NAME, Constants.SCREEN_ADD_CARD)
                        .putExtra(Constants.ARGS_IS_EDIT, false)
                )
            }
        }
    }

    override fun onClick(position: Int, type: String) {
        when (type) {
            Constants.EDIT_ITEM -> {
                startActivity(
                    Intent(activity, HolderActivity::class.java)
                        .putExtra(Constants.SCREEN_NAME, Constants.SCREEN_ADD_CARD)
                        .putExtra(Constants.ARGS_IS_EDIT, true)
                        .putExtra(Constants.ARGS_CARD_NUMBER, list[position].card_num)
                        .putExtra(Constants.ARGS_MONTH, list[position].exp_month)
                        .putExtra(Constants.ARGS_YEAR, list[position].exp_year)
                )
            }
        }
    }

}
