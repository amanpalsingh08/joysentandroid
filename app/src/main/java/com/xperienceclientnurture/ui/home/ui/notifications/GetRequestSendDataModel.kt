package com.xperienceclientnurture.ui.home.ui.notifications


import com.google.gson.annotations.SerializedName

data class GetRequestSendDataModel(
    @SerializedName("data")
    val `data`: List<RequestSendDetails>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)

data class RequestSendDetails(
    @SerializedName("client_email")
    val client_email: String,
    @SerializedName("client_name")
    val client_name: String,
    @SerializedName("conn1")
    val conn1: String,
    @SerializedName("conn2")
    val conn2: String,
    @SerializedName("request_id")
    val request_id: Int,
    @SerializedName("request_status")
    val request_status: String
)