package com.xperienceclientnurture.ui.home.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.xperienceclientnurture.R
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.home.ui.gifts.GiftsTypeData
import com.xperienceclientnurture.utils.Constants

class GiftsListAdapter(var list: List<GiftsTypeData>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var listener: OnItemClickListener

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).name.text = list[position].type_name

        holder.setItemSelectedListener(listener)


    }

    fun setItemSelectedListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_gifts,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun updateList(it: List<GiftsTypeData>) {
        list = it
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {


        val name: TextView = itemView.findViewById(R.id.tv_name)
        lateinit var listener: OnItemClickListener

        init {
            name.setOnClickListener(this)
        }

        fun setItemSelectedListener(listener: OnItemClickListener) {
            this.listener = listener
        }

        override fun onClick(v: View?) {
            when(v!!.id){
                R.id.tv_name->{
                    listener.onClick(adapterPosition,Constants.GIFTS_ITEM)
                }
            }
        }

    }

}




