package com.xperienceclientnurture.ui.home.ui.reminders

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Reminders(
    @Json(name = "id") @field:Json(name = "id")
    val id: Int? = null,
    @Json(name = "user_id") @field:Json(name = "user_id")
    val userID: String? = null,
    @Json(name = "date") @field:Json(name = "date")
    val date: String? = null,
    @Json(name = "message") @field:Json(name = "message")
    val message: String? = null,
    @Json(name = "status") @field:Json(name = "status")
    val status: String? = null,
    @Json(name = "created_at") @field:Json(name = "created_at")
    val createdAt: String? = null,
    @Json(name = "updated_at") @field:Json(name = "updated_at")
    val updatedAt: String? = null,
    @Json(name = "firstname") @field:Json(name = "firstname")
    val firstname: String? = null,
    @Json(name = "lastname") @field:Json(name = "lastname")
    val lastname: String? = null
) : Parcelable