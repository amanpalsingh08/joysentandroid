package com.xperienceclientnurture.ui.home.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.xperienceclientnurture.R
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.home.ui.notifications.RequestReceivedDetails
import com.xperienceclientnurture.utils.Constants

class RequestReceivedAdapter(var list: List<RequestReceivedDetails>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var listener: OnItemClickListener

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).name.text = list[position].client_name
        holder.email.text = list[position].client_email
        holder.connection1Value.text = list[position].conn1
        holder.connection1Status.text = list[position].conn1_status.capitalize()
        holder.connection2Value.text = list[position].conn2
        holder.connection2Status.text = list[position].conn2_status.capitalize()

        if (list[position].conn1_status.equals("pending", ignoreCase = true)) {
            holder.connection1Status.background = ContextCompat.getDrawable(holder.connection1Status.context,R.drawable.bg_red_solid_4dp)
        } else
            holder.connection1Status.background = ContextCompat.getDrawable(holder.connection1Status.context,R.drawable.bg_light_green_solid_4dp)

        if (list[position].conn2_status.equals("pending", ignoreCase = true)) {
            holder.connection2Status.background = ContextCompat.getDrawable(holder.connection2Status.context,R.drawable.bg_red_solid_4dp)
        } else {
            holder.connection2Status.background =
                ContextCompat.getDrawable(holder.connection2Status.context, R.drawable.bg_light_green_solid_4dp)
        }

        if (list[position].request_status.equals("accept", ignoreCase = true)) {
            holder.tvAccept.visibility=View.VISIBLE
            holder.tvCancel.visibility=View.INVISIBLE
            holder.tvRequestValue.text = ""
            holder.tvAccept.text = holder.parentView.context.getString(R.string.accept)
            holder.tvAccept.background = ContextCompat.getDrawable(holder.tvAccept.context,R.drawable.bg_blue_solid_4dp)
        } else {
            holder.tvAccept.visibility=View.INVISIBLE
            holder.tvCancel.visibility=View.VISIBLE
            holder.tvRequestValue.text = holder.parentView.context.getString(R.string.accepted)
            holder.tvCancel.text = holder.parentView.context.getString(R.string.txt_cancel)
            holder.tvCancel.background = ContextCompat.getDrawable(holder.tvCancel.context,R.drawable.bg_red_solid_4dp)

        }

        holder.setItemSelectedListener(listener)




    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_request_received,
                parent,
                false
            )
        )
    }

    fun setItemSelectedListener(listener: OnItemClickListener) {
        this.listener = listener
    }


    override fun getItemCount(): Int {
        return list.size
    }

    fun updateList(it: List<RequestReceivedDetails>) {
        list = it
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener  {
        val parentView: View = itemView.findViewById(R.id.parent_view)
        val name: TextView = itemView.findViewById(R.id.tv_client_name)
        val email: TextView = itemView.findViewById(R.id.tv_client_email)
        val connection1Value: TextView = itemView.findViewById(R.id.tv_connection1_value)
        val connection1Status: TextView = itemView.findViewById(R.id.tv_connection1_status)
        val connection2Value: TextView = itemView.findViewById(R.id.tv_connection2_value)
        val connection2Status: TextView = itemView.findViewById(R.id.tv_connection2_status)
        val tvAccept: TextView = itemView.findViewById(R.id.tv_accept)
        val tvCancel: TextView = itemView.findViewById(R.id.tv_cancel)
        val tvRequestValue: TextView = itemView.findViewById(R.id.tv_request_status_value)
        lateinit var listener: OnItemClickListener

        init {
            tvAccept.setOnClickListener(this)
            tvCancel.setOnClickListener(this)
        }

        fun setItemSelectedListener(listener: OnItemClickListener) {
            this.listener = listener
        }

        override fun onClick(p0: View?) {
            when (p0!!.id) {
                R.id.tv_accept -> {
                    listener.onClick(adapterPosition, Constants.ACCEPT_RECEIVED_REQUEST)
                }
                R.id.tv_cancel -> {
                    listener.onClick(adapterPosition, Constants.CANCEL_RECEIVED_REQUEST)
                }
            }
        }


    }

}




