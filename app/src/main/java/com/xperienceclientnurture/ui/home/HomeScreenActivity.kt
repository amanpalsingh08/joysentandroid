package com.xperienceclientnurture.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentContainerView
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.xperienceclientnurture.base.BaseActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.xperienceclientnurture.R
import com.xperienceclientnurture.interfaces.DialogClickListener
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.ui.home.ui.HolderActivity
import com.xperienceclientnurture.ui.home.ui.cards.MyCardsFragment
import com.xperienceclientnurture.ui.home.ui.changepassword.ChangePasswordFragment
import com.xperienceclientnurture.ui.home.ui.clientconnection.ClientConnectionList
import com.xperienceclientnurture.ui.home.ui.delivered.DeliveredFragment
import com.xperienceclientnurture.ui.home.ui.disabled.DisabledFragment
import com.xperienceclientnurture.ui.home.ui.feedback.FeedbackList
import com.xperienceclientnurture.ui.home.ui.gallery.GalleryFragment
import com.xperienceclientnurture.ui.home.ui.gifts.GiftListFragment
import com.xperienceclientnurture.ui.home.ui.home.HomeFragment
import com.xperienceclientnurture.ui.home.ui.importclients.ImportClients
import com.xperienceclientnurture.ui.home.ui.myaccount.MyAccountFragment
import com.xperienceclientnurture.ui.home.ui.myclients.MyClients
import com.xperienceclientnurture.ui.home.ui.notifications.NotificationsFragment
import com.xperienceclientnurture.ui.home.ui.queueclients.QueueClient
import com.xperienceclientnurture.ui.home.ui.recentclients.RecentClientsFragment
import com.xperienceclientnurture.ui.splash.SplashActivity
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.showCustomDialog
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException


class HomeScreenActivity : BaseActivity(), View.OnClickListener,
    NavigationView.OnNavigationItemSelectedListener,
    BottomNavigationView.OnNavigationItemSelectedListener, DialogClickListener {


    val progressObserver = SingleLiveData<Boolean>()
    val logoutObserver = SingleLiveData<Boolean>()

    override fun onDialogYesClick(click: String) {
        logoutApi()
    }


    override fun onDialogNoClick(click: String) {

    }

    override val containerId: Int
        get() = R.id.nav_host_fragment
    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    lateinit var ivHam: ImageView
    lateinit var drawerLayout: DrawerLayout
    lateinit var navController: NavController
    lateinit var progressView: View
    private val hostContainerID: Int = R.id.nav_host_fragment_new
    lateinit var oldContainer: FrameLayout
    lateinit var newContainer: FragmentContainerView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_screen)
        toolbar = findViewById(R.id.toolbar)
        ivHam = findViewById(R.id.iv_ham)
        toolbarTitle = findViewById(R.id.toolbar_title)
        progressView = findViewById(R.id.progress)
        oldContainer = findViewById(R.id.nav_host_fragment)
        newContainer = findViewById(R.id.nav_host_fragment_new)

        setSupportActionBar(toolbar)
        ivHam.setOnClickListener(this)
        toolbarTitle.text = getString(R.string.menu_home)

        drawerLayout = findViewById(R.id.drawer_layout)

        toolbar.menu.clear()

        val navView: NavigationView = findViewById(R.id.nav_view)
        navView.itemIconTintList = null

        val toggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )

        toggle.isDrawerIndicatorEnabled = false;
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)

        val navBottomView: BottomNavigationView = findViewById(R.id.nav_bottom_view)

        navBottomView.setOnNavigationItemSelectedListener(this)


        if (Prefs.getString(Constants.USER_ROLE, "")
                .equals(Constants.CUSTOMER, ignoreCase = false)
        ) {
            navBottomView.visibility = View.GONE
            updateCustomerUI(navView)
            toolbarTitle.text = getString(R.string.gifts)
            replaceFragment(GiftListFragment::class.java)
        } else {
            if (Prefs.getString(Constants.USER_ROLE, "")
                    .equals(Constants.LENDER, ignoreCase = false)
                || Prefs.getString(Constants.USER_ROLE, "")
                    .equals(Constants.AGENT, ignoreCase = false)
            ) {
                navView.menu.findItem(R.id.nav_gifts).isVisible = true
                navView.menu.findItem(R.id.nav_gifts).isEnabled = true
            }
            navBottomView.selectedItemId = R.id.navigation_home
        }

        setObservers()
    }

    private fun setObservers() {
        progressObserver.observe(this, Observer {

            if (it) {
                progressView.visibility = View.VISIBLE
            } else {
                progressView.visibility = View.GONE
            }

        })
        logoutObserver.observe(this, Observer {

            if (it) {
                Prefs.clear()
                startActivity(

                    Intent(this, SplashActivity::class.java)
                        .putExtra(Constants.IS_LOGIN, true)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

                )
                finish()
                logoutObserver.value = null
            }

        })
    }

    private fun updateCustomerUI(navView: NavigationView) {

        /*Gifts*/
        navView.menu.findItem(R.id.nav_gifts).isVisible = true
        navView.menu.findItem(R.id.nav_gifts).isEnabled = true

        /*Client Connections*/
        navView.menu.findItem(R.id.nav_client_connections).isVisible = false
        navView.menu.findItem(R.id.nav_client_connections).isEnabled = false

        /*My Clients*/
        navView.menu.findItem(R.id.nav_my_clients).isVisible = false
        navView.menu.findItem(R.id.nav_my_clients).isEnabled = false

        /*Queue Clients*/
        navView.menu.findItem(R.id.nav_queue_clients).isVisible = false
        navView.menu.findItem(R.id.nav_queue_clients).isEnabled = false


        /*Client Last Delivered*/
        navView.menu.findItem(R.id.nav_client_last_delivered).isVisible = false
        navView.menu.findItem(R.id.nav_client_last_delivered).isEnabled = false

        /*Disabled Clients*/
        navView.menu.findItem(R.id.nav_recently_disabled_clients).isVisible = false
        navView.menu.findItem(R.id.nav_recently_disabled_clients).isEnabled = false

        /*Recent Clients*/
        navView.menu.findItem(R.id.nav_recent_client_connections).isVisible = false
        navView.menu.findItem(R.id.nav_recent_client_connections).isEnabled = false

        /*Request Notifications*/
        navView.menu.findItem(R.id.nav_request_notifications).isVisible = false
        navView.menu.findItem(R.id.nav_request_notifications).isEnabled = false

        /*Import Clients*/
        navView.menu.findItem(R.id.nav_import_clients).isVisible = false
        navView.menu.findItem(R.id.nav_import_clients).isEnabled = false

        /*Client Feedback*/
        navView.menu.findItem(R.id.nav_client_feedback).isVisible = false
        navView.menu.findItem(R.id.nav_client_feedback).isEnabled = false

        /*Gallery*/
        navView.menu.findItem(R.id.nav_gallery).isVisible = false
        navView.menu.findItem(R.id.nav_gallery).isEnabled = false

        /*Client Notes*/
        navView.menu.findItem(R.id.nav_client_notes).isVisible = false
        navView.menu.findItem(R.id.nav_client_notes).isEnabled = false

        /*Reminders*/
        navView.menu.findItem(R.id.nav_reminders).isVisible = false
        navView.menu.findItem(R.id.nav_reminders).isEnabled = false
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.iv_ham -> {
                if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
                    drawerLayout.closeDrawer(GravityCompat.END);
                } else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }
            }
        }
    }


    override fun onNavigationItemSelected(p0: MenuItem): Boolean {

        when (p0.itemId) {
            R.id.nav_client_notes, R.id.nav_reminders,R.id.nav_client_nurture,R.id.nav_new_home_buyer -> {
                oldContainer.visibility = View.GONE
                newContainer.visibility = View.VISIBLE
            }

            else -> {
                newContainer.visibility = View.GONE
                oldContainer.visibility = View.VISIBLE
            }
        }

        when (p0.itemId) {


            R.id.nav_gifts -> {
                toolbarTitle.text = getString(R.string.gifts)
                replaceFragment(GiftListFragment::class.java)
            }

            R.id.nav_my_account -> {
                replaceFragment(MyAccountFragment::class.java)
            }

            R.id.nav_change_password -> {
                replaceFragment(ChangePasswordFragment::class.java)
            }

            R.id.nav_my_clients -> {
                toolbarTitle.text = getString(R.string.my_clients)
                replaceFragment(MyClients::class.java)
            }

            R.id.nav_client_connections -> {
                toolbarTitle.text = getString(R.string.client_connections)
                replaceFragment(ClientConnectionList::class.java)
            }

            R.id.nav_recent_client_connections -> {
                toolbarTitle.text = getString(R.string.recent_client_connections)
                replaceFragment(RecentClientsFragment::class.java)
            }

            R.id.nav_queue_clients -> {
                toolbarTitle.text = getString(R.string.queue_clients)
                replaceFragment(QueueClient::class.java)
            }

            R.id.nav_client_last_delivered -> {
                toolbarTitle.text = getString(R.string.client_last_delivered)
                replaceFragment(DeliveredFragment::class.java)
            }

            R.id.nav_recently_disabled_clients -> {
                toolbarTitle.text = getString(R.string.recently_disabled_clients)
                replaceFragment(DisabledFragment::class.java)
            }

            R.id.nav_cards -> {
                toolbarTitle.text = getString(R.string.cards)
                replaceFragment(MyCardsFragment::class.java)
            }

            R.id.nav_client_feedback -> {
                toolbarTitle.text = getString(R.string.feedback)
                replaceFragment(FeedbackList::class.java)
            }

            R.id.nav_gallery -> {
                toolbarTitle.text = getString(R.string.gallery)
                replaceFragment(GalleryFragment::class.java)
            }

            R.id.nav_request_notifications -> {
                toolbarTitle.text = getString(R.string.title_notifications)
                replaceFragment(NotificationsFragment::class.java)
            }

            R.id.nav_import_clients -> {
                toolbarTitle.text = getString(R.string.import_clients)
                replaceFragment(ImportClients::class.java)
            }

            R.id.nav_logout -> {
                /*startActivity(
                    Intent(this, MainActivity::class.java)
                        .putExtra(Constants.IS_LOGIN, true)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

                )*/

                showCustomDialog(
                    this,
                    Constants.LOGOUT,
                    Constants.TWO_CLICK_LISTENER,
                    this,
                    getString(R.string.title_logout),
                    getString(R.string.logout_txt),
                    getString(R.string.txt_no),
                    getString(R.string.txt_yes)
                )

            }

            R.id.navigation_add_client -> {
                print("Add Client Clicked")
                startActivity(
                    Intent(this, HolderActivity::class.java)
                        .putExtra(Constants.SCREEN_NAME, Constants.SCREEN_ADD_CLIENT)
                        .putExtra(Constants.ARGS_IS_EDIT, false)
                )

            }

            R.id.navigation_queue -> {
                toolbarTitle.text = getString(R.string.queue_clients)
                replaceFragment(QueueClient::class.java)

            }

            R.id.navigation_sent -> {
                toolbarTitle.text = getString(R.string.request_notifications)
                replaceFragment(NotificationsFragment::class.java)

            }

            R.id.navigation_home -> {
                toolbarTitle.text = getString(R.string.title_home)
                replaceFragment(HomeFragment::class.java)

            }

            R.id.nav_client_notes -> {
                toolbarTitle.text = getString(R.string.title_client_notes)
                configureNav(R.id.nav_client_notes)
            }

            R.id.nav_reminders -> {
                toolbarTitle.text = getString(R.string.title_reminders)
                configureNav(R.id.nav_reminders)
            }

            R.id.nav_client_nurture -> {
                toolbarTitle.text = getString(R.string.title_client_nurture)
                configureNav(R.id.nav_client_nurture)
            }

            R.id.nav_new_home_buyer -> {
                toolbarTitle.text = getString(R.string.title_new_home_buyer)
                configureNav(R.id.nav_new_home_buyer)
            }

        }
        drawerLayout.closeDrawer(GravityCompat.END);
        return true

    }

    override fun onBackPressed() {
        finish()
    }

    private fun configureNav(destination: Int,bundle: Bundle = Bundle()) {
        val navHostFragment = supportFragmentManager.findFragmentById(hostContainerID) as NavHostFragment
        val navController = navHostFragment.navController
        val inflater = navController.navInflater
        val graph = inflater.inflate(R.navigation.main_navigation)
        graph.startDestination = destination
        navController.setGraph(graph,bundle)
    }

    private fun logoutApi() {
        progressObserver.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.logout(Prefs.getString(Constants.USER_ID, ""))
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progressObserver.postValue(false)
                        response.body()?.let {
                            logoutObserver.postValue(true)
                        }
                    } else {
                        progressObserver.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progressObserver.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progressObserver.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

}
