package com.xperienceclientnurture.ui.home.ui.gifts
import com.google.gson.annotations.SerializedName


data class GetPaymentsDataModel(
    @SerializedName("data")
    val `data`: PaymentDetailData,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)

data class PaymentDetailData(
    @SerializedName("price_per_gift")
    val price_per_gift: String,
    @SerializedName("tax")
    val tax: String,
    @SerializedName("total")
    val total: Double
)