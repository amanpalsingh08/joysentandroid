package com.xperienceclientnurture.ui.home.ui.clientconnection

import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentClientConnectionListBinding
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.home.ui.adapter.ClientsConnectionAdapter
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.Filters
import kotlinx.coroutines.Runnable

class ClientConnectionList :
    BaseFragment<FragmentClientConnectionListBinding, ClientConnectionListViewModel>(),
    OnItemClickListener, TextWatcher {
    override fun afterTextChanged(s: Editable?) {

        searchQuery = s.toString()

        if (searchQuery.length > 2) {
            handler.removeCallbacks(runnable)
            handler.post(runnable)
        }

        if (searchQuery.isEmpty()){
            setData(list)
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }


    var handler = Handler()
    val runnable = Runnable {
        setData(Filters.getData(searchQuery, list))
    }

    var searchQuery = ""

    override val fragmentLayoutId: Int
        get() = R.layout.fragment_client_connection_list
    override val viewModelClass: Class<ClientConnectionListViewModel>
        get() = ClientConnectionListViewModel::class.java

    private lateinit var list: List<Request>
    private lateinit var adapter: ClientsConnectionAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        setObserver()

        list = arrayListOf()
        adapter = ClientsConnectionAdapter(list)

        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        adapter.setItemSelectedListener(this)

        binding.tieSearch.addTextChangedListener(this)


        viewModel.getClientConnections()
    }

    private fun setObserver() {

        viewModel.updateUI.observe(this, Observer {
            if (it != null) {
                list = it
                setData(list)
            }
        })

        viewModel.progress.observe(this, Observer {

            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }

        })

        viewModel.noData.observe(this, Observer {

            if (it) {
                binding.noDataFound.root.visibility = View.VISIBLE
            } else {
                binding.noDataFound.root.visibility = View.GONE
            }

        })
    }

    private fun setData(it: List<Request>) {

        adapter.updateList(it)
    }

    override fun onClick(position: Int, type: String) {
        when (type) {

            Constants.REQUESTED_ITEM -> {

                showToast(getString(R.string.server_error))


            }

        }
    }

}
