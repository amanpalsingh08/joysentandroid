package com.xperienceclientnurture.ui.home.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.R
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.home.ui.addeditclient.ClientDetails
import com.xperienceclientnurture.utils.Constants


class MyClientsAdapter(var list: List<ClientDetails>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    lateinit var listener: OnItemClickListener

    private val options: RequestOptions by lazy {
        RequestOptions()
            .centerCrop()
            .placeholder(R.drawable.user_profile)
            .error(R.drawable.user_profile)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).name.text = "${list[position].firstname} ${list[position].lastname}"
        holder.email.text = list[position].email
        holder.group.text = list[position].group
        holder.tvClientPhone.text = list[position].phone


        if (list[position].userid == Prefs.getString(Constants.USER_ID, ""))
            holder.ivConnection.visibility = View.VISIBLE



        Glide
            .with(holder.image.context)
            .load(list[position].profile_image)
            .apply(options)
            .into(holder.image);

        holder.setItemSelectedListener(listener)


    }

    fun setItemSelectedListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_clients,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun updateList(it: List<ClientDetails>) {
        list = it
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val name: TextView = itemView.findViewById(R.id.tv_client_name)
        val parentView: View = itemView.findViewById(R.id.parent_view)
        val email: TextView = itemView.findViewById(R.id.tv_client_email)
        val image: ImageView = itemView.findViewById(R.id.ivUserImage)
        val group: TextView = itemView.findViewById(R.id.tv_client_group)
        val ivConnection: ImageView = itemView.findViewById(R.id.iv_connection)
        val tvClientPhone: TextView = itemView.findViewById(R.id.tv_client_phone)
        val ivSearch: ImageView = itemView.findViewById(R.id.iv_search)
        val ivView: ImageView = itemView.findViewById(R.id.iv_view)

        lateinit var listener: OnItemClickListener

        init {
            parentView.setOnClickListener(this)
            ivConnection.setOnClickListener(this)
            ivSearch.setOnClickListener(this)
            email.setOnClickListener(this)
            tvClientPhone.setOnClickListener(this)
            ivView.setOnClickListener(this)
        }

        fun setItemSelectedListener(listener: OnItemClickListener) {
            this.listener = listener
        }

        override fun onClick(p0: View?) {
            when (p0!!.id) {
                R.id.parent_view -> {
                    listener.onClick(adapterPosition, Constants.EDIT_ITEM)
                }
                R.id.iv_connection -> {
                    listener.onClick(adapterPosition, Constants.REQUEST_CONNECTION)
                }
                R.id.tv_client_email -> {
                    listener.onClick(adapterPosition, Constants.SEND_EMAIL)
                }
                R.id.tv_client_phone -> {
                    listener.onClick(adapterPosition, Constants.SEND_CALL)
                }
                R.id.iv_search -> {
                    listener.onClick(adapterPosition, Constants.SEARCH)
                }
                R.id.iv_view-> {
                    listener.onClick(adapterPosition, Constants.VIEW_CLIENT)
                }
            }
        }

    }

}




