package com.xperienceclientnurture.ui.home.ui.feedback
import com.google.gson.annotations.SerializedName


data class GetFeedbackDataModel(
    @SerializedName("data")
    val `data`: List<FeedbackDetails>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)

data class FeedbackDetails(
    @SerializedName("client_email")
    val client_email: String,
    @SerializedName("client_firstname")
    val client_firstname: String,
    @SerializedName("client_id")
    val client_id: String,
    @SerializedName("client_lastname")
    val client_lastname: String,
    @SerializedName("feedback")
    val feedback: String,
    @SerializedName("feedback_id")
    val feedback_id: Int,
    @SerializedName("user_id")
    val user_id: String
)