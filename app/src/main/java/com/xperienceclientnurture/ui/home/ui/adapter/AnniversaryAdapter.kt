package com.xperienceclientnurture.ui.home.ui.adapter

import android.annotation.SuppressLint
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.xperienceclientnurture.R
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.home.ui.gifts.anniversay.AnniversaryDataList
import com.xperienceclientnurture.utils.Constants
import com.slider.sliderimage.adapters.ViewPagerAdapter
import me.relex.circleindicator.CircleIndicator


class AnniversaryAdapter(var list: List<AnniversaryDataList>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var listener: OnItemClickListener

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        (holder as ViewHolder).name.text = list[position].gift_name
        holder.description.text = list[position].description
        holder.purchase.text = "\$ ${list[position].price}"

        holder.viewPagerFullScreen.adapter =
            ViewPagerAdapter(
                context = holder.viewPagerFullScreen.context,
                items = list[position].img
            )
        holder.viewPagerFullScreen.setCurrentItem(0, true)
        holder.indicatorScreen.setViewPager(holder.viewPagerFullScreen)

        try {

            holder.handler.postDelayed(object : Runnable {
                override fun run() {
                    if (holder.lastPosition < holder.currentPosition && holder.currentPosition != list[position].img.size) {
                        holder.viewPagerFullScreen.currentItem = holder.currentPosition + 1
                        holder.lastPosition = holder.currentPosition
                        holder.currentPosition += 1
                    } else {
                        holder.viewPagerFullScreen.currentItem = 0
                        holder.currentPosition = 0
                        holder.lastPosition = -1
                    }
                    holder.handler.postDelayed(this, 2000)
                }


            }, 2000)


        } catch (ignored: Exception) {
            ignored.printStackTrace()
        }


        holder.setItemSelectedListener(listener)


    }

    fun setItemSelectedListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_items_anniversary,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun updateList(it: List<AnniversaryDataList>) {
        list = it
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var lastPosition = -1
        var currentPosition = 0
        val name: TextView = itemView.findViewById(R.id.tv_client_name)
        val description: TextView = itemView.findViewById(R.id.tv_description)
        val viewPagerFullScreen: ViewPager = itemView.findViewById(R.id.viewPagerFullScreen)
        val indicatorScreen: CircleIndicator = itemView.findViewById(R.id.indicatorScreen)
        val purchase: TextView = itemView.findViewById(R.id.btn_purchase)
        private val btnDetail: TextView = itemView.findViewById(R.id.btn_detail)
        lateinit var listener: OnItemClickListener
        var handler = Handler(Looper.getMainLooper())

        init {
            purchase.setOnClickListener(this)
            btnDetail.setOnClickListener(this)
        }

        fun setItemSelectedListener(listener: OnItemClickListener) {
            this.listener = listener
        }

        override fun onClick(v: View?) {
            when (v!!.id) {
                R.id.btn_purchase, R.id.btn_detail -> {
                    listener.onClick(adapterPosition, Constants.ANNIVERSARY_DETAIL)
                }
            }
        }

    }

}




