package com.xperienceclientnurture.ui.home.ui.epoxyModel

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.xperienceclientnurture.databinding.ListItemActiveSubscriptionsBinding
import com.xperienceclientnurture.ui.home.ui.subscriptions.ActiveSubscriptions

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
class ListViewClientNurtureSubscriptions @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private val binding = ListItemActiveSubscriptionsBinding.inflate(LayoutInflater.from(context), this)

    @ModelProp
    fun setData(dataObj: ActiveSubscriptions) {

        binding.tvClientName.text = getSpanData(buildString {
            append("Client Name: ")
            append(dataObj.firstName)
            append(" ")
            append(dataObj.lastName)
        }, 0, 12)

        binding.tvClientEmail.text = getSpanData(buildString {
            append("Client Email: ")
            append(dataObj.email)
        }, 0, 13)

        binding.tvClientPhone.text = getSpanData(buildString {
            append("Client Phone: ")
            append(dataObj.phone)
        }, 0, 13)

        binding.tvClientFrequency.text = getSpanData(buildString {
            append("Frequency: ")
            append(dataObj.frequency)
        }, 0, 10)

        binding.tvGifts.text = getSpanData(buildString {
            append("Gifts: ")
            append(dataObj.gifts)
        }, 0, 6)

        binding.tvTotal.text = getSpanData(buildString {
            append("Total: ")
            append(dataObj.total)
        }, 0, 6)

    }

    private fun getSpanData(string: String, startIndex: Int, lastIndex: Int): SpannableString {
        val spannable = SpannableString(string)

        spannable.setSpan(StyleSpan(Typeface.BOLD), startIndex, lastIndex, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
        spannable.setSpan(ForegroundColorSpan(Color.BLACK), startIndex, lastIndex, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)

        return spannable
    }

}