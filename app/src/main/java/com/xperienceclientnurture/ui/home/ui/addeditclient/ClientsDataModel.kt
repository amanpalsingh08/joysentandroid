package com.xperienceclientnurture.ui.home.ui.addeditclient
import com.google.gson.annotations.SerializedName


data class ClientsDataModel(
    @SerializedName("data")
    val data: List<ClientDetails>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)