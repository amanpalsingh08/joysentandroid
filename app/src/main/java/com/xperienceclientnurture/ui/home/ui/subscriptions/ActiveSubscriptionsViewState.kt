package com.xperienceclientnurture.ui.home.ui.subscriptions

import com.airbnb.mvrx.Async
import com.airbnb.mvrx.MavericksState
import com.airbnb.mvrx.Uninitialized

data class ActiveSubscriptionsViewState(
    val request: Async<ResultActiveSubscriptions> = Uninitialized,
    val listActiveSubscriptions: List<ActiveSubscriptions> = emptyList()
) : MavericksState {

    fun update(response: ResultActiveSubscriptions): ActiveSubscriptionsViewState {
        return copy(listActiveSubscriptions = response.list.filter { it.id != null })
    }
}
