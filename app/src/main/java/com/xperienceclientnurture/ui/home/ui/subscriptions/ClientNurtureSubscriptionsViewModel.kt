package com.xperienceclientnurture.ui.home.ui.subscriptions

import android.content.Context
import com.airbnb.mvrx.Fail
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.MavericksViewModel
import com.airbnb.mvrx.MavericksViewModelFactory
import com.airbnb.mvrx.Success
import com.airbnb.mvrx.ViewModelContext
import com.xperienceclientnurture.extensions.asFlow
import com.xperienceclientnurture.data.ClientRepository
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.utils.Constants
import kotlinx.coroutines.launch

class ClientNurtureSubscriptionsViewModel(
    state: ActiveSubscriptionsViewState, val context: Context, private val repository: ClientRepository
) : MavericksViewModel<ActiveSubscriptionsViewState>(state) {

    fun getClientNurtureSubscription() =
        viewModelScope.launch {
                repository::clientNurtureSubscriptions.asFlow(Prefs.getString(Constants.USER_ID, ""))
                    .execute {
                        when (it) {
                            is Loading -> copy(request = Loading())
                            is Fail -> {
                                it.error.printStackTrace()
                                copy(request = Fail(it.error))
                            }

                            is Success -> {
                                update(it()).copy(request = Success(it()))
                            }

                            else -> {
                                this
                            }
                        }
                    }
        }

    companion object : MavericksViewModelFactory<ClientNurtureSubscriptionsViewModel, ActiveSubscriptionsViewState> {

        override fun create(
            viewModelContext: ViewModelContext, state: ActiveSubscriptionsViewState
        ) = ClientNurtureSubscriptionsViewModel(state, viewModelContext.activity(), ClientRepository(context = viewModelContext.activity()))
    }


}