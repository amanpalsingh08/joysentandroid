package com.xperienceclientnurture.ui.home.ui.gifts.anniversay

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


data class AnniversayDataModel(
    @SerializedName("data")
    val `data`: List<AnniversaryDataList>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)

data class AnniversaryDataList(
    @SerializedName("description")
    val description: String,
    @SerializedName("gift_name")
    val gift_name: String,
    @SerializedName("height")
    val height: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("img")
    val img: List<String>,
    @SerializedName("length")
    val length: String,
    @SerializedName("price")
    val price: String,
    @SerializedName("weight")
    val weight: String,
    @SerializedName("width")
    val width: String
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString()!!,
        source.readString()!!,
        source.readString()!!,
        source.readInt(),
        source.createStringArrayList()!!,
        source.readString()!!,
        source.readString()!!,
        source.readString()!!,
        source.readString()!!
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(description)
        writeString(gift_name)
        writeString(height)
        writeInt(id)
        writeStringList(img)
        writeString(length)
        writeString(price)
        writeString(weight)
        writeString(width)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<AnniversaryDataList> =
            object : Parcelable.Creator<AnniversaryDataList> {
                override fun createFromParcel(source: Parcel): AnniversaryDataList =
                    AnniversaryDataList(source)

                override fun newArray(size: Int): Array<AnniversaryDataList?> = arrayOfNulls(size)
            }
    }
}