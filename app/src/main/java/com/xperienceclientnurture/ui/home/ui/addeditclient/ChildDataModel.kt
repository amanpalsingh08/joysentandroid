package com.xperienceclientnurture.ui.home.ui.addeditclient

data class ChildDataModel(
    val name: String = "",
    val age: String = "",
    val birthday: String = "",
    val gender: String = ""
)
