package com.xperienceclientnurture.ui.home.ui.reminders

import android.content.Context
import com.airbnb.mvrx.Fail
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.MavericksViewModel
import com.airbnb.mvrx.MavericksViewModelFactory
import com.airbnb.mvrx.Success
import com.airbnb.mvrx.ViewModelContext
import com.xperienceclientnurture.extensions.asFlow
import com.xperienceclientnurture.data.ClientRepository
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.utils.ApiParams
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.Validators
import kotlinx.coroutines.launch

class AddEditRemindersViewModel(
    state: AddEditRemindersViewState, val context: Context, private val repository: ClientRepository
) : MavericksViewModel<AddEditRemindersViewState>(state) {

    var clientID = ""
    var onShowError: ((String) -> Unit)? = null


    fun addEditReminders(date: String, time: String, message: String) = withState { state ->
        if (!Validators().validateReminders(date, time, message)) {
            onShowError?.invoke(Validators.errorMessage.toString())
        } else {
            val params = HashMap<String, String>()
            params[ApiParams.USER_ID.value()] = Prefs.getString(Constants.USER_ID, "")
            state.parent?.id?.let { noteID ->
                params[ApiParams.REMINDER_ID.value()] = noteID.toString()
            }
            params[ApiParams.DATE.value()] = buildString {
                append(date)
                append(" ")
                append(time)
            }
            params[ApiParams.MESSAGE.value()] = message

            viewModelScope.launch {
                if (state.parent?.id == null)
                  addReminder(params)
                else updateReminder(params)
            }
        }
    }

    private fun addReminder(params : HashMap<String, String>) =
        repository::addReminderData.asFlow(params)
        .execute {
            when (it) {
                is Loading -> copy(request = Loading())
                is Fail -> {
                    it.error.printStackTrace()
                    copy(request = Fail(it.error))
                }

                is Success -> {
                    copy(request = Success(it()))
                }

                else -> {
                    this
                }
            }
        }

    private fun updateReminder(params : HashMap<String, String>) =
        repository::updateReminders.asFlow(params)
        .execute {
            when (it) {
                is Loading -> copy(request = Loading())
                is Fail -> {
                    it.error.printStackTrace()
                    copy(request = Fail(it.error))
                }

                is Success -> {
                    copy(request = Success(it()))
                }

                else -> {
                    this
                }
            }
        }

    companion object : MavericksViewModelFactory<AddEditRemindersViewModel, AddEditRemindersViewState> {

        override fun create(
            viewModelContext: ViewModelContext, state: AddEditRemindersViewState
        ) = AddEditRemindersViewModel(state, viewModelContext.activity(), ClientRepository(context = viewModelContext.activity()))
    }


}