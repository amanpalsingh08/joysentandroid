package com.xperienceclientnurture.ui.home.ui.addeditclient

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentRequestConnectionBinding
import com.xperienceclientnurture.interfaces.DialogClickListener
import com.xperienceclientnurture.ui.commonmodel.UsersConnectionDetails
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.Utils
import com.xperienceclientnurture.utils.showCustomDialog
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RequestConnection :
    BaseFragment<FragmentRequestConnectionBinding, RequestConnectionViewModel>(),
    AdapterView.OnItemSelectedListener, View.OnClickListener, DialogClickListener {
    private lateinit var listConnections: List<UsersConnectionDetails>
    override val fragmentLayoutId: Int
        get() = R.layout.fragment_request_connection
    override val viewModelClass: Class<RequestConnectionViewModel>
        get() = RequestConnectionViewModel::class.java

    var userConnections = arrayOfNulls<String>(0)

    private var selfContriList =
        arrayOf("0%", "10%", "20%", "30%", "40%", "50%", "60%", "70%", "80%", "90%", "100%")

    var conn1ContriList = listOf<String>()
    var conn2ContriList = listOf<String>()


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        setObservers()

        setViewListeners()

        viewModel.clientID = bundle!!.getString(Constants.ARGS_CLIENT_ID, "")

        viewModel.getUserConnectionList()

        CoroutineScope(Dispatchers.IO).launch {
            viewModel.getClientRequestDetails()
            viewModel.getPaymentDetails()
        }

        setSelfContributionSpinner()
    }

    private fun setViewListeners() {
        binding.tvPartner1.setOnClickListener(this)
        binding.tvPartner2.setOnClickListener(this)
        binding.tvSearchPartner.setOnClickListener(this)
        binding.tvBack.setOnClickListener(this)
        binding.tv100Percent.setOnClickListener(this)
        binding.tvCoBrand.setOnClickListener(this)

        binding.viewConnectionCompensation.tvDefaultCompensation.setOnClickListener(this)
        binding.viewConnectionCompensation.tvCustomCompensation.setOnClickListener(this)
        binding.viewConnectionCompensation.tvRequest.setOnClickListener(this)

    }

    private fun setObservers() {

        viewModel.progress.observe(this, Observer {

            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }

        })

        viewModel.updateConnections.observe(this, Observer {

            if (it != null) {
                listConnections = it
                userConnections = arrayOfNulls(it.size)
                for (s in it.indices) {
                    userConnections[s] = it[s].firstname + " " + it[s].lastname
                }
                updateSpinnerData()
                viewModel.updateConnections.value = null
            }


        })

        viewModel.observerClientRequest.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                when (it.statusCode) {
                    200 -> {
                        binding.clPartners.visibility = View.VISIBLE
                        setClientDetails(it.data)
                    }
                    400 -> {
                        binding.clPartners.visibility = View.GONE
                    }
                }
                viewModel.observerClientRequest.value = null
            }

        })

        viewModel.observerPaymentDetails.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                setPaymentDetails(it)
                viewModel.observerPaymentDetails.value = null
            }

        })

        viewModel.listener.observe(this, Observer {

            if (it != null) {

                when (it) {
                    Constants.BACK -> {
                        requireActivity().sendBroadcast(Intent(Constants.RECEIVER_MY_CLIENT))
                        requireActivity().onBackPressed()
                    }
                }
                viewModel.listener.value = null
            }

        })

    }

    @SuppressLint("SetTextI18n")
    private fun setClientDetails(details: ClientRequestData) {
        binding.tvPartnerOne.text = details.connection1
        binding.tvPartnerTwo.text = details.connection2
        binding.tvPartnerThree.text = details.connection3

        binding.tvCompensationOne.text = "${details.connection1Contri}%"
        binding.tvCompensationTwo.text = "${details.connection2Contri}%"
        binding.tvCompensationThree.text = "${details.connection3Contri}%"

        binding.tvStatusOne.text = details.connection1Status
        binding.tvStatusTwo.text = details.connection1Status
        binding.tvStatusThree.text = details.connection1Status
    }

    @SuppressLint("SetTextI18n")
    private fun setPaymentDetails(paymentData: PaymentData?) {
        paymentData?.let {
            binding.tvTitleGiftsValue.text = it.giftsSchedule.size.toString()
            binding.tvTitlePricePerGiftValue.text = "$ ${it.paymentDetail.pricePerGift}"
            binding.tvTitleTaxValue.text = it.paymentDetail.tax
            binding.tvTitleTotalValue.text = "$ ${it.paymentDetail.total}"

            binding.tvTitleGift1Value.text = "${it.giftsSchedule[0].type} (${it.giftsSchedule[0].schedule})";
            binding.tvTitleGift2Value.text = "${it.giftsSchedule[1].type} (${it.giftsSchedule[1].schedule})"
            binding.tvTitleGift3Value.text = "${it.giftsSchedule[2].type} (${it.giftsSchedule[2].schedule})"
            binding.tvTitleGift4Value.text = "${it.giftsSchedule[3].type} (${it.giftsSchedule[3].schedule})"
            binding.tvTitleGift5Value.text = "${it.giftsSchedule[4].type} (${it.giftsSchedule[4].schedule})"
        }
    }


    private fun updateSpinnerData() {
        val arrayAdapter =
            ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, userConnections)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.viewConnectionCompensation.spinnerConnection1.adapter = arrayAdapter
        binding.viewConnectionCompensation.spinnerConnection2.adapter = arrayAdapter
        binding.viewConnectionCompensation.spinnerConnection1.onItemSelectedListener = this@RequestConnection
        binding.viewConnectionCompensation.spinnerConnection2.onItemSelectedListener = this@RequestConnection
        binding.viewConnectionCompensation.spinnerConnection1.setSelection(0)
        binding.viewConnectionCompensation.spinnerConnection2.setSelection(1)

    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        when (p0!!.id) {
            R.id.spinner_connection1 -> {
//                if (viewModel.connection2 != listConnections[p2].id.toString())
                    viewModel.connection1Value = listConnections[p2].id.toString()
                /*else
                    binding.viewConnectionCompensation.spinner_connection1.setSelection(0)*/

            }
            R.id.spinner_connection2 -> {
//                if (viewModel.connection1 != listConnections[p2].id.toString())
                    viewModel.connection2Value = listConnections[p2].id.toString()
                /*else
                    binding.viewConnectionCompensation.spinner_connection2.setSelection(0)*/

            }

            R.id.spinner_self_contribution -> {
                viewModel.selfContri = selfContriList[p2].replace("%", "")
                if (viewModel.isConnection2)
                    conn1ContriList = Utils().getList(viewModel.selfContri.toInt())
                else {
                    val total = 100 - (viewModel.selfContri.toInt())
                    val list: MutableList<String> = arrayListOf()
                    list.add(total.toString() + "%")
                    conn1ContriList = list
                }
                setConnection1ContributionSpinner()
            }
            R.id.spinner_connection1_contribution -> {
                viewModel.connection1Contri = conn1ContriList[p2].replace("%", "")
                val total =
                    100 - (viewModel.selfContri.toInt() + viewModel.connection1Contri.toInt())
                val list: MutableList<String> = arrayListOf()
                list.add(total.toString() + "%")
                conn2ContriList = list
                setConnection2ContributionSpinner()
            }
            R.id.spinner_connection2_contribution -> {
                viewModel.connection2Contri = conn2ContriList[p2].replace("%", "")
            }
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {

            R.id.tv_partner1 -> {
                binding.viewConnectionCompensation.root.visibility = View.VISIBLE
                viewModel.isConnection2 = false
                viewModel.isConnection1 = true
                setConnection1View()
                viewModel.isDefault = true
                viewModel.isCustom = false
                setDefaultCompensateView()
            }
            R.id.tv_partner2 -> {
                binding.viewConnectionCompensation.root.visibility = View.VISIBLE
                viewModel.isConnection2 = true
                viewModel.isConnection1 = false
                setConnection2View()
                viewModel.isDefault = true
                viewModel.isCustom = false
                setDefaultCompensateView()

            }
            R.id.tv_search_partner -> {
                binding.viewConnectionCompensation.root.visibility = View.GONE
                binding.llPartners.visibility = View.GONE
                binding.ll100Percent.visibility = View.VISIBLE
            }
            R.id.tv_back -> {
                binding.viewConnectionCompensation.root.visibility = View.GONE
                binding.llPartners.visibility = View.GONE
                binding.ll100Percent.visibility = View.VISIBLE
            }
            R.id.tv_100_percent -> {
                showCustomDialog(
                    requireContext(),
                    Constants.LOGOUT,
                    Constants.ONE_CLICK_LISTENER,
                    this, getString(R.string.pay_100_percent_payment),
                    getString(R.string.txt_pay_payment),
                    "",
                    getString(R.string.pay_now)
                )
            }
            R.id.tv_co_brand -> {
                binding.ll100Percent.visibility = View.GONE
                binding.llPartners.visibility = View.VISIBLE
            }
            R.id.tv_default_compensation -> {
                viewModel.isDefault = true
                viewModel.isCustom = false
                setDefaultCompensateView()
            }
            R.id.tv_custom_compensation -> {
                viewModel.isDefault = false
                viewModel.isCustom = true
                setCustomCompensateView()
            }
            R.id.tv_request -> {
                viewModel.submitRequest()

            }
        }
    }

    private fun setConnection1View() {

        binding.viewConnectionCompensation.tvTitleConnections.text =
            getString(R.string.one_connection)

        binding.viewConnectionCompensation.tvTitleConnectionOne.text =
            getString(R.string.choose_connection)

        binding.viewConnectionCompensation.rlSpinnerConnection2.visibility = View.GONE

        binding.viewConnectionCompensation.rlSpinnerConnection2Contribution.visibility =
            View.GONE

        binding.viewConnectionCompensation.spinnerConnection1Contribution.isEnabled = false

    }

    private fun setConnection2View() {

        binding.viewConnectionCompensation.tvTitleConnections.text =
            getString(R.string.two_connections)

        binding.viewConnectionCompensation.tvTitleConnectionOne.text =
            getString(R.string.connection_1)

        binding.viewConnectionCompensation.tvTitleConnectionTwo.text =
            getString(R.string.connection_2)

        binding.viewConnectionCompensation.rlSpinnerConnection2.visibility = View.VISIBLE

        binding.viewConnectionCompensation.rlSpinnerConnection2Contribution.visibility =
            View.VISIBLE

        binding.viewConnectionCompensation.spinnerConnection2Contribution.isEnabled = false
    }

    private fun setDefaultCompensateView() {
        binding.viewConnectionCompensation.rlSpinnerSelfContribution.visibility =
            View.GONE
        binding.viewConnectionCompensation.rlSpinnerConnection1Contribution.visibility =
            View.GONE
        binding.viewConnectionCompensation.rlSpinnerConnection2Contribution.visibility =
            View.GONE
    }

    private fun setCustomCompensateView() {

        if (viewModel.isConnection1) {
            binding.viewConnectionCompensation.rlSpinnerSelfContribution.visibility =
                View.VISIBLE
            binding.viewConnectionCompensation.rlSpinnerConnection1Contribution.visibility =
                View.VISIBLE
            binding.viewConnectionCompensation.rlSpinnerConnection2Contribution.visibility =
                View.GONE
        } else if (viewModel.isConnection2) {
            binding.viewConnectionCompensation.rlSpinnerConnection1Contribution.isEnabled = true
            binding.viewConnectionCompensation.rlSpinnerSelfContribution.visibility =
                View.VISIBLE
            binding.viewConnectionCompensation.rlSpinnerConnection1Contribution.visibility =
                View.VISIBLE
            binding.viewConnectionCompensation.rlSpinnerConnection2Contribution.visibility =
                View.VISIBLE
        }


    }

    private fun setSelfContributionSpinner() {
        val arrayAdapter =
            ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, selfContriList)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.viewConnectionCompensation.spinnerSelfContribution.adapter = arrayAdapter
        binding.viewConnectionCompensation.spinnerSelfContribution.onItemSelectedListener =
            this
        binding.viewConnectionCompensation.spinnerSelfContribution.setSelection(0)
    }

    private fun setConnection1ContributionSpinner() {


        val arrayAdapter =
            ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, conn1ContriList)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.viewConnectionCompensation.spinnerConnection1Contribution.adapter = arrayAdapter
        binding.viewConnectionCompensation.spinnerConnection1Contribution.onItemSelectedListener =
            this
        binding.viewConnectionCompensation.spinnerConnection1Contribution.setSelection(0)
    }

    private fun setConnection2ContributionSpinner() {
        val arrayAdapter =
            ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, conn2ContriList)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.viewConnectionCompensation.spinnerConnection2Contribution.adapter = arrayAdapter
        binding.viewConnectionCompensation.spinnerConnection2Contribution.onItemSelectedListener =
            this
        binding.viewConnectionCompensation.spinnerConnection2Contribution.setSelection(0)
    }

    override fun onDialogYesClick(click: String) {
        when (click) {
            getString(R.string.pay_now) -> {
                viewModel.pay100Percent()
            }
        }
    }

    override fun onDialogNoClick(click: String) {

    }

}
