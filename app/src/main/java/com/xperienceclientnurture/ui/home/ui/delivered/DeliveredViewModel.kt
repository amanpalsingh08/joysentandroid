package com.xperienceclientnurture.ui.home.ui.delivered

import android.util.Log
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.utils.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class DeliveredViewModel : BaseViewModel() {

    val updateUI = SingleLiveData<List<DeliveredClientDetails>>()
    val progress = SingleLiveData<Boolean>()
    val noData = SingleLiveData<Boolean>()


    fun getDeliveredClients() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getDeliveredClients(Prefs.getString(Constants.USER_ID, ""))
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                if (it.status.equals(Constants.ERROR, ignoreCase = true)) {
                                    noData.postValue(true)
                                } else {
                                    val list = it.data ?: arrayListOf()
                                    if (list.isEmpty()) {
                                        noData.postValue(true)
                                    } else {
                                        noData.postValue(false)
                                    }
                                    updateUI.postValue(list)
                                }
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }
}
