package com.xperienceclientnurture.ui.home.ui.clientnotes

import com.airbnb.mvrx.Async
import com.airbnb.mvrx.MavericksState
import com.airbnb.mvrx.Uninitialized
import com.xperienceclientnurture.ui.home.ui.addeditclient.ClientDetails
import com.xperienceclientnurture.ui.home.ui.addeditclient.ClientsDataModel
import retrofit2.Response

data class AddClientNotesViewState(
    val parent: ClientNotes? = null,
    val request: Async<ResultClientNotes> = Uninitialized,
    val requestClients: Async<Response<ClientsDataModel>> = Uninitialized,
    val listClients: List<ClientDetails> = emptyList()
) : MavericksState {

    constructor(target : ClientNotes) : this (parent = target)

    fun update(response: Response<ClientsDataModel>): AddClientNotesViewState {
        return copy(listClients = response.body()?.data ?: emptyList())
    }
}
