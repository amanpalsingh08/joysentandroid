package com.xperienceclientnurture.ui.home.ui.delivered

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentDeliveredBinding
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.home.HomeScreenActivity
import com.xperienceclientnurture.ui.home.ui.adapter.DeliveredClientsAdapter
import com.xperienceclientnurture.utils.Constants

class DeliveredFragment : BaseFragment<FragmentDeliveredBinding, DeliveredViewModel>(),
    OnItemClickListener {
    override val fragmentLayoutId: Int
        get() = R.layout.fragment_delivered
    override val viewModelClass: Class<DeliveredViewModel>
        get() = DeliveredViewModel::class.java

    private lateinit var list: List<DeliveredClientDetails>
    private lateinit var adapter: DeliveredClientsAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        setObservers()

        list = arrayListOf()
        adapter = DeliveredClientsAdapter(list)

        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        adapter.setItemSelectedListener(this)
        viewModel.getDeliveredClients()
    }

    private fun setObservers() {
        viewModel.updateUI.observe(viewLifecycleOwner) {
            if (it != null) {
                setData(it)
                viewModel.updateUI.value = null
            }
        }

        viewModel.progress.observe(viewLifecycleOwner) {

            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }

        }

        viewModel.noData.observe(viewLifecycleOwner) {

            if (it) {
                binding.noDataFound.root.visibility = View.VISIBLE
            } else {
                binding.noDataFound.root.visibility = View.GONE
            }

        }
    }

    override fun onClick(position: Int, type: String) {
        when (type) {
            Constants.SEND_EMAIL -> {
                (activity as HomeScreenActivity).sendEmail(
                    list[position].email,
                    "Xperience Client Nurture", "You are our potential client."
                )
            }

            Constants.SEND_CALL -> {
                (activity as HomeScreenActivity).call(list[position].phone)
            }
        }
    }

    private fun setData(it: List<DeliveredClientDetails>) {
        list = it
        adapter.updateList(list)
    }

}
