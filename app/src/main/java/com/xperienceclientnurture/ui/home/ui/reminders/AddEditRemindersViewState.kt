package com.xperienceclientnurture.ui.home.ui.reminders

import com.airbnb.mvrx.Async
import com.airbnb.mvrx.MavericksState
import com.airbnb.mvrx.Uninitialized

data class AddEditRemindersViewState(
    val parent: Reminders? = null,
    val request: Async<ResultReminders> = Uninitialized
) : MavericksState {

    constructor(target : Reminders) : this (parent = target)
}
