package com.xperienceclientnurture.ui.home.ui.notifications

import com.google.gson.annotations.SerializedName


data class GetRequestReceivedDataModel(
    @SerializedName("data")
    val `data`: ReceivedData,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)

data class ReceivedData(
    @SerializedName("card")
    val card: Int,
    @SerializedName("requests")
    val requests: List<RequestReceivedDetails>,
    @SerializedName("subscription_requests")
    val subscription_requests: List<SubscriptionRequest>
)

data class RequestReceivedDetails(
    @SerializedName("client_email")
    val client_email: String,
    @SerializedName("client_name")
    val client_name: String,
    @SerializedName("conn1")
    val conn1: String,
    @SerializedName("conn1_id")
    val conn1_id: String,
    @SerializedName("conn1_status")
    val conn1_status: String,
    @SerializedName("conn2")
    val conn2: String,
    @SerializedName("conn2_status")
    val conn2_status: String,
    @SerializedName("request_id")
    val request_id: Int,
    @SerializedName("request_status")
    val request_status: String
)

data class SubscriptionRequest(
    @SerializedName("client_email")
    val clientEmail: String,
    @SerializedName("client_name")
    val clientName: String,
    @SerializedName("conn1")
    val conn1: String,
    @SerializedName("conn1_id")
    val conn1Id: Int,
    @SerializedName("conn1_status")
    val conn1Status: String,
    @SerializedName("conn2")
    val conn2: String,
    @SerializedName("conn2_status")
    val conn2Status: String,
    @SerializedName("request_id")
    val requestId: Int,
    @SerializedName("request_status")
    val requestStatus: String,
    @SerializedName("type")
    val type: String
)