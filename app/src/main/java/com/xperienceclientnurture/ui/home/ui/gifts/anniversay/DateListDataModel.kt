package com.xperienceclientnurture.ui.home.ui.gifts.anniversay

import com.google.gson.annotations.SerializedName

data class DateListDataModel(
    @SerializedName("data")
    val `data`: List<String>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)