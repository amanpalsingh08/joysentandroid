package com.xperienceclientnurture.ui.home.ui.addeditclient

import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.textfield.TextInputEditText
import com.xperienceclientnurture.R
import com.xperienceclientnurture.utils.DateTimePicker

fun AddEditClient.setSpouseData() {

    val arrayAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, gender)
    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    binding.layoutSpouse.spinnerSpouseGender.adapter = arrayAdapter
    binding.layoutSpouse.spinnerSpouseGender.onItemSelectedListener = this
    binding.layoutSpouse.spinnerSpouseGender.setSelection(0)

    binding.layoutSpouse.tieSpouseAnniversary.setOnClickListener(this)
    binding.layoutSpouse.tieSpouseBirthday.setOnClickListener(this)

}

fun AddEditClient.updateSpouseData(spouseChildren: SpouseChildren?) {

    spouseChildren?.let { spouseInfo ->
        binding.layoutSpouse.tieSpouseName.setText(spouseInfo.name ?: "")
        binding.layoutSpouse.tieSpouseBirthday.setText(spouseInfo.birthday ?: "")
        binding.layoutSpouse.tieSpouseAnniversary.setText(spouseInfo.anniversary ?: "")
        binding.layoutSpouse.tieSpouseAge.setText(spouseInfo.age ?: "")
        gender.forEachIndexed { index, obj ->
            if (obj.lowercase() == spouseInfo.gender?.lowercase())
                binding.layoutSpouse.spinnerSpouseGender.setSelection(index)
        }
    }
}

fun AddEditClient.updateChildData(children: List<SpouseChildren>?) {

    children?.forEach { childInfo ->
        setChildData(childInfo)
    }
}

fun AddEditClient.setChildData(childData: SpouseChildren? = null) {
    addChild(childData)
    binding.btnAddChild.setOnClickListener {
        addChild()
    }
}

fun AddEditClient.addChild(childData: SpouseChildren? = null) {
    val child = this.layoutInflater.inflate(R.layout.layout_child_information, null, false) as ConstraintLayout
    if (listChilds.isNotEmpty()) {
        child.findViewById<Button>(R.id.btn_delete).apply {
            visibility = View.VISIBLE
            setOnClickListener {
                val childView = listChilds.find { it == child }
                binding.llChild.removeView(childView)
            }
        }
    }
    var genderPosition = 0
    childData?.gender?.let { gend ->
        gender.forEachIndexed { index, obj ->
            if (obj.lowercase() == gend.lowercase())
                genderPosition = index
        }
    }

    setGenderSpinner(child.findViewById(R.id.spinner_gender), genderPosition)

    child.findViewById<TextInputEditText>(R.id.tie_name).setText(childData?.name ?: "")
    child.findViewById<TextInputEditText>(R.id.tie_age).setText(childData?.age ?: "")
    child.findViewById<TextInputEditText>(R.id.tie_birthday).setText(childData?.birthday ?: "")

    child.findViewById<TextInputEditText>(R.id.tie_birthday).setOnClickListener {
        DateTimePicker.showDatePicker(requireContext(), this, child)
    }
    binding.llChild.addView(child)
    listChilds.add(child)

}

private fun AddEditClient.setGenderSpinner(spinner: Spinner, genderPosition: Int) {
    val arrayAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, gender)
    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    spinner.adapter = arrayAdapter
    spinner.onItemSelectedListener = this
    spinner.setSelection(genderPosition)
}


