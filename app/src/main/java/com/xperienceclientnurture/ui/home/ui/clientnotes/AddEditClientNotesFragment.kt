package com.xperienceclientnurture.ui.home.ui.clientnotes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.MavericksView
import com.airbnb.mvrx.Success
import com.airbnb.mvrx.activityViewModel
import com.airbnb.mvrx.withState
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseMavericksFragment
import com.xperienceclientnurture.databinding.FragmentAddEditClientNotesBinding

class AddEditClientNotesFragment : BaseMavericksFragment(), MavericksView, AdapterView.OnItemSelectedListener {

    private lateinit var binding: FragmentAddEditClientNotesBinding
    private val viewModel: AddEditClientNotesViewModel by activityViewModel()
    private var viewLayout: View? = null
    private var position = 0
    private var userClients = mutableListOf<String>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        if (viewLayout == null) {
            binding = FragmentAddEditClientNotesBinding.inflate(inflater, container, false)
            viewLayout = binding.root
        }
        return viewLayout as View
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rlSubmit.setOnClickListener {
            it.hideSoftInput()
            viewModel.addEditClientNote(binding.tieNotes.text.toString().trim())
        }

        viewModel.onShowError = {
            showSnack(binding.parent, it)
        }

        withState(viewModel) { state ->
            binding.tieNotes.setText(state.parent?.notes ?: "")
        }

        viewModel.getClients()
    }

    override fun invalidate() = withState(viewModel) { state ->
        binding.flLoading.isVisible = state.requestClients is Loading || state.request is Loading

        if (state.request is Success && state.request.complete) {
            onUpdatedNotes?.invoke(true)
            requireActivity().onBackPressed()
        }

        if (state.listClients.isNotEmpty()) {
            state.listClients.forEachIndexed { index, data ->
                if (data.id == state.parent?.clientID?.toInt()) position = index
                userClients.add(data.firstname?:"")
            }
            updateSpinnerData()
            binding.spinnerClients.isEnabled = state.parent?.clientID == null
        }
    }

    private fun updateSpinnerData() {
        val arrayAdapter =
            ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, userClients)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerClients.adapter = arrayAdapter
        binding.spinnerClients.onItemSelectedListener = this
        binding.spinnerClients.setSelection(position)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent?.id) {
            R.id.spinner_clients -> {
                withState(viewModel) { state ->
                    viewModel.clientID = state.listClients[position].id.toString()
                }
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

}