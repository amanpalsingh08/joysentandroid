package com.xperienceclientnurture.ui.home.ui.gifts.anniversay

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.CompoundButton
import androidx.lifecycle.Observer
import com.xperienceclientnurture.sharedpreference.Prefs
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentCustomerAnniversaryDetailBinding
import com.xperienceclientnurture.interfaces.DialogClickListener
import com.xperienceclientnurture.ui.home.ui.HolderActivity
import com.xperienceclientnurture.ui.login.LoginDataModel
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.showCustomDialog
import com.slider.sliderimage.adapters.ViewPagerAdapter

class CustomerAnniversaryDetailFragment :
    BaseFragment<FragmentCustomerAnniversaryDetailBinding, AnniversaryDetailViewModel>(),
    View.OnClickListener, CompoundButton.OnCheckedChangeListener, DialogClickListener {
    override fun onDialogYesClick(click: String) {
        startActivity(
            Intent(activity, HolderActivity::class.java)
                .putExtra(Constants.SCREEN_NAME, Constants.SCREEN_ADD_CARD)
                .putExtra(Constants.ARGS_IS_EDIT, false)
        )
    }

    override fun onDialogNoClick(click: String) {
    }


    override val fragmentLayoutId: Int
        get() = R.layout.fragment_customer_anniversary_detail
    override val viewModelClass: Class<AnniversaryDetailViewModel>
        get() = AnniversaryDetailViewModel::class.java

    private lateinit var anniversaryDetail: AnniversaryDetailDataModel
    private var giftID = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bundle.let {
            giftID = it?.getString(Constants.GIFT_ID, "") ?: ""
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        setObservers()

        setViewListeners()

        val data: AnniversaryDataList = bundle!!.getParcelable(Constants.HOME_DECOR_MODEL)!!

        setDetails(data)
    }


    private fun setViewListeners() {
        binding.tvBuy.setOnClickListener(this)
        binding.tvPurchaseNow.setOnClickListener(this)
        binding.tvCancel.setOnClickListener(this)
        binding.cbSameAddress.setOnCheckedChangeListener(this)

    }

    private fun setObservers() {


        viewModel.progress.observe(this, Observer {
            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }
        })
        viewModel.listener.observe(this, Observer { })


        viewModel.updateDetail.observe(this, Observer {

            if (it != null) {
                anniversaryDetail = it
                setDetails(anniversaryDetail)
                viewModel.updateDetail.value = null
            }
        })

        viewModel.purchaseSuccess.observe(this, Observer {
            if (it != null) {
                showToast(it.message)
                binding.constraintLayout.visibility = View.GONE
                binding.tvPurchaseNow.visibility = View.VISIBLE
                (activity as HolderActivity).toolbarTitleHolder.text =
                    getString(R.string.anniversary_gifts)
                activity!!.onBackPressed()
                viewModel.purchaseSuccess.value = null
            }
        })

        viewModel.navigateCardScreen.observe(this, Observer {
            if (it) {
                showCustomDialog(
                    requireContext(),
                    dialogType = Constants.LOGOUT,
                    dialogClickType = Constants.TWO_CLICK_LISTENER,
                    dialogClickListener = this,
                    message = "Please add card first!",
                    title = "Error",
                    yes = "Okay"
                )
                viewModel.navigateCardScreen.value = false

            }
        })
    }


    override fun onClick(v: View?) {

        when (v!!.id) {
            R.id.tv_buy -> {
                viewModel.purchaseCustomer(giftID)
            }
            R.id.tv_cancel -> {
                binding.constraintLayout.visibility = View.GONE
                binding.tvPurchaseNow.visibility = View.VISIBLE
                binding.clAddress.visibility = View.VISIBLE
            }
            R.id.tv_purchase_now -> {
                if (viewModel.addressObj.firstName.isNotEmpty()
                    && viewModel.addressObj.lastName.isNotEmpty()
                    && viewModel.addressObj.email.isNotEmpty()
                    && viewModel.addressObj.phone.isNotEmpty()
                    && viewModel.addressObj.address.isNotEmpty()
                    && viewModel.addressObj.message.isNotEmpty()
                    && viewModel.addressObj.city.isNotEmpty()
                    && viewModel.addressObj.state.isNotEmpty()
                    && viewModel.addressObj.zipCode.isNotEmpty()
                )
                    viewModel.getCustomerShippingPrice(giftID)
                else
                    showToast(getString(R.string.please_fill_all_the_fields))
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setDetails(anniversaryDetail: AnniversaryDetailDataModel) {

        binding.constraintLayout.visibility = View.VISIBLE
        binding.tvPurchaseNow.visibility = View.GONE
        binding.clAddress.visibility = View.GONE

        binding.tvTotalPrice.text = "\$ ${anniversaryDetail.data.total}"
        binding.tvQuantity.text = "\$ ${anniversaryDetail.data.quantity}"
        binding.tvTax.text = "\$ ${anniversaryDetail.data.tax}"
        binding.tvShippingPrice.text = "\$ ${anniversaryDetail.data.shipping}"


    }


    @SuppressLint("SetTextI18n")
    private fun setDetails(anniversaryDataList: AnniversaryDataList) {
        binding.tvName.text = anniversaryDataList.gift_name
        binding.tvHeight.text = anniversaryDataList.height
        binding.tvWeight.text = anniversaryDataList.weight
        binding.tvLength.text = anniversaryDataList.length

        binding.tvDesription.text = anniversaryDataList.description

        binding.tvPrice.text = "\$ ${anniversaryDataList.price}"
        binding.tvBasePrice.text = "\$ ${anniversaryDataList.price}"

        binding.viewPagerFullScreen.adapter =
            ViewPagerAdapter(
                context = binding.viewPagerFullScreen.context,
                items = anniversaryDataList.img
            )
        binding.viewPagerFullScreen.setCurrentItem(0, true)
        binding.indicatorScreen.setViewPager(binding.viewPagerFullScreen)


    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        if (buttonView!!.isPressed) {
            when (buttonView.id) {
                R.id.cb_same_address -> {
                    if (isChecked) {
                        val gson = Gson()
                        val jsonString = Prefs.getString(Constants.KEY_LOGIN_OBJ, "")
                        val loginObj = gson.fromJson<LoginDataModel.Data>(
                            jsonString,
                            object : TypeToken<LoginDataModel.Data>() {}.type
                        )

                        viewModel.addressObj = CustomerAddressModel(
                            firstName = loginObj.firstname,
                            lastName = loginObj.lastname,
                            email = loginObj.email,
                            city = loginObj.city ?: "",
                            address = loginObj.address ?: "",
                            state = loginObj.state ?: "",
                            zipCode = loginObj.zip_code ?: ""

                        )

                    } else
                        viewModel.addressObj = CustomerAddressModel()

                    binding.dataObj = viewModel.addressObj
                }
            }
        }
    }


}


