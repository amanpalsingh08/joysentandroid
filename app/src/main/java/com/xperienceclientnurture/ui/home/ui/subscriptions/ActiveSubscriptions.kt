package com.xperienceclientnurture.ui.home.ui.subscriptions

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class ActiveSubscriptions(
    @Json(name = "id") @field:Json(name = "id")
    val id: Int? = null,
    @Json(name = "client_id") @field:Json(name = "client_id")
    val clientId: String? = null,
    @Json(name = "sender_id") @field:Json(name = "sender_id")
    val senderId: String? = null,
    @Json(name = "receiver_id") @field:Json(name = "receiver_id")
    val receiverId: String? = null,
    @Json(name = "referer_id") @field:Json(name = "referer_id")
    val refererId: Int? = null,
    @Json(name = "sender_contri") @field:Json(name = "sender_contri")
    val senderContri: Int? = null,
    @Json(name = "receiver_contri") @field:Json(name = "receiver_contri")
    val receiverContri: Int? = null,
    @Json(name = "referer_contri") @field:Json(name = "referer_contri")
    val refererContri: Int? = null,
    @Json(name = "sender_status") @field:Json(name = "sender_status")
    val senderStatus: Int? = null,
    @Json(name = "receiver_status") @field:Json(name = "receiver_status")
    val receiverStatus: Int? = null,
    @Json(name = "status") @field:Json(name = "status")
    val status: String? = null,
    @Json(name = "gifts") @field:Json(name = "gifts")
    val gifts: String? = null,
    @Json(name = "frequency") @field:Json(name = "frequency")
    val frequency: String? = null,
    @Json(name = "total") @field:Json(name = "total")
    val total: String? = null,
    @Json(name = "type") @field:Json(name = "type")
    val type: String? = null,
    @Json(name = "months") @field:Json(name = "months")
    val months: String? = null,
    @Json(name = "year_num") @field:Json(name = "year_num")
    val yearNum: Int? = null,
    @Json(name = "created_at") @field:Json(name = "created_at")
    val createdAt: String? = null,
    @Json(name = "updated_at") @field:Json(name = "updated_at")
    val updatedAt: String? = null,
    @Json(name = "userid") @field:Json(name = "userid")
    val userId: String? = null,
    @Json(name = "user_type") @field:Json(name = "user_type")
    val userType: String? = null,
    @Json(name = "firstname") @field:Json(name = "firstname")
    val firstName: String? = null,
    @Json(name = "lastname") @field:Json(name = "lastname")
    val lastName: String? = null,
    @Json(name = "name") @field:Json(name = "name")
    val name: String? = null,
    @Json(name = "stage") @field:Json(name = "stage")
    val stage: String? = null,
    @Json(name = "lead_source") @field:Json(name = "lead_source")
    val leadSource: String? = null,
    @Json(name = "assigned_to") @field:Json(name = "assigned_to")
    val assignedTo: String? = null,
    @Json(name = "email") @field:Json(name = "email")
    val email: String? = null,
    @Json(name = "profile_image") @field:Json(name = "profile_image")
    val profileImage: String? = null,
    @Json(name = "phone") @field:Json(name = "phone")
    val phone: String? = null,
    @Json(name = "phone_type") @field:Json(name = "phone_type")
    val phoneType: String? = null,
    @Json(name = "phone2") @field:Json(name = "phone2")
    val phone2: String? = null,
    @Json(name = "phone_type2") @field:Json(name = "phone_type2")
    val phoneType2: String? = null,
    @Json(name = "address") @field:Json(name = "address")
    val address: String? = null,
    @Json(name = "city") @field:Json(name = "city")
    val city: String? = null,
    @Json(name = "state") @field:Json(name = "state")
    val state: String? = null,
    @Json(name = "zipcode") @field:Json(name = "zipcode")
    val zipcode: String? = null,
    @Json(name = "country") @field:Json(name = "country")
    val country: String? = null,
    @Json(name = "shipping_address") @field:Json(name = "shipping_address")
    val shippingAddress: String? = null,
    @Json(name = "shipping_city") @field:Json(name = "shipping_city")
    val shippingCity: String? = null,
    @Json(name = "shipping_state") @field:Json(name = "shipping_state")
    val shippingState: String? = null,
    @Json(name = "shipping_zipcode") @field:Json(name = "shipping_zipcode")
    val shippingZipCode: String? = null,
    @Json(name = "shipping_country") @field:Json(name = "shipping_country")
    val shippingCountry: String? = null,
    @Json(name = "group") @field:Json(name = "group")
    val group: String? = null,
    @Json(name = "closing_date") @field:Json(name = "closing_date")
    val closingDate: String? = null
) : Parcelable