package com.xperienceclientnurture.ui.home.ui.disabled

import com.google.gson.annotations.SerializedName

data class GetDisabledClientDataModel(
    @SerializedName("data")
    val `data`: List<DisabledClientDetails>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)

data class DisabledClientDetails(
    @SerializedName("address")
    val address: String,
    @SerializedName("city")
    val city: String,
    @SerializedName("client_name")
    val client_name: String,
    @SerializedName("con1")
    val con1: String,
    @SerializedName("con2")
    val con2: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("group")
    val group: String,
    @SerializedName("last_delivered")
    val last_delivered: String,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("state")
    val state: String
)