package com.xperienceclientnurture.ui.home.ui.clientnotes

import android.content.Context
import com.airbnb.mvrx.Fail
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.MavericksViewModel
import com.airbnb.mvrx.MavericksViewModelFactory
import com.airbnb.mvrx.Success
import com.airbnb.mvrx.ViewModelContext
import com.xperienceclientnurture.data.ClientRepository
import com.xperienceclientnurture.extensions.asFlow
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.utils.ApiParams
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.Validators
import kotlinx.coroutines.launch

class AddEditClientNotesViewModel(
    state: AddClientNotesViewState, val context: Context, private val repository: ClientRepository
) : MavericksViewModel<AddClientNotesViewState>(state) {

    var clientID = ""
    var onShowError: ((String) -> Unit)? = null

    fun getClients() =
        viewModelScope.launch {
            repository::userClients.asFlow(Prefs.getString(Constants.USER_ID, ""))
                .execute {
                    when (it) {
                        is Loading -> copy(requestClients = Loading())
                        is Fail -> {
                            it.error.printStackTrace()
                            copy(requestClients = Fail(it.error))
                        }

                        is Success -> {
                            update(it()).copy(requestClients = Success(it()))
                        }

                        else -> {
                            this
                        }
                    }
                }
        }

    fun addEditClientNote(note: String) = withState { state ->
        if (!Validators().validateClientNotes(clientID, note)) {
            onShowError?.invoke(Validators.errorMessage.toString())
        } else {
            val params = HashMap<String, String>()
            params[ApiParams.USER_ID.value()] = Prefs.getString(Constants.USER_ID, "")
            state.parent?.id?.let { noteID ->
                params[ApiParams.NOTE_ID.value()] = noteID.toString()
            }
            params[ApiParams.CLIENT_ID.value()] = clientID
            params[ApiParams.CLIENT_NOTES.value()] = note
            viewModelScope.launch {
                if (state.parent?.id == null)
                    addClientNote(params)
                else updateClientNote(params)

            }
        }
    }

    private fun addClientNote(params: HashMap<String, String>) = viewModelScope.launch {
        repository::addNoteData.asFlow(params)
            .execute {
                when (it) {
                    is Loading -> copy(request = Loading())
                    is Fail -> {
                        it.error.printStackTrace()
                        copy(request = Fail(it.error))
                    }

                    is Success -> {
                        copy(request = Success(it()))
                    }

                    else -> {
                        this
                    }
                }
            }
    }

    private fun updateClientNote(params: HashMap<String, String>) = viewModelScope.launch {
        repository::updateNote.asFlow(params)
            .execute {
                when (it) {
                    is Loading -> copy(request = Loading())
                    is Fail -> {
                        it.error.printStackTrace()
                        copy(request = Fail(it.error))
                    }

                    is Success -> {
                        copy(request = Success(it()))
                    }

                    else -> {
                        this
                    }
                }
            }
    }


    companion object : MavericksViewModelFactory<AddEditClientNotesViewModel, AddClientNotesViewState> {

        override fun create(
            viewModelContext: ViewModelContext, state: AddClientNotesViewState
        ) = AddEditClientNotesViewModel(state, viewModelContext.activity(), ClientRepository(context = viewModelContext.activity()))
    }


}