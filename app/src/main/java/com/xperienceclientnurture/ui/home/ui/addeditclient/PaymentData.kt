package com.xperienceclientnurture.ui.home.ui.addeditclient


import com.google.gson.annotations.SerializedName

data class PaymentData(
    @SerializedName("gifts_schedule")
    val giftsSchedule: List<GiftsSchedule>,
    @SerializedName("payment_detail")
    val paymentDetail: PaymentDetail
)