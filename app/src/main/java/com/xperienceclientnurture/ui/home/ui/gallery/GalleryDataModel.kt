package com.xperienceclientnurture.ui.home.ui.gallery
import com.google.gson.annotations.SerializedName


data class GalleryDataModel(
    @SerializedName("data")
    val `data`: List<GalleryDetails>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)

data class GalleryDetails(
    @SerializedName("description")
    val description: String,
    @SerializedName("img")
    val img: String
)