package com.xperienceclientnurture.ui.home.ui.recentclients
import com.google.gson.annotations.SerializedName


data class GetRecentClientsConnectionDataModel(
    @SerializedName("data")
    val `data`: List<RecentClientsConnectionDetails>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)

data class RecentClientsConnectionDetails(
    @SerializedName("address")
    val address: String,
    @SerializedName("assigned_to")
    val assigned_to: String,
    @SerializedName("city")
    val city: String,
    @SerializedName("client_id")
    val client_id: String,
    @SerializedName("country")
    val country: String,
    @SerializedName("created_at")
    val created_at: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("firstname")
    val firstname: String,
    @SerializedName("frequency")
    val frequency: String,
    @SerializedName("group")
    val group: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("lastname")
    val lastname: String,
    @SerializedName("lead_source")
    val leadSource: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("phone2")
    val phone2: Any,
    @SerializedName("phone_type")
    val phone_type: Any,
    @SerializedName("phone_type2")
    val phone_type2: Any,
    @SerializedName("profile_image")
    val profile_image: String,
    @SerializedName("receiver_contri")
    val receiver_contri: Int,
    @SerializedName("receiver_id")
    val receiver_id: String,
    @SerializedName("receiver_status")
    val receiver_status: Int,
    @SerializedName("referer_contri")
    val referer_contri: Int,
    @SerializedName("referer_id")
    val refererId: Int,
    @SerializedName("sender_contri")
    val sender_contri: Int,
    @SerializedName("sender_id")
    val sender_id: String,
    @SerializedName("sender_status")
    val sender_status: Int,
    @SerializedName("shipping_address")
    val shipping_address: Any,
    @SerializedName("shipping_city")
    val shipping_city: Any,
    @SerializedName("shipping_country")
    val shipping_country: String,
    @SerializedName("shipping_state")
    val shipping_state: Any,
    @SerializedName("shipping_zipcode")
    val shipping_zipcode: Any,
    @SerializedName("stage")
    val stage: String,
    @SerializedName("state")
    val state: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("updated_at")
    val updated_at: String,
    @SerializedName("userid")
    val userid: String,
    @SerializedName("zipcode")
    val zipcode: String
)