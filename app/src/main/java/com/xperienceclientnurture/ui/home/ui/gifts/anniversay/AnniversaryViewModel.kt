package com.xperienceclientnurture.ui.home.ui.gifts.anniversay

import android.util.Log
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class AnniversaryViewModel : BaseViewModel() {

    val progress = SingleLiveData<Boolean>()
    val listener = SingleLiveData<String>()
    val updateAnniversary = SingleLiveData<List<AnniversaryDataList>>()
    var giftType=""


    fun getAnniversary() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getAnniversaryList(giftType)
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                updateAnniversary.postValue(it.data)

                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }



}
