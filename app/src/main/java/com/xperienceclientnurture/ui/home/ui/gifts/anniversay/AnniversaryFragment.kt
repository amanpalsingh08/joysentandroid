package com.xperienceclientnurture.ui.home.ui.gifts.anniversay

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentAnniversaryBinding
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.home.ui.HolderActivity
import com.xperienceclientnurture.ui.home.ui.adapter.AnniversaryAdapter
import com.xperienceclientnurture.utils.Constants

class AnniversaryFragment : BaseFragment<FragmentAnniversaryBinding, AnniversaryViewModel>(),
    OnItemClickListener {

    override val fragmentLayoutId: Int
        get() = R.layout.fragment_anniversary
    override val viewModelClass: Class<AnniversaryViewModel>
        get() = AnniversaryViewModel::class.java

    private var listAnniversary: List<AnniversaryDataList> = arrayListOf()
    lateinit var adapter: AnniversaryAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        viewModel.giftType = bundle!!.getString(Constants.GIFT_TYPE)!!

        viewModel.getAnniversary()


        adapter = AnniversaryAdapter(listAnniversary)

        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        adapter.setItemSelectedListener(this)

        setObservers()

    }

    private fun setObservers() {

        viewModel.updateAnniversary.observe(this, Observer {

            if (it != null) {
                listAnniversary = it
                adapter.updateList(listAnniversary)
                viewModel.updateAnniversary.value = null

            }


        })
        viewModel.progress.observe(this, Observer {
            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }
        })
        viewModel.listener.observe(this, Observer { })


    }

    override fun onClick(position: Int, type: String) {
        when (type) {
            Constants.ANNIVERSARY_DETAIL -> {
                (activity as HolderActivity).ivShare.visibility = View.VISIBLE
                (activity as HolderActivity).toolbarTitleHolder.text =
                    getString(R.string.home_decor_details)
                val bundle = Bundle()
                bundle.putString(Constants.GIFT_ID, listAnniversary[position].id.toString())
                bundle.putParcelable(Constants.HOME_DECOR_MODEL, listAnniversary[position])
                if (Prefs.getString(Constants.USER_ROLE, "")
                        .equals(Constants.CUSTOMER, ignoreCase = false)
                )
                    replaceFragment(
                        CustomerAnniversaryDetailFragment::class.java,
                        null,
                        true,
                        bundle
                    )
                else
                    replaceFragment(AnniversaryDetailFragment::class.java, null, true, bundle)
            }
        }
    }

}
