package com.xperienceclientnurture.ui.home.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.xperienceclientnurture.R
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.forgotpassword.NotificationTitleModel

class NotificationTitleAdapter(var list: List<NotificationTitleModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var listener: OnItemClickListener.OnTitleClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_title,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setItemSelectedListener(listener: OnItemClickListener.OnTitleClickListener) {
        this.listener = listener
    }

    fun updateList(it: List<NotificationTitleModel>) {
        list = it
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        println("holder = [${holder}], position = [${position}]")

        (holder as ViewHolder).title.text = list[position].title

        if (list[position].isPressed) {
            holder.title.background =
                ContextCompat.getDrawable(holder.title.context, R.drawable.bg_blue_solid_4dp)
            holder.title.setTextColor(
                ContextCompat.getColor(
                    holder.title.context,
                    android.R.color.white
                )
            )
        } else {
            holder.title.background =
                ContextCompat.getDrawable(holder.title.context, R.drawable.bg_gray_solid_4dp)
            holder.title.setTextColor(
                ContextCompat.getColor(
                    holder.title.context,
                    android.R.color.black
                )
            )
        }

        holder.setItemSelectedListener(listener)

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        lateinit var listener: OnItemClickListener.OnTitleClickListener
        val title: TextView = itemView.findViewById(R.id.tv_title)

        init {
            title.setOnClickListener(this)
        }

        fun setItemSelectedListener(listener: OnItemClickListener.OnTitleClickListener) {
            this.listener = listener
        }

        override fun onClick(v: View?) {
            when (v!!.id) {
                R.id.tv_title -> {
                    listener.onTitleClick(adapterPosition, title.text.toString())
                }
            }
        }

    }
}