package com.xperienceclientnurture.ui.home.ui.clientnotes

import android.content.Context
import com.airbnb.mvrx.Fail
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.MavericksViewModel
import com.airbnb.mvrx.MavericksViewModelFactory
import com.airbnb.mvrx.Success
import com.airbnb.mvrx.ViewModelContext
import com.xperienceclientnurture.extensions.asFlow
import com.xperienceclientnurture.data.ClientRepository
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.utils.Constants
import kotlinx.coroutines.launch

class ClientNotesViewModel(
    state: ClientNotesViewState, val context: Context, private val repository: ClientRepository
) : MavericksViewModel<ClientNotesViewState>(state) {

    var noteID = ""

    fun getClientNotes() =
        viewModelScope.launch {
            repository::clientNotes.asFlow(Prefs.getString(Constants.USER_ID, ""))
                .execute {
                    when (it) {
                        is Loading -> copy(request = Loading())
                        is Fail -> {
                            it.error.printStackTrace()
                            copy(request = Fail(it.error))
                        }

                        is Success -> {
                            update(it()).copy(request = Success(it()))
                        }

                        else -> {
                            this
                        }
                    }
                }
        }

    fun deleteClientNote() =
        viewModelScope.launch {
            repository::deleteClientNote.asFlow(Prefs.getString(Constants.USER_ID, ""), noteID)
                .execute {
                    when (it) {
                        is Loading -> copy(requestDelete = Loading())
                        is Fail -> {
                            it.error.printStackTrace()
                            copy(requestDelete = Fail(it.error))
                        }

                        is Success -> {
                            getClientNotes()
                            copy(requestDelete = Success(it()))
                        }

                        else -> {
                            this
                        }
                    }
                }
        }

    companion object : MavericksViewModelFactory<ClientNotesViewModel, ClientNotesViewState> {

        override fun create(
            viewModelContext: ViewModelContext, state: ClientNotesViewState
        ) = ClientNotesViewModel(state, viewModelContext.activity(), ClientRepository(context = viewModelContext.activity()))
    }


}