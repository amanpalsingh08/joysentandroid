package com.xperienceclientnurture.ui.home.ui.viewclient

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.ui.home.ui.addeditclient.ClientDetails
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class ClientInfoViewModel : BaseViewModel() {


    val clientDetailsData = MutableLiveData<ClientDetails>()

    var clientID = ""
    val progress = SingleLiveData<Boolean>()
    val image = SingleLiveData<String>()
    val updateUI = SingleLiveData<ClientDetails>()

    fun getClientDetails() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            println("Client Id == $clientID")
            val response = service.getClientDetails(clientID)
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                clientDetailsData.value = it.data
                                updateUI.value = it.data
                                image.postValue(it.data.profile_image)
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                e.printStackTrace()
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                e.printStackTrace()
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

}
