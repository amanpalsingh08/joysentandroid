package com.xperienceclientnurture.ui.home.ui.addeditclient


import com.google.gson.annotations.SerializedName

data class GiftsSchedule(
    @SerializedName("gift")
    val gift: String,
    @SerializedName("schedule")
    val schedule: String,
    @SerializedName("type")
    val type: String
)