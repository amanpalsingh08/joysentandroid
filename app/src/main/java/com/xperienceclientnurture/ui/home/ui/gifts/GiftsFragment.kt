package com.xperienceclientnurture.ui.home.ui.gifts

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentGiftsBinding
import com.xperienceclientnurture.interfaces.DialogClickListener
import com.xperienceclientnurture.ui.commonmodel.CommonDataModel
import com.xperienceclientnurture.ui.commonmodel.UsersConnectionDetails
import com.xperienceclientnurture.ui.home.ui.addeditclient.ClientDetails
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.Utils
import com.xperienceclientnurture.utils.showCustomDialog
import kotlin.math.roundToInt

class GiftsFragment : BaseFragment<FragmentGiftsBinding, GiftsViewModel>(), View.OnClickListener,
    AdapterView.OnItemSelectedListener, DialogClickListener {

    var subscriptionType =
        arrayOf("Please Select", "Nurture Subscription", "Home Buyer Subscription")

    private var selfContriList =
        arrayOf("0%", "10%", "20%", "30%", "40%", "50%", "60%", "70%", "80%", "90%", "100%")

    private var conn1ContriList = listOf<String>()
    private var conn2ContriList = listOf<String>()

    override val fragmentLayoutId: Int
        get() = R.layout.fragment_gifts
    override val viewModelClass: Class<GiftsViewModel>
        get() = GiftsViewModel::class.java

    var userConnections = arrayOfNulls<String>(0)
    var arrayMyClients = arrayOfNulls<String>(0)
    private lateinit var listConnections: List<UsersConnectionDetails>
    private lateinit var listMyClients: List<ClientDetails>


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        setObservers()

        setSubscriptionTypeSpinner()

        setSelfContributionSpinner()

        setViewListeners()

        binding.tvGifts.text = getSpanData(getString(R.string.text_gift_number, viewModel.gifts), 0, 15)
        binding.tvFrequency.text = getSpanData(getString(R.string.text_frequency, viewModel.frequency), 0, 9)

        viewModel.getMyClients()

    }

    private fun setSubscriptionTypeSpinner() {
        val arrayAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, subscriptionType)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerSubscriptionType.adapter = arrayAdapter
        binding.spinnerSubscriptionType.onItemSelectedListener = this@GiftsFragment
        binding.spinnerSubscriptionType.setSelection(0)
    }

    private fun getSpanData(string: String, startIndex: Int, lastIndex: Int): SpannableString {
        val spannable = SpannableString(string)

        spannable.setSpan(StyleSpan(Typeface.BOLD), startIndex, lastIndex, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
        spannable.setSpan(ForegroundColorSpan(Color.BLACK), startIndex, lastIndex, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)

        return spannable
    }

    private fun setViewListeners() {
        binding.viewPaymentDetails.tv100Percent.setOnClickListener(this)
        binding.viewPaymentDetails.tvConnection1.setOnClickListener(this)
        binding.viewPaymentDetails.tvConnection2.setOnClickListener(this)
        binding.viewPaymentDetails.tvSearchClient.setOnClickListener(this)
        binding.viewPaymentDetails.tvBack.setOnClickListener(this)
        binding.viewPaymentDetails.tvCoBrand.setOnClickListener(this)

        binding.viewConnectionCompensation.tvDefaultCompensation.setOnClickListener(this)
        binding.viewConnectionCompensation.tvCustomCompensation.setOnClickListener(this)
        binding.viewConnectionCompensation.tvRequest.setOnClickListener(this)

    }

    private fun setObservers() {

        viewModel.updateConnections.observe(this) {

            if (it != null) {
                listConnections = it
                userConnections = arrayOfNulls(listConnections.size)
                for (s in listConnections.indices) {
                    userConnections[s] = "${listConnections[s].firstname} ${listConnections[s].lastname}"
                }
                updateClientConnectionSpinnerData()
                viewModel.updateConnections.value = null
            }


        }
        viewModel.progress.observe(this) {
            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }
        }
        viewModel.purchase.observe(this) {
            if (it!=null){
                showAlreadyPaidDialog(it)
                viewModel.purchase.value = null
            }
        }
        viewModel.updateClients.observe(this) {

            if (it != null) {
                listMyClients = it
                arrayMyClients = arrayOfNulls(listMyClients.size)
                for (s in listMyClients.indices) {
                    arrayMyClients[s] = listMyClients[s].firstname + " " + listMyClients[s].lastname
                }
                updateMyClientsSpinnerData()
                viewModel.updateClients.value = null
            }


        }

        /*viewModel.updatePrices.observe(this) {

            if (it != null) {

                binding.viewPaymentDetails.root.visibility = View.VISIBLE
                binding.viewPaymentDetails.rlConnection.visibility = View.GONE
                binding.viewPaymentDetails.rl100Percent.visibility = View.VISIBLE

                binding.viewPaymentDetails.tvPricePerGiftValue.text = it.price_per_gift
                binding.viewPaymentDetails.tvTaxValue.text = it.tax
                binding.viewPaymentDetails.tvTotalValue.text = it.total.toString()

                viewModel.total = it.total.toString()

                viewModel.updatePrices.value = null
            }
        }*/

    }

    private fun showAlreadyPaidDialog(data: CommonDataModel) {
        showCustomDialog(
            requireContext(),
            Constants.LOGOUT,
            Constants.ONE_CLICK_LISTENER,
            this,
            title = "Already Paid",
            message = data.message,
            yes = getString(R.string.txt_ok)
        )
    }

    private fun updatePrice(price: Int) {
        binding.viewPaymentDetails.root.visibility = View.VISIBLE
        binding.viewPaymentDetails.rlConnection.visibility = View.GONE
        binding.viewPaymentDetails.rl100Percent.visibility = View.VISIBLE

        binding.viewPaymentDetails.tvPricePerGiftValue.text = getString(R.string.text_price_int, price)
        binding.viewPaymentDetails.tvTaxValue.text = "6%"
        val total = ((6.div(100f)).times(price)).plus(price).roundToInt()
        binding.viewPaymentDetails.tvTotalValue.text = getString(R.string.text_price_int, total)

        viewModel.total = total.toString()

    }

    private fun updateClientConnectionSpinnerData() {
        for (x in userConnections) {
            print("lol $x")
        }

        val arrayAdapter =
            ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, userConnections)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.viewConnectionCompensation.spinnerConnection1.adapter = arrayAdapter
        binding.viewConnectionCompensation.spinnerConnection1.onItemSelectedListener =
            this@GiftsFragment
        binding.viewConnectionCompensation.spinnerConnection1.setSelection(0)

        if (viewModel.isConnection2) {
            binding.viewConnectionCompensation.spinnerConnection2.adapter = arrayAdapter
            binding.viewConnectionCompensation.spinnerConnection2.onItemSelectedListener =
                this@GiftsFragment
            binding.viewConnectionCompensation.spinnerConnection2.setSelection(0)
        }
    }

    private fun updateMyClientsSpinnerData() {
        val arrayAdapter =
            ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, arrayMyClients)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerClients.adapter = arrayAdapter
        binding.spinnerClients.onItemSelectedListener = this@GiftsFragment
        binding.spinnerClients.setSelection(0)


    }

    private fun setSelfContributionSpinner() {
        val arrayAdapter =
            ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, selfContriList)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.viewConnectionCompensation.spinnerSelfContribution.adapter = arrayAdapter
        binding.viewConnectionCompensation.spinnerSelfContribution.onItemSelectedListener =
            this@GiftsFragment
        binding.viewConnectionCompensation.spinnerSelfContribution.setSelection(0)
    }

    private fun setConnection1ContributionSpinner() {


        val arrayAdapter =
            ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, conn1ContriList)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.viewConnectionCompensation.spinnerConnection1Contribution.adapter = arrayAdapter
        binding.viewConnectionCompensation.spinnerConnection1Contribution.onItemSelectedListener =
            this@GiftsFragment
        binding.viewConnectionCompensation.spinnerConnection1Contribution.setSelection(0)
    }

    private fun setConnection2ContributionSpinner() {
        val arrayAdapter =
            ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, conn2ContriList)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.viewConnectionCompensation.spinnerConnection2Contribution.adapter = arrayAdapter
        binding.viewConnectionCompensation.spinnerConnection2Contribution.onItemSelectedListener =
            this@GiftsFragment
        binding.viewConnectionCompensation.spinnerConnection2Contribution.setSelection(0)
    }

    override fun onClick(v: View?) {

        when (v!!.id) {

            /*R.id.rl_submit -> {
                if (viewModel.gifts.isNotEmpty()) {
                    binding.viewConnectionCompensation.root.visibility = View.GONE
                    viewModel.getTotal()

                } else
                    showToast(getString(R.string.please_select_gifts))
            }*/

            R.id.tv_request -> {
                viewModel.submitRequest()

            }

            R.id.tv_co_brand -> {
                binding.viewPaymentDetails.rl100Percent.visibility = View.GONE
                binding.viewPaymentDetails.rlConnection.visibility = View.VISIBLE
            }

            R.id.tv_back -> {
                binding.viewPaymentDetails.rlConnection.visibility = View.GONE
                binding.viewConnectionCompensation.root.visibility = View.GONE
                binding.viewPaymentDetails.rl100Percent.visibility = View.VISIBLE
            }

            R.id.tv_100_percent -> {
                viewModel.isConnection1 = false
                viewModel.isConnection2 = false
                binding.viewConnectionCompensation.root.visibility = View.GONE

                val message = buildString {
                    append(getString(R.string.txt_pay_payment))
                    append("\n\n")
                    append(getString(R.string.text_pay_now_1))
                }
                val spannableString = SpannableString(message)
                spannableString.setSpan(ForegroundColorSpan(Color.RED), 65, message.length, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
                spannableString.setSpan(StyleSpan(Typeface.BOLD), 65, 69, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)

                showCustomDialog(
                    requireContext(),
                    Constants.LOGOUT,
                    Constants.ONE_CLICK_LISTENER,
                    this,
                    getString(R.string.text_pay_now_title),
                    message = message,
                    yes = getString(R.string.pay_now),
                    spannableString = spannableString
                )
            }

            R.id.tv_connection1 -> {
                binding.viewConnectionCompensation.root.visibility = View.VISIBLE
                viewModel.isConnection2 = false
                viewModel.isConnection1 = true
                setConnection1View()
                viewModel.getUserConnectionList()
                viewModel.isDefault = true
                viewModel.isCustom = false
                setDefaultCompensateView()
            }

            R.id.tv_connection2 -> {
                binding.viewConnectionCompensation.root.visibility = View.VISIBLE
                viewModel.isConnection2 = true
                viewModel.isConnection1 = false
                setConnection2View()
                viewModel.getUserConnectionList()
                viewModel.isDefault = true
                viewModel.isCustom = false
                setDefaultCompensateView()
            }

            R.id.tv_search_client -> {
                showToast(getString(R.string.under_development))
                viewModel.isConnection1 = false
                viewModel.isConnection2 = false
                binding.viewConnectionCompensation.root.visibility = View.GONE
            }

            R.id.tv_default_compensation -> {
                viewModel.isDefault = true
                viewModel.isCustom = false
                setDefaultCompensateView()
            }

            R.id.tv_custom_compensation -> {
                viewModel.isDefault = false
                viewModel.isCustom = true
                setCustomCompensateView()
            }
        }
    }


    override fun onDialogYesClick(click: String) {

        when (click) {

            getString(R.string.pay_now) -> {
                viewModel.payNow()
            }

            /*getString(R.string.subscribe) -> {
                showCustomDialog(
                    requireContext(),
                    Constants.LOGOUT,
                    Constants.TWO_CLICK_LISTENER,
                    this,
                    getString(R.string.subscribe),
                    getString(R.string.txt_subscribe_payment),
                    getString(R.string.subscribe),
                    getString(R.string.txt_cancel)
                )
            }*/

        }

    }

    override fun onDialogNoClick(click: String) {

        when (click) {

            getString(R.string.subscribe) -> {
                viewModel.subscribePayment()
            }

            getString(R.string.pay_now) -> {
                viewModel.payNow()
            }
        }
    }


    private fun setConnection1View() {

        binding.viewConnectionCompensation.tvTitleConnections.text =
            getString(R.string.one_connection)

        binding.viewConnectionCompensation.tvTitleConnectionOne.text =
            getString(R.string.choose_connection)

        binding.viewConnectionCompensation.rlSpinnerConnection2.visibility = View.GONE

        binding.viewConnectionCompensation.rlSpinnerConnection2Contribution.visibility =
            View.GONE

        binding.viewConnectionCompensation.spinnerConnection1Contribution.isEnabled = false

    }

    private fun setConnection2View() {

        binding.viewConnectionCompensation.tvTitleConnections.text =
            getString(R.string.two_connections)

        binding.viewConnectionCompensation.tvTitleConnectionOne.text =
            getString(R.string.connection_1)

        binding.viewConnectionCompensation.tvTitleConnectionTwo.text =
            getString(R.string.connection_2)

        binding.viewConnectionCompensation.rlSpinnerConnection2.visibility = View.VISIBLE

        binding.viewConnectionCompensation.rlSpinnerConnection2Contribution.visibility =
            View.VISIBLE

        binding.viewConnectionCompensation.rlSpinnerConnection2Contribution.isEnabled = false
    }

    private fun setDefaultCompensateView() {
        binding.viewConnectionCompensation.rlSpinnerSelfContribution.visibility =
            View.GONE
        binding.viewConnectionCompensation.rlSpinnerConnection1Contribution.visibility =
            View.GONE
        binding.viewConnectionCompensation.rlSpinnerConnection2Contribution.visibility =
            View.GONE
    }

    private fun setCustomCompensateView() {

        if (viewModel.isConnection1) {
            binding.viewConnectionCompensation.rlSpinnerSelfContribution.visibility =
                View.VISIBLE
            binding.viewConnectionCompensation.rlSpinnerConnection1Contribution.visibility =
                View.VISIBLE
            binding.viewConnectionCompensation.rlSpinnerConnection2Contribution.visibility =
                View.GONE
        } else if (viewModel.isConnection2) {
            binding.viewConnectionCompensation.rlSpinnerConnection1Contribution.isEnabled = true
            binding.viewConnectionCompensation.rlSpinnerSelfContribution.visibility =
                View.VISIBLE
            binding.viewConnectionCompensation.rlSpinnerConnection1Contribution.visibility =
                View.VISIBLE
            binding.viewConnectionCompensation.rlSpinnerConnection2Contribution.visibility =
                View.VISIBLE
        }


    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent!!.id) {
            R.id.spinner_clients -> {
                viewModel.clientID = listMyClients[position].id.toString()
            }

            R.id.spinner_subscription_type -> {
                when (position) {
                    1 -> {
                        viewModel.subscriptionType = subscriptionType[position].lowercase()
                        updatePrice(35)
                    }

                    2 -> {
                        viewModel.subscriptionType = subscriptionType[position].lowercase()
                        updatePrice(30)
                    }

                    else -> {
                        viewModel.subscriptionType = ""
                        binding.viewConnectionCompensation.root.visibility = View.GONE
                    }
                }


            }

            R.id.spinner_connection1 -> {
                viewModel.connection1Value = listConnections[position].id.toString()
            }

            R.id.spinner_connection2 -> {
                viewModel.connection2Value = listConnections[position].id.toString()
            }

            R.id.spinner_self_contribution -> {
                viewModel.selfContri = selfContriList[position].replace("%", "")
                if (viewModel.isConnection2)
                    conn1ContriList = Utils().getList(viewModel.selfContri.toInt())
                else {
                    val total = 100 - (viewModel.selfContri.toInt())
                    val list: MutableList<String> = arrayListOf()
                    list.add("$total%")
                    conn1ContriList = list
                }
                setConnection1ContributionSpinner()
            }

            R.id.spinner_connection1_contribution -> {
                viewModel.connection1Contri = conn1ContriList[position].replace("%", "")
                val total =
                    100 - (viewModel.selfContri.toInt() + viewModel.connection1Contri.toInt())
                val list: MutableList<String> = arrayListOf()
                list.add("$total%")
                conn2ContriList = list
                setConnection2ContributionSpinner()
            }

            R.id.spinner_connection2_contribution -> {
                viewModel.connection2Contri = conn2ContriList[position].replace("%", "")
            }
        }
    }


}
