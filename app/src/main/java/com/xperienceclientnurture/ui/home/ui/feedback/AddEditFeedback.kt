package com.xperienceclientnurture.ui.home.ui.feedback

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter

import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentAddEditFeedbackBinding
import com.xperienceclientnurture.ui.home.ui.addeditclient.ClientDetails
import com.xperienceclientnurture.utils.Constants

class AddEditFeedback : BaseFragment<FragmentAddEditFeedbackBinding, AddEditFeedbackViewModel>(),
    AdapterView.OnItemSelectedListener {
    override val fragmentLayoutId: Int
        get() = R.layout.fragment_add_edit_feedback
    override val viewModelClass: Class<AddEditFeedbackViewModel>
        get() = AddEditFeedbackViewModel::class.java

    private lateinit var listClients: List<ClientDetails>
    var userClients = arrayOfNulls<String>(0)
    var position = 0

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.viewmodel = viewModel

        viewModel.isEdit = bundle!!.getBoolean(Constants.ARGS_IS_EDIT, false)
        setObservers()
        if (viewModel.isEdit) {
            binding.rlSpinnerClients.visibility = View.GONE
            binding.spinnerClients.isEnabled = false
            binding.spinnerClients.visibility = View.GONE
            viewModel.feedbackID = bundle!!.getString(Constants.ARGS_FEEDBACK_ID, "")
            viewModel.feedback.value = bundle!!.getString(Constants.ARGS_FEEDBACK, "")
            viewModel.clientID = bundle!!.getString(Constants.ARGS_CLIENT_ID, "")
        }

        viewModel.getUserClients()


    }

    private fun setObservers() {

        viewModel.progress.observe(this@AddEditFeedback){

            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }
        }

        viewModel.listener.observe(this@AddEditFeedback) {

            if (it != null) {
                when (it) {

                    Constants.BACK -> {
                        requireActivity().sendBroadcast(Intent(Constants.RECEIVER_FEEDBACK))
                        requireActivity().finish()
                    }
                }

                viewModel.listener.value = null
            }

        }

        viewModel.updateClients.observe(this) {

            if (it != null) {
                listClients = it
                userClients = arrayOfNulls(it.size)
                for (s in it.indices) {
                    if (viewModel.clientID == it[s].id.toString()) {
                        position = s
                    }
                    userClients[s] = it[s].firstname
                }
                updateSpinnerData()
                viewModel.updateClients.value = null
            }


        }
    }

    private fun updateSpinnerData() {
        val arrayAdapter =
            ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, userClients)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerClients.adapter = arrayAdapter
        binding.spinnerClients.onItemSelectedListener = this@AddEditFeedback
        binding.spinnerClients.setSelection(position)
    }


    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        when (p0!!.id) {
            R.id.spinner_clients -> {
                viewModel.clientID = listClients[p2].id.toString()
            }
        }
    }

}
