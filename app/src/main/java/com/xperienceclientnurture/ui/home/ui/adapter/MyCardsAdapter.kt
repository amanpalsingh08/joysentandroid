package com.xperienceclientnurture.ui.home.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.xperienceclientnurture.R
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.home.ui.cards.CardsDetails
import com.xperienceclientnurture.utils.Constants

class MyCardsAdapter(var list: MutableList<CardsDetails>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var listener: OnItemClickListener


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).cardNumber.text = list[position].card_num
        holder.month.text = list[position].exp_month
        holder.year.text = list[position].exp_year

        holder.setItemSelectedListener(listener)


    }

    fun setItemSelectedListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_card,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun updateList(it: MutableList<CardsDetails>) {
        list = it
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val cardNumber: TextView = itemView.findViewById(R.id.tv_card_number_value)
        val month: TextView = itemView.findViewById(R.id.tv_month_value)
        val image: ImageView = itemView.findViewById(R.id.iv_edit)
        val year: TextView = itemView.findViewById(R.id.tv_year_value)
        val ivEdit: ImageView = itemView.findViewById(R.id.iv_edit)

        lateinit var listener: OnItemClickListener

        init {
            ivEdit.setOnClickListener(this)
        }

        fun setItemSelectedListener(listener: OnItemClickListener) {
            this.listener = listener
        }

        override fun onClick(p0: View?) {
            when (p0!!.id) {
                R.id.iv_edit -> {
                    listener.onClick(adapterPosition, Constants.EDIT_ITEM)
                }
            }
        }

    }

}




