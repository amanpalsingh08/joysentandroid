package com.xperienceclientnurture.ui.home.ui.gifts
import com.google.gson.annotations.SerializedName


data class GiftsTypeDataModel(
    @SerializedName("data")
    val `data`: List<GiftsTypeData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)

data class GiftsTypeData(
    @SerializedName("created_at")
    val created_at: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("parent_id")
    val parent_id: Any,
    @SerializedName("status")
    val status: Int,
    @SerializedName("type_name")
    val type_name: String,
    @SerializedName("type_slug")
    val type_slug: String,
    @SerializedName("updated_at")
    val updated_at: String
)