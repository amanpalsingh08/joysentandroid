package com.xperienceclientnurture.ui.home.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.xperienceclientnurture.R
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.home.ui.feedback.FeedbackDetails
import com.xperienceclientnurture.utils.Constants

class FeedbackAdapter(var list: List<FeedbackDetails>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var listener: OnItemClickListener

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).name.text =
            "${list[position].client_firstname} ${list[position].client_lastname}"
        holder.email.text = list[position].client_email
        holder.feedback.text = list[position].feedback

        holder.setItemSelectedListener(listener)

    }

    fun setItemSelectedListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_feedback,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun updateList(it: List<FeedbackDetails>) {
        list = it
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val name: TextView = itemView.findViewById(R.id.tv_client_name)
        val parentView: View = itemView.findViewById(R.id.parent_view)
        val email: TextView = itemView.findViewById(R.id.tv_client_email)
        val feedback: TextView = itemView.findViewById(R.id.tv_feedback)
        val ivEdit: ImageView = itemView.findViewById(R.id.iv_edit)
        val ivDelete: ImageView = itemView.findViewById(R.id.iv_delete)
        lateinit var listener: OnItemClickListener

        init {
            ivEdit.setOnClickListener(this)
            ivDelete.setOnClickListener(this)
        }

        fun setItemSelectedListener(listener: OnItemClickListener) {
            this.listener = listener
        }

        override fun onClick(p0: View?) {
            when (p0!!.id) {
                R.id.iv_edit -> {
                    listener.onClick(adapterPosition, Constants.EDIT_ITEM)
                }
                R.id.iv_delete-> {
                    listener.onClick(adapterPosition, Constants.DELETE_ITEM)
                }
            }
        }

    }

}




