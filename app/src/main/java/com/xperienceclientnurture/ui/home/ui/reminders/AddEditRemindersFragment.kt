package com.xperienceclientnurture.ui.home.ui.reminders

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.MavericksView
import com.airbnb.mvrx.Success
import com.airbnb.mvrx.activityViewModel
import com.airbnb.mvrx.withState
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.xperienceclientnurture.base.BaseMavericksFragment
import com.xperienceclientnurture.databinding.FragmentAddEditRemindersBinding
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.TimeZone

class AddEditRemindersFragment : BaseMavericksFragment(), MavericksView {

    private lateinit var binding: FragmentAddEditRemindersBinding
    private val viewModel: AddEditRemindersViewModel by activityViewModel()
    private var viewLayout: View? = null
    private var selectedDate = ""
    private var selectedTime = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        if (viewLayout == null) {
            binding = FragmentAddEditRemindersBinding.inflate(inflater, container, false)
            viewLayout = binding.root
        }
        return viewLayout as View
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rlFeedbackSubmit.setOnClickListener {
            it.hideSoftInput()
            viewModel.addEditReminders(
                selectedDate, selectedTime,
                binding.tieMessage.text.toString().trim()
            )
        }

        binding.tieDate.setSafeOnClickListener {
            showDatePicker()
        }

        binding.tieTime.setSafeOnClickListener {
            showTimePicker()
        }

        viewModel.onShowError = {
            showSnack(binding.parent, it)
        }

        withState(viewModel) { state ->
            state.parent?.let { obj ->
                binding.tieMessage.setText(obj.message ?: "")
                if (obj.date?.contains(" ") == true) {
                    selectedDate = obj.date.split(" ")[0]
                    selectedTime = obj.date.split(" ")[1]
                }
                binding.tieDate.setText(selectedDate)
                binding.tieTime.setText(selectedTime)
            }
        }
    }

    override fun invalidate() = withState(viewModel) { state ->
        binding.flLoading.isVisible = state.request is Loading

        if (state.request is Success && state.request.complete) {
            onUpdatedReminders?.invoke(true)
            requireActivity().onBackPressed()
        }

    }

    private fun showDatePicker() {
        val datePicker =
            MaterialDatePicker.Builder.datePicker()
                .setTitleText("Select Date")
                .setSelection(getSelectedDate(selectedDate))
                .build()
        datePicker.addOnPositiveButtonClickListener {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val date = Date(it)
            selectedDate = simpleDateFormat.format(date)
            binding.tieDate.setText(selectedDate)
        }
        datePicker.show(requireActivity().supportFragmentManager, "Date Picker")
    }

    private fun showTimePicker() {
        var hour = 0
        var minute = 0
        if (selectedTime.isNotEmpty()) {
            hour = selectedTime.split(":")[0].toInt()
            minute = selectedTime.split(":")[1].toInt()
        }

        val timePicker =
            MaterialTimePicker.Builder()
                .setTimeFormat(TimeFormat.CLOCK_12H)
                .setHour(hour)
                .setMinute(minute)
                .setTitleText("Select Time")
                .build()

        timePicker.addOnPositiveButtonClickListener {
            selectedTime = buildString {
                append(timePicker.hour)
                append(":")
                append(timePicker.minute)
            }
            binding.tieTime.setText(selectedTime)
        }
        timePicker.show(requireActivity().supportFragmentManager, "Time Picker")
    }

    private fun getSelectedDate(dateString: String): Long {

        val calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))

        if (dateString.isEmpty())
            return calendar.timeInMillis

        val splitDate = dateString.split("-")

        calendar[Calendar.YEAR] = splitDate[0].toInt()
        calendar[Calendar.MONTH] = splitDate[1].toInt().minus(1)
        calendar[Calendar.DAY_OF_MONTH] = splitDate[2].toInt()

        return calendar.timeInMillis
    }
}