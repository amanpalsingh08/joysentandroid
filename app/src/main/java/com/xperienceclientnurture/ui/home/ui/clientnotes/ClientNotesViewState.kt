package com.xperienceclientnurture.ui.home.ui.clientnotes

import com.airbnb.mvrx.Async
import com.airbnb.mvrx.MavericksState
import com.airbnb.mvrx.Uninitialized

data class ClientNotesViewState(
    val request: Async<ResultClientNotes> = Uninitialized,
    val requestDelete: Async<ResultClientNotes> = Uninitialized,
    val listClientNotes: List<ClientNotes> = emptyList()
) : MavericksState {

    fun update(response: ResultClientNotes): ClientNotesViewState {
        return copy(listClientNotes = response.list.filter { it.id != null })
    }
}
