package com.xperienceclientnurture.ui.home.ui.reminders

import android.content.Context
import com.airbnb.mvrx.Fail
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.MavericksViewModel
import com.airbnb.mvrx.MavericksViewModelFactory
import com.airbnb.mvrx.Success
import com.airbnb.mvrx.ViewModelContext
import com.xperienceclientnurture.extensions.asFlow
import com.xperienceclientnurture.data.ClientRepository
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.utils.Constants
import kotlinx.coroutines.launch

class RemindersViewModel(
    state: RemindersViewState, val context: Context, private val repository: ClientRepository
) : MavericksViewModel<RemindersViewState>(state) {

    var reminderID = ""

    fun getReminders() =
        viewModelScope.launch {
            repository::reminders.asFlow(Prefs.getString(Constants.USER_ID, ""))
                .execute {
                    when (it) {
                        is Loading -> copy(request = Loading())
                        is Fail -> {
                            it.error.printStackTrace()
                            copy(request = Fail(it.error))
                        }

                        is Success -> {
                            update(it()).copy(request = Success(it()))
                        }

                        else -> {
                            this
                        }
                    }
                }
        }

    fun deleteClientNote() =
        viewModelScope.launch {
            repository::deleteReminder.asFlow(Prefs.getString(Constants.USER_ID, ""), reminderID)
                .execute {
                    when (it) {
                        is Loading -> copy(requestDelete = Loading())
                        is Fail -> {
                            it.error.printStackTrace()
                            copy(requestDelete = Fail(it.error))
                        }

                        is Success -> {
                            getReminders()
                            copy(requestDelete = Success(it()))
                        }

                        else -> {
                            this
                        }
                    }
                }
        }

    companion object : MavericksViewModelFactory<RemindersViewModel, RemindersViewState> {

        override fun create(
            viewModelContext: ViewModelContext, state: RemindersViewState
        ) = RemindersViewModel(state, viewModelContext.activity(), ClientRepository(context = viewModelContext.activity()))
    }


}