package com.xperienceclientnurture.ui.home.ui.clientconnection
import com.google.gson.annotations.SerializedName


data class GetClientConnectionDataModel(
    @SerializedName("data")
    val `data`: ClientConnectionData,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)

data class ClientConnectionData(
    @SerializedName("card")
    val card: Int,
    @SerializedName("requests")
    val requests: List<Request>
)

data class Request(
    @SerializedName("client_email")
    val client_email: String,
    @SerializedName("client_id")
    val client_id: Int,
    @SerializedName("client_name")
    val client_name: String,
    @SerializedName("connection_id")
    val connection_id: Int,
    @SerializedName("connection_name")
    val connection_name: String,
    @SerializedName("connection_role")
    val connection_role: String,
    @SerializedName("status")
    val status: String
)