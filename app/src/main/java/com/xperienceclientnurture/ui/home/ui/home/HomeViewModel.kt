package com.xperienceclientnurture.ui.home.ui.home

import android.util.Log
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.ui.home.ui.delivered.DeliveredClientDetails
import com.xperienceclientnurture.ui.home.ui.queueclients.QueueClientsDetails
import com.xperienceclientnurture.utils.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class HomeViewModel : BaseViewModel() {


    val updateUIQueue = SingleLiveData<List<QueueClientsDetails>>()
    val updateUIDelivered = SingleLiveData<List<DeliveredClientDetails>>()
    val progressQueue = SingleLiveData<Boolean>()
    val progressDelivered = SingleLiveData<Boolean>()

    val noDataDelivered = SingleLiveData<Boolean>()
    val noDataQueue = SingleLiveData<Boolean>()


    fun getQueueClients() {
        progressQueue.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getQueueClients(Prefs.getString(Constants.USER_ID, ""))
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progressQueue.postValue(false)
                        response.body()?.let {
                            when (it.status_code) {
                                200 -> {
                                    val list = it.data
                                    if (list.isEmpty()) {
                                        noDataQueue.postValue(true)
                                    } else {
                                        noDataQueue.postValue(false)
                                    }
                                    updateUIQueue.postValue(list)
                                }

                                400 -> {
                                    noDataQueue.postValue(true)
                                }
                            }
                        }
                    } else {
                        progressQueue.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progressQueue.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progressQueue.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    fun getDeliveredClients() {
        progressDelivered.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getDeliveredClients(Prefs.getString(Constants.USER_ID, ""))
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progressDelivered.postValue(false)
                        response.body()?.let {
                            when (it.status_code) {
                                200 -> {
                                    if (it.status.equals(Constants.ERROR, ignoreCase = true)) {
                                        noDataDelivered.postValue(true)
                                    } else {
                                        val list = it.data
                                        if (list.isEmpty()) {
                                            noDataDelivered.postValue(true)
                                        } else {
                                            noDataDelivered.postValue(false)
                                        }
                                        updateUIDelivered.postValue(list)
                                    }
                                }
                                400->{
                                    noDataDelivered.postValue(true)
                                }
                            }
                        }
                    } else {
                        progressDelivered.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progressDelivered.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progressDelivered.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

}