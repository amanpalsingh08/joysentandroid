package com.xperienceclientnurture.ui.home.ui.viewclient

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentClientInfoBinding
import com.xperienceclientnurture.ui.home.HomeScreenActivity
import com.xperienceclientnurture.ui.home.ui.addeditclient.ClientDetails
import com.xperienceclientnurture.utils.Constants

class ClientInfoFragment : BaseFragment<FragmentClientInfoBinding, ClientInfoViewModel>(),
    View.OnClickListener {
    override val fragmentLayoutId: Int
        get() = R.layout.fragment_client_info
    override val viewModelClass: Class<ClientInfoViewModel>
        get() = ClientInfoViewModel::class.java

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        setObservers()

        viewModel.clientID = bundle!!.getString(Constants.ARGS_CLIENT_ID, "")

        viewModel.getClientDetails()

    }

    private fun setObservers() {
        viewModel.progress.observe(viewLifecycleOwner) {

            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }
        }

        viewModel.image.observe(viewLifecycleOwner) {

            if (it != null) {
                Glide
                    .with(requireContext())
                    .load(it)
                    .into(binding.ivUserImage);
                viewModel.image.value = null
            }

        }

        viewModel.updateUI.observe(viewLifecycleOwner) {
            if (it != null) {
                setSpouseChildData(it)
                viewModel.updateUI.value = null
            }
        }


        binding.tvPhone1Value.setOnClickListener(this)
        binding.tvPhone2Value.setOnClickListener(this)
        binding.tvEmailValue.setOnClickListener(this)
    }

    private fun setSpouseChildData(details: ClientDetails) {
        binding.tvTitleSpouseName.text = getString(R.string.name_colon_new, details.spouseChildren?.name ?: "")
        binding.tvTitleSpouseAge.text = getString(R.string.age_colon_new, details.spouseChildren?.age ?: "")
        binding.tvTitleSpouseGender.text = getString(R.string.gender_colon_new, details.spouseChildren?.gender ?: "")
        binding.tvTitleSpouseBirthday.text = getString(R.string.birthday_colon_new, details.spouseChildren?.birthday ?: "")
        binding.tvTitleSpouseAnniversary.text = getString(R.string.anniversary_colon_new, details.spouseChildren?.anniversary ?: "")


        details.childs?.forEachIndexed { index, dataObj ->
            val child = this.layoutInflater.inflate(R.layout.layout_view_child_info, null, true) as ConstraintLayout
            child.findViewById<AppCompatTextView>(R.id.tv_title_child).text = getString(R.string.child_tag, index.plus(1))
            child.findViewById<AppCompatTextView>(R.id.tv_title_child_name).text = getString(R.string.name_colon_new, dataObj.name)
            child.findViewById<AppCompatTextView>(R.id.tv_title_child_age).text = getString(R.string.age_colon_new, dataObj.age)
            child.findViewById<AppCompatTextView>(R.id.tv_title_child_gender).text = getString(R.string.gender_colon_new, dataObj.gender)
            child.findViewById<AppCompatTextView>(R.id.tv_title_child_birthday).text = getString(R.string.birthday_colon_new, dataObj.birthday)
            if (details.childs?.size?.minus(1) == index)
                child.findViewById<View>(R.id.view7).visibility = View.GONE
            else child.findViewById<View>(R.id.view7).visibility = View.VISIBLE
            binding.llParent.addView(child)
        }

    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.tv_phone1_value -> {
                (activity as HomeScreenActivity).call(viewModel.clientDetailsData.value!!.phone ?: "")
            }

            R.id.tv_phone2_value -> {
                (activity as HomeScreenActivity).call(viewModel.clientDetailsData.value!!.phone2 ?: "")
            }

            R.id.tv_email_value -> {
                (activity as HomeScreenActivity).sendEmail(
                    viewModel.clientDetailsData.value!!.email ?: "",
                    "Xperiencce Client Nurture",
                    "You are our potential client."
                )
            }
        }
    }


}
