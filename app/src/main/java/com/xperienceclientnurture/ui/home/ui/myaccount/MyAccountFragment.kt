package com.xperienceclientnurture.ui.home.ui.myaccount

//import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import android.Manifest
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.fondesa.kpermissions.coroutines.flow
import com.fondesa.kpermissions.extension.permissionsBuilder
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.bottomsheet.BottomSheet
import com.xperienceclientnurture.databinding.FragmentMyAccountBinding
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.ui.home.HomeScreenActivity
import com.xperienceclientnurture.utils.Constants
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.Calendar

class MyAccountFragment : BaseFragment<FragmentMyAccountBinding, MyAccountViewModel>(),
    BottomSheet.BottomSheetListener, AdapterView.OnItemSelectedListener, View.OnClickListener {

    var types = arrayOf("lender", "agent", "team", "brokerage", "company", "vendor", "sales rep")
    var states = arrayOfNulls<String>(0)

    companion object {
        const val IMAGE_DIRECTORY = "/xperience_client_nurture"
        const val GALLERY = 1
        const val CAMERA = 2
    }

    private lateinit var selectProfileBottomSheet: BottomSheet
    private var profileImagePath: String? = ""

    override val fragmentLayoutId: Int
        get() = R.layout.fragment_my_account
    override val viewModelClass: Class<MyAccountViewModel>
        get() = MyAccountViewModel::class.java

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.viewmodel = viewModel

        (activity as HomeScreenActivity).toolbarTitle.text = getString(R.string.my_account)

        setObservers()

        viewModel.getUserDetails()

        val arrayAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, types)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.spinnerType.isEnabled = false
        binding.spinnerType.isClickable = false
        binding.spinnerType.adapter = arrayAdapter
        binding.spinnerType.onItemSelectedListener = this@MyAccountFragment
        binding.spinnerType.setSelection(0)

        binding.ivUserImage.setOnClickListener(this)

    }

    private fun setObservers() {
        viewModel.progress.observe(viewLifecycleOwner) {

            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }

        }




        viewModel.listener.observe(viewLifecycleOwner) {

            if (it != null) {
                when (it) {
                    Constants.UPDATE_IMAGE -> {
                        selectProfileImage()
                    }
                }

                viewModel.listener.value = null
            }

        }

        viewModel.updateSpinner.observe(viewLifecycleOwner) {

            if (it != null) {
                states = arrayOfNulls(it.size)
                for (s in it.indices) {
                    states[s] = it.get(s).code
                }

                updateSpinnerData()


                viewModel.updateSpinner.value = null
            }

        }

        viewModel.updateUI.observe(viewLifecycleOwner) {

            if (it != null) {

                setData(it)

                viewModel.updateUI.value = null
            }

        }
    }

    private fun setData(it: UserData) {
        binding.tieEmail.setText(it.email)
        binding.tieFirstName.setText(it.firstname)
        binding.tieLastName.setText(it.lastname)
        binding.tiePhone.setText(Prefs.getString(Constants.USER_PHONE, ""))
        binding.tieAddress.setText(it.address)
        if (it.zip_code != null)
            binding.tieZipCode.setText(it.zip_code)
        else
            binding.tieZipCode.setText("")
        if (it.city != null)
            binding.tieCity.setText(it.city)
        else
            binding.tieCity.setText("")

        Glide
            .with(this)
            .load(it.image)
            .into(binding.ivUserImage);


        viewModel.state = it.state ?: ""
        viewModel.userType = it.role ?: ""
        for (index in types.indices) {
            if (it.role.contentEquals(types[index])) {
                binding.spinnerType.setSelection(index)
                break
            }

        }


    }

    private fun updateSpinnerData() {
        val arrayAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, states)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerState.adapter = arrayAdapter
        binding.spinnerState.onItemSelectedListener = this@MyAccountFragment
        binding.spinnerState.setSelection(viewModel.statePos)

    }


    private fun selectProfileImage() {
        val request = permissionsBuilder(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ).build()
        lifecycleScope.launch {
            request.flow().collect { result ->
                println("MyAccountFragment.selectProfileImage $result")
                // Handle the result.
                selectProfileBottomSheet = BottomSheet(this@MyAccountFragment)
                selectProfileBottomSheet.show(requireActivity().supportFragmentManager, "BottomSheet")
                selectProfileBottomSheet.isCancelable = false
            }
        }
        request.send()
    }

    private fun selectImageFromGallery() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun selectImageFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != AppCompatActivity.RESULT_CANCELED) {
            if (requestCode == GALLERY) {
                if (data != null) {
                    val contentURI = data.data
                    try {
                        val bitmap =
                            MediaStore.Images.Media.getBitmap(requireContext().contentResolver, contentURI)
                        profileImagePath = saveImage(bitmap)
                        binding.ivUserImage!!.setImageBitmap(bitmap)
                        selectProfileBottomSheet.dialog!!.cancel()
                    } catch (e: IOException) {
                        e.printStackTrace()
                        Toast.makeText(context, "Failed to save image", Toast.LENGTH_SHORT).show()
                    }
                }
            } else if (requestCode == CAMERA) {
                val thumbnail = data!!.extras!!.get("data") as Bitmap
                binding.ivUserImage!!.setImageBitmap(thumbnail)
                selectProfileBottomSheet.dialog!!.cancel()
                profileImagePath = saveImage(thumbnail)
            }
        }
    }


    private fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 60, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString() + IMAGE_DIRECTORY
        )
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            viewModel.imageFile = File(
                wallpaperDirectory, ((Calendar.getInstance()
                    .timeInMillis).toString() + ".jpg")
            )
            viewModel.imageFile!!.createNewFile()
            val fo = FileOutputStream(viewModel.imageFile!!)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                context,
                arrayOf(viewModel.imageFile!!.path),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            return viewModel.imageFile!!.absolutePath
        } catch (e1: IOException) {
            e1.printStackTrace()
            return ""
        }
    }

    override fun onOptionClick(text: String) {
        if (text == "gallary clicked") {
            selectImageFromGallery()
        }
        if (text == "camera clicked") {
            selectImageFromCamera()
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        when (p0!!.id) {
            R.id.spinner_type -> {
                viewModel.userType = types[p2]
            }

            R.id.spinner_state -> {
                viewModel.state = states[p2]!!
            }
        }
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivUserImage -> {
                selectProfileImage()
            }
        }
    }

}
