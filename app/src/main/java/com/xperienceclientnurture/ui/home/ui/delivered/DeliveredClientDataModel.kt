package com.xperienceclientnurture.ui.home.ui.delivered
import com.google.gson.annotations.SerializedName



data class DeliveredClientDataModel(
    @SerializedName("data")
    val `data`: List<DeliveredClientDetails>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)

data class DeliveredClientDetails(
    @SerializedName("address")
    val address: String,
    @SerializedName("city")
    val city: String,
    @SerializedName("client_assigned_to")
    val client_assigned_to: String,
    @SerializedName("client_conn1_name")
    val client_conn1_name: String,
    @SerializedName("client_conn2_name")
    val client_conn2_name: String,
    @SerializedName("client_connection1")
    val client_connection1: String,
    @SerializedName("client_connection2")
    val client_connection2: String,
    @SerializedName("client_firstname")
    val client_firstname: String,
    @SerializedName("client_group")
    val client_group: String,
    @SerializedName("client_id")
    val client_id: String,
    @SerializedName("client_lastname")
    val client_lastname: String,
    @SerializedName("client_lead_source")
    val client_lead_source: String,
    @SerializedName("client_name")
    val client_name: String,
    @SerializedName("client_referer")
    val client_referer: Any,
    @SerializedName("client_stage")
    val client_stage: String,
    @SerializedName("created_at")
    val created_at: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("label_id")
    val label_id: Any,
    @SerializedName("orderId")
    val orderId: Any,
    @SerializedName("orderNumber")
    val orderNumber: Any,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("state")
    val state: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("tracking_id")
    val tracking_id: Any,
    @SerializedName("type")
    val type: String,
    @SerializedName("updated_at")
    val updated_at: String
)