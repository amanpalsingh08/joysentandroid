package com.xperienceclientnurture.ui.home.ui.importclients
import com.google.gson.annotations.SerializedName


data class ImportClientDataModel(
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)