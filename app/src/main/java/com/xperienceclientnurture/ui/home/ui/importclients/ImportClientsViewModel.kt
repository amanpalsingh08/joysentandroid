package com.xperienceclientnurture.ui.home.ui.importclients

import android.util.Log
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.utils.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.HttpException
import java.io.File

class ImportClientsViewModel : BaseViewModel() {

    var file: File? = null
    val progress = SingleLiveData<Boolean>()
    val listener = SingleLiveData<String>()
    private var importParams: HashMap<String, RequestBody> = HashMap()

    fun submitClients() {

        if (file != null) {
            importParams["user_id"] = Prefs.getString(Constants.USER_ID, "").toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(
//                    MediaType.parse("text/plain"),
//                    Prefs.getString(Constants.USER_ID, "")
//                )


            progress.postValue(true)
            val service = RetrofitFactory.makeRetrofitService()
            CoroutineScope(Dispatchers.IO).launch {

                val response = service.importClients(
                    importParams,
                    getRequestFile()
                )

                try {

                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {
                            progress.postValue(false)
                            response.body()?.let {
                                showToast(it.message)
                            }
                        } else {
                            progress.postValue(false)
                            showToast("Error network operation failed with ${response.code()}")
                        }
                    }
                } catch (e: HttpException) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Exception ${e.message}")
                } catch (e: Throwable) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Ooops: Something else went wrong")
                }
            }
        } else {
            showToast("Please select file")
        }

    }

    private fun getRequestFile(): MultipartBody.Part {

        val requestBody = file!!.asRequestBody()
//            RequestBody.create(MediaType.parse("multipart/form-data"), file!!)
        return MultipartBody.Part.createFormData("file", file!!.name, requestBody)

    }

}
