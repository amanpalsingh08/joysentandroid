package com.xperienceclientnurture.ui.home.ui.reminders

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class ResultReminders(
    @Json(name = "status") @field:Json(name = "status")
    val status: String,
    @Json(name = "status_code") @field:Json(name = "status_code")
    val statusCode: Int,
    @Json(name = "message") @field:Json(name = "message")
    val message: String,
    @Json(name = "data") @field:Json(name = "data")
    val list: List<Reminders> = emptyList()
):Parcelable
