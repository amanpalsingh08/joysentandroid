package com.xperienceclientnurture.ui.home.ui.reminders

import com.airbnb.mvrx.Async
import com.airbnb.mvrx.MavericksState
import com.airbnb.mvrx.Uninitialized

data class RemindersViewState(
    val request: Async<ResultReminders> = Uninitialized,
    val requestDelete: Async<ResultReminders> = Uninitialized,
    val listReminders: List<Reminders> = emptyList()
) : MavericksState {

    fun update(response: ResultReminders): RemindersViewState {
        return copy(listReminders = response.list.filter { it.id != null })
    }
}
