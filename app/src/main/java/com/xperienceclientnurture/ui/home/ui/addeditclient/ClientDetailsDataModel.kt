package com.xperienceclientnurture.ui.home.ui.addeditclient

import com.google.gson.annotations.SerializedName


data class ClientDetailsDataModel(
    @SerializedName("data")
    val data: ClientDetails,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)

data class ClientDetails(
    @SerializedName("client_type")
    val clientType: String? = null,
    @SerializedName("address")
    val address: String? = null,
    @SerializedName("assigned_to")
    val assigned_to: String? = null,
    @SerializedName("city")
    val city: String? = null,
    @SerializedName("country")
    val country: String? = null,
    @SerializedName("email")
    val email: String? = null,
    @SerializedName("firstname")
    val firstname: String? = null,
    @SerializedName("frequency")
    val frequency: String? = null,
    @SerializedName("group")
    val group: String? = null,
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("lastname")
    val lastname: String? = null,
    @SerializedName("lead_source")
    val leadSource: String? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("phone")
    val phone: String? = null,
    @SerializedName("phone2")
    val phone2: String? = null,
    @SerializedName("phone_type")
    val phone_type: String? = null,
    @SerializedName("phone_type2")
    val phone_type2: String? = null,
    @SerializedName("profile_image")
    val profile_image: String? = null,
    @SerializedName("shipping_address")
    val shipping_address: String? = null,
    @SerializedName("shipping_city")
    val shipping_city: String? = null,
    @SerializedName("shipping_country")
    val shipping_country: String? = null,
    @SerializedName("shipping_state")
    val shipping_state: String? = null,
    @SerializedName("shipping_zipcode")
    val shipping_zipcode: String? = null,
    @SerializedName("stage")
    val stage: String? = null,
    @SerializedName("state")
    val state: String? = null,
    @SerializedName("status")
    val status: String? = null,
    @SerializedName("userid")
    val userid: String? = null,
    @SerializedName("zipcode")
    val zipcode: String? = null,
    @SerializedName("closing_date")
    var closingDate: String? = null,
    @SerializedName("spouse")
    var spouseChildren: SpouseChildren? = null,
    @SerializedName("childs")
    var childs: List<SpouseChildren>? = emptyList()
)

data class SpouseChildren(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("user_id")
    val userId: String? = null,
    @SerializedName("client_id")
    val clientId: String? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("age")
    val age: String? = null,
    @SerializedName("gender")
    val gender: String? = null,
    @SerializedName("birthday")
    val birthday: String? = null,
    @SerializedName("anniversary")
    val anniversary: String? = null,
    @SerializedName("info_about")
    val infoAbout: String? = null,
    @SerializedName("status")
    val status: String? = null,
    @SerializedName("created_at")
    val createdAt: String? = null,
    @SerializedName("updated_at")
    val updatedAt: String? = null
)