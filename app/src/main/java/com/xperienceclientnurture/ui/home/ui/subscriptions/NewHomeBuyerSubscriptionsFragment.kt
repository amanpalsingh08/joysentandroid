package com.xperienceclientnurture.ui.home.ui.subscriptions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.airbnb.epoxy.AsyncEpoxyController
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.MavericksView
import com.airbnb.mvrx.activityViewModel
import com.airbnb.mvrx.withState
import com.xperienceclientnurture.common.simpleController
import com.xperienceclientnurture.base.BaseMavericksFragment
import com.xperienceclientnurture.databinding.FragmentNewHomeBuyerSubscriptionsBinding
import com.xperienceclientnurture.ui.home.ui.epoxyModel.listViewNewHomeBuyerSubscriptions

class NewHomeBuyerSubscriptionsFragment : BaseMavericksFragment(), MavericksView {

    private lateinit var binding: FragmentNewHomeBuyerSubscriptionsBinding
    private val viewModel: NewHomeBuyerSubscriptionsViewModel by activityViewModel()
    private val epoxyController: AsyncEpoxyController by lazy { epoxyController() }
    private var viewLayout: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        if (viewLayout == null) {
            binding = FragmentNewHomeBuyerSubscriptionsBinding.inflate(inflater, container, false)
            viewLayout = binding.root
        }
        return viewLayout as View
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.setController(epoxyController)
        viewModel.getNewHomeBuyerSubscription()

    }

    override fun invalidate() = withState(viewModel) { state ->
        binding.flLoading.isVisible = state.request is Loading
        epoxyController.requestModelBuild()
    }

    private fun epoxyController() = simpleController(viewModel) { state ->

        if (state.listActiveSubscriptions.isNotEmpty()) {
            state.listActiveSubscriptions.forEach { obj ->
                listViewNewHomeBuyerSubscriptions {
                    id(obj.id)
                    data(obj)
                }
            }
        }
    }
}