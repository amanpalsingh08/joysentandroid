package com.xperienceclientnurture.ui.home.ui.gallery

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentGalleryBinding
import com.xperienceclientnurture.ui.home.ui.adapter.GalleryAdapter

class GalleryFragment : BaseFragment<FragmentGalleryBinding, GalleryViewModel>() {
    private lateinit var list: MutableList<GalleryDetails>
    override val fragmentLayoutId: Int
        get() = R.layout.fragment_gallery
    override val viewModelClass: Class<GalleryViewModel>
        get() = GalleryViewModel::class.java

    lateinit var adapter: GalleryAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel
        list = arrayListOf()

        viewModel.updateUI.observe(this, Observer {
            if (it != null) {
                setData(it)
            }


        })

        viewModel.progress.observe(this, Observer {

            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }

        })

        viewModel.noData.observe(this, Observer {

            if (it) {
                binding.noDataFound.root.visibility = View.VISIBLE
            } else {
                binding.noDataFound.root.visibility = View.GONE
            }

        })
        adapter = GalleryAdapter(list)

        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        viewModel.getGallery()
    }

    private fun setData(it: List<GalleryDetails>) {
        adapter.updateList(it)
    }

}