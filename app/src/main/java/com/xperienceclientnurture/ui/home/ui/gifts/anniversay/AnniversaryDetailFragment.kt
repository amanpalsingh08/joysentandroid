package com.xperienceclientnurture.ui.home.ui.gifts.anniversay

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentAnniversaryDetailBinding
import com.xperienceclientnurture.ui.home.ui.HolderActivity
import com.xperienceclientnurture.ui.home.ui.addeditclient.ClientDetails
import com.xperienceclientnurture.utils.Constants
import com.slider.sliderimage.adapters.ViewPagerAdapter

class AnniversaryDetailFragment :
    BaseFragment<FragmentAnniversaryDetailBinding, AnniversaryDetailViewModel>(),
    View.OnClickListener,
    AdapterView.OnItemSelectedListener {


    override val fragmentLayoutId: Int
        get() = R.layout.fragment_anniversary_detail
    override val viewModelClass: Class<AnniversaryDetailViewModel>
        get() = AnniversaryDetailViewModel::class.java

    private var arrayMyClients = arrayOfNulls<String>(0)
    private var arrayOfDates = arrayOfNulls<String>(0)
    private lateinit var listOfDates: List<String>
    private val listGiftCounts = arrayOf(
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10",
        "11",
        "12",
        "13",
        "14",
        "15"

    )
    private lateinit var listMyClients: List<ClientDetails>
    private lateinit var anniversaryDetail: AnniversaryDetailDataModel
    private var giftID = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bundle.let {
            giftID = it?.getString(Constants.GIFT_ID, "") ?: ""
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        setObservers()

        setViewListeners()

        val data: AnniversaryDataList = bundle!!.getParcelable(Constants.HOME_DECOR_MODEL)!!

        setDetails(data)
        updateDatesSpinnerData()


        viewModel.getMyClients()
    }


    private fun setViewListeners() {
        binding.tvBuy.setOnClickListener(this)
        binding.tvPurchaseNow.setOnClickListener(this)
        binding.tvCancel.setOnClickListener(this)

    }

    private fun setObservers() {


        viewModel.progress.observe(this, Observer {
            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }
        })
        viewModel.listener.observe(this, Observer { })
        viewModel.updateClients.observe(this, Observer {

            if (it != null) {
                listMyClients = it
                arrayMyClients = arrayOfNulls(listMyClients.size)
                for (s in listMyClients.indices) {
                    arrayMyClients[s] = listMyClients[s].firstname + " " + listMyClients[s].lastname
                }
                updateMyClientsSpinnerData()
                viewModel.updateClients.value = null
            }
        })

        viewModel.updateDates.observe(this, Observer {

            if (it != null) {
                listOfDates = it
                arrayOfDates = arrayOfNulls(listOfDates.size)
                for (s in listOfDates.indices) {
                    arrayOfDates[s] = listOfDates[s]
                }
                updateDatesSpinnerData()
                viewModel.updateDates.value = null
            }
        })

        viewModel.updateDetail.observe(this, Observer {

            if (it != null) {
                anniversaryDetail = it
                setDetails(anniversaryDetail)
                viewModel.updateDetail.value = null
            }
        })

        viewModel.purchaseSuccess.observe(this, Observer {
            if (it != null) {
                showToast(it.message)
                binding.constraintLayout.visibility = View.GONE
                binding.tvPurchaseNow.visibility = View.VISIBLE
                (activity as HolderActivity).toolbarTitleHolder.text =
                    getString(R.string.anniversary_gifts)
                activity!!.onBackPressed()
                viewModel.purchaseSuccess.value = null
            }
        })
    }

    private fun updateMyClientsSpinnerData() {
        val arrayAdapter =
            ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, arrayMyClients)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerClient.adapter = arrayAdapter
        binding.spinnerClient.onItemSelectedListener = this@AnniversaryDetailFragment
        binding.spinnerClient.setSelection(0)
        viewModel.clientID = listMyClients[0].id.toString()
    }

    private fun updateDatesSpinnerData() {
        val arrayAdapter =
            ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, listGiftCounts)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerDates.adapter = arrayAdapter
        binding.spinnerDates.onItemSelectedListener = this@AnniversaryDetailFragment
        binding.spinnerDates.setSelection(0)
        viewModel.quantity = listGiftCounts[0]
    }

    override fun onClick(v: View?) {

        when (v!!.id) {
            R.id.tv_buy -> {
                viewModel.purchase(giftID)
            }
            R.id.tv_cancel -> {
                binding.constraintLayout.visibility = View.GONE
                binding.tvPurchaseNow.visibility = View.VISIBLE
            }
            R.id.tv_purchase_now -> {
                viewModel.getAnniversaryDetailByClient(
                    giftID,
                    viewModel.clientID, viewModel.quantity,viewModel.message
                )
            }

        }
    }

    @SuppressLint("SetTextI18n")
    private fun setDetails(anniversaryDetail: AnniversaryDetailDataModel) {

        binding.constraintLayout.visibility = View.VISIBLE
        binding.tvPurchaseNow.visibility = View.GONE

        binding.tvTotalPrice.text = "\$ ${anniversaryDetail.data.total}"
        binding.tvQuantity.text = "\$ ${anniversaryDetail.data.quantity}"
        binding.tvTax.text = "\$ ${anniversaryDetail.data.tax}"
        binding.tvShippingPrice.text = "\$ ${anniversaryDetail.data.shipping}"


    }


    @SuppressLint("SetTextI18n")
    private fun setDetails(anniversaryDataList: AnniversaryDataList) {
        binding.tvName.text = anniversaryDataList.gift_name
        binding.tvHeight.text = anniversaryDataList.height
        binding.tvWeight.text = anniversaryDataList.weight
        binding.tvLength.text = anniversaryDataList.length

        binding.tvDesription.text = anniversaryDataList.description

        binding.tvPrice.text = "\$ ${anniversaryDataList.price}"
        binding.tvBasePrice.text = "\$ ${anniversaryDataList.price}"

        binding.viewPagerFullScreen.adapter =
            ViewPagerAdapter(
                context = binding.viewPagerFullScreen.context,
                items = anniversaryDataList.img
            )
        binding.viewPagerFullScreen.setCurrentItem(0, true)
        binding.indicatorScreen.setViewPager(binding.viewPagerFullScreen)


    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent?.id) {
            R.id.spinner_client -> {
                viewModel.clientID = listMyClients[position].id.toString()
            }
            R.id.spinner_dates -> {
                viewModel.quantity = listGiftCounts[position]
                binding.constraintLayout.visibility = View.GONE
                binding.tvPurchaseNow.visibility = View.VISIBLE

            }
        }
    }


}


