package com.xperienceclientnurture.ui.home.ui.clientnotes

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class ClientNotes(
    @Json(name = "userid") @field:Json(name = "userid")
    val userid: String? = null,
    @Json(name = "id") @field:Json(name = "id")
    val id: Int? = null,
    @Json(name = "client_id") @field:Json(name = "client_id")
    val clientID: String? = null,
    @Json(name = "user_id") @field:Json(name = "user_id")
    val userID: String? = null,
    @Json(name = "notes") @field:Json(name = "notes")
    val notes: String? = null,
    @Json(name = "notes_access") @field:Json(name = "notes_access")
    val notesAccess: String? = null,
    @Json(name = "status") @field:Json(name = "status")
    val status: String? = null,
    @Json(name = "created_at") @field:Json(name = "created_at")
    val createdAt: String? = null,
    @Json(name = "updated_at") @field:Json(name = "updated_at")
    val updatedAt: String? = null,
    @Json(name = "client_firstname") @field:Json(name = "client_firstname")
    val clientFirstname: String? = null,
    @Json(name = "client_lastname") @field:Json(name = "client_lastname")
    val clientLastname: String? = null,
    @Json(name = "user_firstname") @field:Json(name = "user_firstname")
    val userFirstname: String? = null,
    @Json(name = "user_lastname") @field:Json(name = "user_lastname")
    val userLastname: String? = null
) : Parcelable