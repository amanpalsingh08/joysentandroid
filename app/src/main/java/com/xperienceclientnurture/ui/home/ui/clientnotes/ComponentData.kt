package com.xperienceclientnurture.ui.home.ui.clientnotes

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class ComponentData(
    val id: String? = "",
    val name: String? = "",
    val selector: String? = "",
    val query: String? = "",
    val value: String? = ""
) : Parcelable {
}
