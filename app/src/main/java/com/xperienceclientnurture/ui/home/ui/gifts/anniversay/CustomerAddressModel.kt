package com.xperienceclientnurture.ui.home.ui.gifts.anniversay


data class CustomerAddressModel(
    var firstName: String="",
    var lastName: String="",
    var address: String="",
    var city: String="",
    var state: String="",
    var zipCode: String="",
    var phone: String="",
    var email: String="",
    var message: String=""
)