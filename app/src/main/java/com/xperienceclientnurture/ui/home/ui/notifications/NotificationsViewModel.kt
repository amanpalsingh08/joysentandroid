package com.xperienceclientnurture.ui.home.ui.notifications

import android.util.Log
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.utils.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class NotificationsViewModel : BaseViewModel() {

    val updateSendData = SingleLiveData<List<RequestSendDetails>>()
    val updateReceivedData = SingleLiveData<List<RequestReceivedDetails>>()
    val updateSubscriptionData = SingleLiveData<List<SubscriptionRequest>>()
    val progress = SingleLiveData<Boolean>()
    val noData = SingleLiveData<Boolean>()
    var isSent = true

    fun getSendRequests() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getRequestsSend(Prefs.getString(Constants.USER_ID, ""))
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            when (it.status_code) {
                                200 -> {
                                    if (it.data==null)
                                        noData.postValue(true)
                                    val list = it.data ?: arrayListOf()
                                    if (list.isEmpty()) {
                                        noData.postValue(true)
                                    } else {
                                        noData.postValue(false)
                                    }
                                    updateSendData.postValue(list)
                                }
                                400 ->{
                                    noData.postValue(true)
                                }
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    fun getReceivedRequests() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getRequestsReceived(Prefs.getString(Constants.USER_ID, ""))
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            when (it.status_code) {
                                200 -> {
                                    if (it.data==null)
                                        noData.postValue(true)
                                    val list = it.data.requests ?: arrayListOf()
                                    if (list.isEmpty()) {
                                        noData.postValue(true)
                                    } else {
                                        noData.postValue(false)
                                    }
                                    updateReceivedData.postValue(list)
                                }
                                400->{
                                    noData.postValue(true)
                                }
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }
    fun getSubscriptionRequests() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getRequestsReceived(Prefs.getString(Constants.USER_ID, ""))
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            when (it.status_code) {
                                200 -> {
                                    if (it.data==null)
                                        noData.postValue(true)
                                    val list = it.data.subscription_requests
                                    if (list.isEmpty()) {
                                        noData.postValue(true)
                                    } else {
                                        noData.postValue(false)
                                    }
                                    updateSubscriptionData.postValue(list)
                                }
                                400->{
                                    noData.postValue(true)
                                }
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }


    fun cancelRequest(requestID: String) {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response =
                service.getCancelRequest(Prefs.getString(Constants.USER_ID, ""), requestID)
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                if (isSent)
                                    getSendRequests()
                                else
                                    getReceivedRequests()
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    fun acceptRequest(requestID: String,type :String) {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response =
                service.getAcceptRequest(Prefs.getString(Constants.USER_ID, ""), requestID)
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                when(type){
                                    Constants.ACCEPT_RECEIVED_REQUEST->{
                                        getReceivedRequests()
                                    }
                                    Constants.ACCEPT_SUBSCRIPTION_REQUEST->{
                                        getSubscriptionRequests()
                                    }
                                }
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

}