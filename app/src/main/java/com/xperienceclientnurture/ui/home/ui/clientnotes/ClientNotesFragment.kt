package com.xperienceclientnurture.ui.home.ui.clientnotes

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.parseAsHtml
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.airbnb.epoxy.AsyncEpoxyController
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.Mavericks
import com.airbnb.mvrx.MavericksView
import com.airbnb.mvrx.activityViewModel
import com.airbnb.mvrx.withState
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseMavericksFragment
import com.xperienceclientnurture.common.simpleController
import com.xperienceclientnurture.databinding.FragmentClientNotesBinding
import com.xperienceclientnurture.interfaces.DialogClickListener
import com.xperienceclientnurture.ui.home.ui.HolderActivity
import com.xperienceclientnurture.ui.home.ui.epoxyModel.listViewClientNotes
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.showCustomDialog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class ClientNotesFragment : BaseMavericksFragment(), MavericksView, DialogClickListener {

    private lateinit var binding: FragmentClientNotesBinding
    private val viewModel: ClientNotesViewModel by activityViewModel()
    private val epoxyController: AsyncEpoxyController by lazy { epoxyController() }
    private var viewLayout: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        if (viewLayout == null) {
            binding = FragmentClientNotesBinding.inflate(inflater, container, false)
            viewLayout = binding.root
        }
        return viewLayout as View
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.setController(epoxyController)
        viewModel.getClientNotes()

        binding.fab.setOnClickListener {
            onAddEdit(ClientNotes())
        }

        onUpdatedNotes = {
            viewModel.getClientNotes()
        }
    }

    override fun invalidate() = withState(viewModel) { state ->
        binding.flLoading.isVisible = state.request is Loading || state.requestDelete is Loading
        epoxyController.requestModelBuild()
    }

    private fun epoxyController() = simpleController(viewModel) { state ->

        if (state.listClientNotes.isNotEmpty()) {
            state.listClientNotes.forEach { obj ->
                listViewClientNotes {
                    id(obj.id)
                    data(obj)
                    viewListener { model, _, _, _ -> onViewNote(model.data()) }
                    clickListener { model, _, _, _ -> onAddEdit(model.data()) }
                    deleteListener { model, _, _, _ -> onDelete(model.data()) }
                }
            }
        }
    }

    private fun onAddEdit(data: ClientNotes) {
        startActivity(
            Intent(activity, HolderActivity::class.java)
                .putExtra(Constants.SCREEN_NAME, Constants.ADD_EDIT_CLIENT_NOTES)
                .putExtra(Mavericks.KEY_ARG, data)
        )
    }

    private fun onDelete(data: ClientNotes) {
        viewModel.noteID = data.id?.toString() ?: ""
        showCustomDialog(
            requireContext(),
            Constants.LOGOUT,
            Constants.TWO_CLICK_LISTENER,
            this,
            getString(R.string.title_delete_note),
            getString(R.string.text_delete_note),
            getString(R.string.txt_no),
            getString(R.string.txt_yes)
        )
    }

    private fun onViewNote(data: ClientNotes) {
        viewLifecycleOwner.lifecycleScope.launch {
            showComponentSheetDialog(
                requireContext(), ComponentData(
                    id = data.id?.toString(),
                    name= "Notes",
                    value = data.notes?.parseAsHtml()?.toString()?.trim()
                )
            )
        }
    }

    private suspend fun showComponentSheetDialog(
        context: Context,
        componentData: ComponentData
    ) = withContext(Dispatchers.Main) {
        suspendCoroutine {

            ComponentBuilder(context, componentData)
                .onApply { name, query, _ ->
                    executeContinuation(it, name, query)
                }
                .onReset { name, query, _ ->
                    executeContinuation(it, name, query)
                }
                .onCancel {
                    it.resume(null)
                }
                .show()
        }
    }

    private fun executeContinuation(continuation: Continuation<ComponentMetaData?>, name: String, query: String) {
        continuation.resume(ComponentMetaData(name = name, query = query))
    }


    override fun onDialogYesClick(click: String) {
        if (viewModel.noteID.isNotEmpty())
            viewModel.deleteClientNote()
    }

    override fun onDialogNoClick(click: String) {
        viewModel.noteID = ""
    }

}