package com.xperienceclientnurture.ui.home.ui.gifts.anniversay

import android.util.Log
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.ui.commonmodel.CommonDataModel
import com.xperienceclientnurture.ui.home.ui.addeditclient.ClientDetails
import com.xperienceclientnurture.utils.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class AnniversaryDetailViewModel : BaseViewModel() {

    var clientID = ""
    var date = ""
    var quantity = ""
    var message = ""
    var addressObj = CustomerAddressModel()
    val progress = SingleLiveData<Boolean>()
    val listener = SingleLiveData<String>()
    val purchaseSuccess = SingleLiveData<CommonDataModel>()
    val updateClients = SingleLiveData<List<ClientDetails>>()
    val updateDates = SingleLiveData<List<String>>()
    val updateDetail = SingleLiveData<AnniversaryDetailDataModel>()
    val navigateCardScreen=SingleLiveData<Boolean>()


    fun getMyClients() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getClients(Prefs.getString(Constants.USER_ID, ""))
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                updateClients.postValue(it.data)

                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    fun getDateList() {
//        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getDateList()
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
//                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                updateDates.postValue(it.data)

                            }
                        }
                    } else {
//                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
//                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
//                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    fun getAnniversaryDetailByClient(
        giftID: String,
        clientID: String,
        quantity: String,
        message: String
    ) {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getAnniversaryDetail(giftID, clientID, quantity, message)
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                updateDetail.postValue(it)

                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    fun getCustomerShippingPrice(
        giftID: String
    ) {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getCustomerShippingPrice(giftID, addressObj.zipCode)
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                updateDetail.postValue(it)

                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    fun purchase(giftID: String) {
        /* val params = HashMap<String, String>()
         params["user_id"] = Prefs.getString(Constants.USER_ID, "")
         params["client_id"] = clientID
         params["gift_id"] = giftID
         params["message"] = message
         params["quantity"] = quantity*/

        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.purchaseGiftDecor(
                userID = Prefs.getString(Constants.USER_ID, ""),
                clientId = clientID, giftId = giftID, message = message, quantity = quantity
            )
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                purchaseSuccess.postValue(it)

                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    fun purchaseCustomer(giftID: String) {
        val params = HashMap<String, String>()
        params["user_id"] = Prefs.getString(Constants.USER_ID, "")
        params["gift_id"] = giftID
        params["message"] = addressObj.message
        params["firstname"] = addressObj.firstName
        params["lastname"] = addressObj.lastName
        params["email"] = addressObj.email
        params["phone"] = addressObj.phone
        params["address"] = addressObj.address
        params["state"] = addressObj.state
        params["city"] = addressObj.city
        params["zipcode"] = addressObj.zipCode


        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.purchaseCustomer(params)
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            when (it.status_code) {
                                200 -> {
                                    purchaseSuccess.postValue(it)
                                }
                                400 -> {
                                    if (it.message.equals(Constants.CARD_ERROR)){
                                        navigateCardScreen.postValue(true)
                                    }

                                }
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }


}
