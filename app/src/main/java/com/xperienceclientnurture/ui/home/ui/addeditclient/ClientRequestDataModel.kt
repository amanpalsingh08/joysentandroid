package com.xperienceclientnurture.ui.home.ui.addeditclient
import com.google.gson.annotations.SerializedName


data class ClientRequestDataModel(
    @SerializedName("data")
    val `data`: ClientRequestData,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val statusCode: Int
)

data class ClientRequestData(
    @SerializedName("connection1")
    val connection1: String,
    @SerializedName("connection1_contri")
    val connection1Contri: Int,
    @SerializedName("connection1_status")
    val connection1Status: String,
    @SerializedName("connection2")
    val connection2: String,
    @SerializedName("connection2_contri")
    val connection2Contri: Int,
    @SerializedName("connection3")
    val connection3: String,
    @SerializedName("connection3_contri")
    val connection3Contri: Int,
    @SerializedName("connection_type")
    val connectionType: String
)