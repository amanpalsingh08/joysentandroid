package com.xperienceclientnurture.ui.home.ui.addeditclient

import android.util.Log
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.ui.commonmodel.UsersConnectionDetails
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.Validators
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class RequestConnectionViewModel : BaseViewModel() {

    var isConnection2 = false
    var isConnection1 = false
    var isDefault = false
    var isCustom = false
    val progress = SingleLiveData<Boolean>()
    val listener = SingleLiveData<String>()
    val updateConnections = SingleLiveData<List<UsersConnectionDetails>>()
    val observerClientRequest = SingleLiveData<ClientRequestDataModel>()
    val observerPaymentDetails = SingleLiveData<PaymentData>()
    var selfContri = ""
    var connection1Contri = ""
    var connection2Contri = ""
    var clientID = ""
    var connection1Value = ""
    var connection2Value = ""

    fun submit() {

        if (!Validators().validateRequestConnection(connection1Value, connection2Value)) {
            showToast(Validators.errorMessage.toString())
        } else {

            val params = HashMap<String, String>()

            //Need to be updated

            params["connection1"] = connection1Value
            params["connection2"] = connection2Value
            params["client_id"] = clientID
            params["user_id"] = Prefs.getString(Constants.USER_ID, "")



            progress.postValue(true)
            val service = RetrofitFactory.makeRetrofitService()
            CoroutineScope(Dispatchers.IO).launch {
                val response = service.postRequestConnection(params)
                try {

                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {
                            progress.postValue(false)
                            response.body()?.let {
                                showToast(it.message)
                                listener.postValue(Constants.BACK)

                            }
                        } else {
                            progress.postValue(false)
                            showToast("Error network operation failed with ${response.code()}")
                        }
                    }
                } catch (e: HttpException) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Exception ${e.message}")
                } catch (e: Throwable) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Ooops: Something else went wrong")
                }
            }
        }
    }

    fun pay100Percent() {

        val params = HashMap<String, String>()

        //Need to be updated

        params["client_id"] = clientID
        params["user_id"] = Prefs.getString(Constants.USER_ID, "")

        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.pay100Percent(params)
            try {

                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            showToast(it.message)
                            listener.postValue(Constants.BACK)

                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    fun getUserConnectionList() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getUsersConnection()
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                updateConnections.postValue(it.data)

                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    fun getClientRequestDetails() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getClientRequest(clientID)
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            observerClientRequest.postValue(it)
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    fun getPaymentDetails() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getPaymentDetails()
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.statusCode == 200) {
                                observerPaymentDetails.postValue(it.paymentData)

                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    fun submitRequest() {

        if (isConnection1) {
            if (isDefault) {
                requestConnection()
            } else if (isCustom) {
                requestConnection()
            }
        } else if (isConnection2) {
            if (isDefault) {
                requestConnections()
            } else if (isCustom) {
                requestConnections()
            }
        }


        if (isDefault) {
            requestConnection()
        } else if (isCustom) {
            requestConnections()
        }
    }

    private fun requestConnection() {

        val params = HashMap<String, String>()
        params["user_id"] = Prefs.getString(Constants.USER_ID, "")
        params["client_id"] = clientID
        params["connection"] = connection1Value
        if (isDefault) {
            params["self_contri"] = "50"
            params["conn_contri"] = "50"
        } else {
            params["self_contri"] = selfContri
            params["conn_contri"] = connection1Contri
        }
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.requestConnection(params)
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                showToast(it.message)
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }

    }

    private fun requestConnections() {

        val params = HashMap<String, String>()
        params["user_id"] = Prefs.getString(Constants.USER_ID, "")
        params["client_id"] = clientID
        params["connection1"] = connection1Value
        params["connection2"] = connection2Value
        params["self_contri"] = selfContri
        params["conn1_contri"] = connection1Contri
        params["conn2_contri"] = connection2Contri
        if (isDefault) {
            params["self_contri"] = "33.33"
            params["conn1_contri"] = "33.33"
            params["conn2_contri"] = "33.33"
        } else {
            params["self_contri"] = selfContri
            params["conn1_contri"] = connection1Contri
            params["conn2_contri"] = connection2Contri
        }

        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.requestConnections(params)
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                showToast(it.message)
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }

    }


}
