package com.xperienceclientnurture.ui.home.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.xperienceclientnurture.R
import com.xperienceclientnurture.ui.home.ui.gallery.GalleryDetails

class GalleryAdapter(var list: List<GalleryDetails>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        Glide
            .with((holder as ViewHolder).image.context)
            .load(list[position].img)
            .into(holder.image)

        holder.tvDescription.text=list[position].description


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_gallery,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun updateList(it: List<GalleryDetails>) {
        list = it
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.findViewById(R.id.iv_gallery_image)
        val tvDescription: TextView = itemView.findViewById(R.id.tv_description)

    }

}




