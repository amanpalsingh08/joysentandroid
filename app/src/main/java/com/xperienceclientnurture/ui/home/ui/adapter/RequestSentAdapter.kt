package com.xperienceclientnurture.ui.home.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.xperienceclientnurture.R
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.home.ui.notifications.RequestSendDetails
import com.xperienceclientnurture.utils.Constants

class RequestSentAdapter(var list: List<RequestSendDetails>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var listener: OnItemClickListener

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).name.text = list[position].client_name
        holder.email.text = list[position].client_email
        holder.connection1Value.text = list[position].conn1
        holder.connection2Value.text = list[position].conn2


        holder.connection2Status.text=list[position].request_status.capitalize()
        if (list[position].request_status.equals("pending", ignoreCase = true)) {
            holder.connection2Status.background =
                ContextCompat.getDrawable(holder.connection2Status.context, R.drawable.bg_red_solid_4dp)
            holder.tvCancel.visibility = View.VISIBLE

        } else if (list[position].request_status.equals("connected", ignoreCase = true)) {
            holder.connection2Status.background =
                ContextCompat.getDrawable(
                    holder.connection2Status.context,
                    R.drawable.bg_light_green_solid_4dp
                )
            holder.tvCancel.visibility = View.INVISIBLE

        }

        holder.setItemSelectedListener(listener)


    }

    fun setItemSelectedListener(listener: OnItemClickListener) {
        this.listener = listener
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_request_sent,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun updateList(it: List<RequestSendDetails>) {
        list = it
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val name: TextView = itemView.findViewById(R.id.tv_client_name)
        val email: TextView = itemView.findViewById(R.id.tv_client_email)
        val connection1Value: TextView = itemView.findViewById(R.id.tv_connection1_value)
        val connection2Value: TextView = itemView.findViewById(R.id.tv_connection2_value)
        val connection2Status: TextView = itemView.findViewById(R.id.tv_connection2_status)
        val tvCancel: TextView = itemView.findViewById(R.id.tv_cancel)
        lateinit var listener: OnItemClickListener


        init {
            tvCancel.setOnClickListener(this)
        }

        fun setItemSelectedListener(listener: OnItemClickListener) {
            this.listener = listener
        }

        override fun onClick(p0: View?) {
            when (p0!!.id) {
                R.id.tv_cancel -> {
                    println("Text Value : ${tvCancel.text}")

                    listener.onClick(adapterPosition, Constants.CANCEL_SENT_REQUEST)
                }
            }
        }

    }


}




