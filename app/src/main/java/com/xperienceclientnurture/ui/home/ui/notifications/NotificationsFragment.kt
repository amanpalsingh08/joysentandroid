package com.xperienceclientnurture.ui.home.ui.notifications

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentNotificationsBinding
import com.xperienceclientnurture.interfaces.DialogClickListener
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.forgotpassword.NotificationTitleModel
import com.xperienceclientnurture.ui.home.ui.adapter.NotificationTitleAdapter
import com.xperienceclientnurture.ui.home.ui.adapter.RequestReceivedAdapter
import com.xperienceclientnurture.ui.home.ui.adapter.RequestSentAdapter
import com.xperienceclientnurture.ui.home.ui.adapter.RequestSubscriptionAdapter
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.showCustomDialog

class NotificationsFragment : BaseFragment<FragmentNotificationsBinding, NotificationsViewModel>(),
    View.OnClickListener, OnItemClickListener, DialogClickListener,
    OnItemClickListener.OnTitleClickListener {

    override val fragmentLayoutId: Int
        get() = R.layout.fragment_notifications
    override val viewModelClass: Class<NotificationsViewModel>
        get() = NotificationsViewModel::class.java

    private lateinit var sentAdapter: RequestSentAdapter
    private lateinit var receivedAdapter: RequestReceivedAdapter
    private lateinit var subscriptionAdapter: RequestSubscriptionAdapter
    private lateinit var notificationAdapter: NotificationTitleAdapter

    private var listReceived: List<RequestReceivedDetails> = arrayListOf()
    private var listSubscription: List<SubscriptionRequest> = arrayListOf()
    private var listSent: List<RequestSendDetails> = arrayListOf()
    private var listNotification: MutableList<NotificationTitleModel> = arrayListOf()

    var position: Int = 0
    var type: String = ""

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        setTitleAdapter()

        setObservers()

        viewModel.isSent = true
        viewModel.getSendRequests()

    }

    private fun setTitleAdapter() {
        listNotification.add(
            NotificationTitleModel(
                isPressed = true,
                title = getString(R.string.sent)
            )
        )
        listNotification.add(
            NotificationTitleModel(
                isPressed = false,
                title = getString(R.string.received)
            )
        )
        listNotification.add(
            NotificationTitleModel(
                isPressed = false,
                title = getString(R.string.text_subscription)
            )
        )

        notificationAdapter = NotificationTitleAdapter(listNotification)
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        binding.rvTitleNotification.layoutManager = linearLayoutManager

        binding.rvTitleNotification.adapter = notificationAdapter

        notificationAdapter.setItemSelectedListener(this)


    }

    private fun setObservers() {

        viewModel.progress.observe(viewLifecycleOwner) {

            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }

        }

        viewModel.updateSendData.observe(viewLifecycleOwner) {
            if (it != null) {
                setSentData(it)
                viewModel.updateSendData.value = null

            }
        }

        viewModel.updateReceivedData.observe(viewLifecycleOwner) {
            if (it != null) {
                setReceivedData(it)
                viewModel.updateReceivedData.value = null

            }
        }
        viewModel.updateSubscriptionData.observe(viewLifecycleOwner) {
            if (it != null) {
                setSubscriptionData(it)
                viewModel.updateSubscriptionData.value = null

            }
        }

        viewModel.noData.observe(viewLifecycleOwner) {

            if (it) {
                binding.noDataFound.root.visibility = View.VISIBLE
            } else {
                binding.noDataFound.root.visibility = View.GONE
            }

        }

        sentAdapter = RequestSentAdapter(listSent)
        receivedAdapter = RequestReceivedAdapter(listReceived)
        subscriptionAdapter = RequestSubscriptionAdapter(listSubscription)

        binding.recyclerView.layoutManager = LinearLayoutManager(context)

        binding.recyclerView.adapter = sentAdapter


    }

    private fun setSentData(it: List<RequestSendDetails>) {
        listSent = it
        binding.recyclerView.adapter = sentAdapter
        sentAdapter.updateList(listSent)
        sentAdapter.setItemSelectedListener(this)
    }

    private fun setReceivedData(it: List<RequestReceivedDetails>) {
        listReceived = it
        binding.recyclerView.adapter = receivedAdapter
        receivedAdapter.updateList(listReceived)
        receivedAdapter.setItemSelectedListener(this)
    }

    private fun setSubscriptionData(it: List<SubscriptionRequest>) {
        listSubscription = it
        binding.recyclerView.adapter = subscriptionAdapter
        subscriptionAdapter.updateList(listSubscription)
        subscriptionAdapter.setItemSelectedListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.tv_sent -> {
                viewModel.isSent = true
                binding.tvSent.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_blue_solid_4dp)
                binding.tvSent.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        android.R.color.white
                    )
                )
                binding.tvReceived.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_gray_solid_4dp)
                binding.tvReceived.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        android.R.color.black
                    )
                )
                setSentData(arrayListOf())
                setReceivedData(arrayListOf())

                viewModel.getSendRequests()
            }
            R.id.tv_received -> {
                viewModel.isSent = false
                binding.tvReceived.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_blue_solid_4dp)
                binding.tvReceived.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        android.R.color.white
                    )
                )
                binding.tvSent.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_gray_solid_4dp)
                binding.tvSent.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        android.R.color.black
                    )
                )

                setSentData(arrayListOf())
                setReceivedData(arrayListOf())

                viewModel.getReceivedRequests()
            }
        }
    }

    override fun onClick(position: Int, type: String) {
        this.position = position
        this.type = type
        when (type) {
            Constants.CANCEL_SENT_REQUEST -> {

                showCustomDialog(
                    requireContext(),
                    Constants.LOGOUT,
                    Constants.TWO_CLICK_LISTENER,
                    this,
                    Constants.CANCEL_REQUEST,
                    getString(R.string.message_cancel_request),
                    getString(R.string.txt_cancel),
                    getString(R.string.txt_ok)
                )

            }

            Constants.CANCEL_RECEIVED_REQUEST -> {
                showCustomDialog(
                    requireContext(),
                    Constants.LOGOUT,
                    Constants.TWO_CLICK_LISTENER,
                    this,
                    Constants.CANCEL_REQUEST,
                    getString(R.string.message_cancel_request),
                    getString(R.string.txt_no),
                    getString(R.string.txt_yes)
                )
            }

            Constants.ACCEPT_RECEIVED_REQUEST -> {
                showCustomDialog(
                    requireContext(),
                    Constants.LOGOUT,
                    Constants.TWO_CLICK_LISTENER,
                    this,
                    Constants.ACCEPT_REQUEST,
                    getString(R.string.message_accept_request),
                    getString(R.string.txt_close),
                    getString(R.string.txt_accept)
                )
            }
            Constants.ACCEPT_SUBSCRIPTION_REQUEST -> {
                showCustomDialog(
                    requireContext(),
                    Constants.LOGOUT,
                    Constants.TWO_CLICK_LISTENER,
                    this,
                    Constants.ACCEPT_REQUEST,
                    getString(R.string.message_accept_request),
                    getString(R.string.txt_close),
                    getString(R.string.txt_accept)
                )
            }
        }
    }

    override fun onDialogYesClick(click: String) {
        when (type) {
            Constants.CANCEL_SENT_REQUEST -> {
                viewModel.cancelRequest(listSent[position].request_id.toString())
            }
            Constants.CANCEL_RECEIVED_REQUEST -> {
                viewModel.cancelRequest(listReceived[position].request_id.toString())
            }
            Constants.ACCEPT_RECEIVED_REQUEST -> {
                viewModel.acceptRequest(listReceived[position].request_id.toString(),type)
            }
            Constants.ACCEPT_SUBSCRIPTION_REQUEST -> {
                viewModel.acceptRequest(listSubscription[position].requestId.toString(),type)
            }
        }

    }

    override fun onDialogNoClick(click: String) {

    }

    override fun onTitleClick(position: Int, type: String) {

        binding.noDataFound.root.visibility = View.GONE

        for ((index, value) in listNotification.withIndex()) {
            value.isPressed = index == position
        }

        notificationAdapter.updateList(listNotification)

        when (type) {
            getString(R.string.sent) -> {
                setSentData(arrayListOf())
                setReceivedData(arrayListOf())
                setSubscriptionData(arrayListOf())

                viewModel.getSendRequests()
            }
            getString(R.string.received) -> {
                setSentData(arrayListOf())
                setReceivedData(arrayListOf())
                setSubscriptionData(arrayListOf())

                viewModel.getReceivedRequests()
            }
            getString(R.string.text_subscription) -> {
                setSentData(arrayListOf())
                setReceivedData(arrayListOf())
                setSubscriptionData(arrayListOf())

                viewModel.getSubscriptionRequests()
            }
        }


    }

}