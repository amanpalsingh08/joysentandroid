package com.xperienceclientnurture.ui.home.ui.clientnotes

data class ComponentMetaData(
    val name: String? = "",
    val query: String? = "",
    val queryMap: Map<String, String>? = mapOf()
)
