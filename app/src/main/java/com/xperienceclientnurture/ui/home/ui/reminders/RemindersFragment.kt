package com.xperienceclientnurture.ui.home.ui.reminders

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.airbnb.epoxy.AsyncEpoxyController
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.Mavericks
import com.airbnb.mvrx.MavericksView
import com.airbnb.mvrx.activityViewModel
import com.airbnb.mvrx.withState
import com.xperienceclientnurture.common.simpleController
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseMavericksFragment
import com.xperienceclientnurture.databinding.FragmentRemindersBinding
import com.xperienceclientnurture.interfaces.DialogClickListener
import com.xperienceclientnurture.ui.home.ui.HolderActivity
import com.xperienceclientnurture.ui.home.ui.epoxyModel.listViewReminders
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.showCustomDialog

class RemindersFragment : BaseMavericksFragment(), MavericksView, DialogClickListener {

    private lateinit var binding: FragmentRemindersBinding
    private val viewModel: RemindersViewModel by activityViewModel()
    private val epoxyController: AsyncEpoxyController by lazy { epoxyController() }
    private var viewLayout: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        if (viewLayout == null) {
            binding = FragmentRemindersBinding.inflate(inflater, container, false)
            viewLayout = binding.root
        }
        return viewLayout as View
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.setController(epoxyController)
        viewModel.getReminders()

        binding.fab.setOnClickListener {
            onAddEdit(Reminders())
        }

        onUpdatedReminders = {
            viewModel.getReminders()
        }
    }

    override fun invalidate() = withState(viewModel) { state ->
        binding.flLoading.isVisible = state.request is Loading || state.requestDelete is Loading
        epoxyController.requestModelBuild()
    }

    private fun epoxyController() = simpleController(viewModel) { state ->

        if (state.listReminders.isNotEmpty()) {
            state.listReminders.forEach { obj ->
                listViewReminders {
                    id(obj.id)
                    data(obj)
                    clickListener { model, _, _, _ -> onAddEdit(model.data()) }
                    deleteListener { model, _, _, _ -> onDelete(model.data()) }
                }
            }
        }
    }

    private fun onAddEdit(data: Reminders) {
        startActivity(
            Intent(activity, HolderActivity::class.java)
                .putExtra(Constants.SCREEN_NAME, Constants.ADD_EDIT_REMINDERS)
                .putExtra(Mavericks.KEY_ARG, data)
        )
    }

    private fun onDelete(data: Reminders) {
        viewModel.reminderID = data.id?.toString() ?: ""
        showCustomDialog(
            requireContext(),
            Constants.LOGOUT,
            Constants.TWO_CLICK_LISTENER,
            this,
            getString(R.string.title_delete_reminder),
            getString(R.string.text_delete_reminder),
            getString(R.string.txt_no),
            getString(R.string.txt_yes)
        )

    }

    override fun onDialogYesClick(click: String) {
        if (viewModel.reminderID.isNotEmpty())
            viewModel.deleteClientNote()
    }

    override fun onDialogNoClick(click: String) {
        viewModel.reminderID = ""
    }

}