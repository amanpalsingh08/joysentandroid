package com.xperienceclientnurture.ui.home.ui.gifts

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentGiftListBinding
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.ui.home.ui.HolderActivity
import com.xperienceclientnurture.ui.home.ui.adapter.GiftsListAdapter
import com.xperienceclientnurture.utils.Constants

class GiftListFragment : BaseFragment<FragmentGiftListBinding, GiftListViewModel>(),
    OnItemClickListener {

    private lateinit var listGiftsType: MutableList<GiftsTypeData>
    override val fragmentLayoutId: Int
        get() = R.layout.fragment_gift_list
    override val viewModelClass: Class<GiftListViewModel>
        get() = GiftListViewModel::class.java

    lateinit var adapter: GiftsListAdapter


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        subscriberObservers()

//        viewModel.getGiftsType()


    }

    private fun subscriberObservers() {
        viewModel.progress.observe(this, Observer {
            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }
        })
        listGiftsType = arrayListOf()
        val obj = GiftsTypeData(
            type_name = "Subscription",
            id = -1,
            created_at = "",
            parent_id = "",
            type_slug = "Subscription",
            updated_at = "",
            status = 1
        )
        if (!Prefs.getString(Constants.USER_ROLE, "").equals(
                Constants.CUSTOMER,
                ignoreCase = false
            )
        )
            listGiftsType.add(obj)
        setGiftTypeAdapter()

        /*viewModel.observerGiftTypeList.observe(this, Observer {
            if (it != null) {
                listGiftsType = arrayListOf()
                val obj = GiftsTypeData(
                    type_name = "Subscription",
                    id = -1,
                    created_at = "",
                    parent_id = "",
                    type_slug = "Subscription",
                    updated_at = "",
                    status = 1
                )

                if (!Prefs.getString(Constants.USER_ROLE, "").equals(
                        Constants.CUSTOMER,
                        ignoreCase = false
                    )
                )
                    listGiftsType.add(obj)
                listGiftsType.addAll(it)
                viewModel.observerGiftTypeList.value = null
                setGiftTypeAdapter()
            }
        })*/
    }

    private fun setGiftTypeAdapter() {
        adapter = GiftsListAdapter(listGiftsType)

        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        adapter.setItemSelectedListener(this)
    }

    override fun onClick(position: Int, type: String) {
        when (type) {
            Constants.GIFTS_ITEM -> {
                if (Prefs.getString(Constants.USER_ROLE, "").equals(
                        Constants.CUSTOMER,
                        ignoreCase = false
                    )
                ) {
                    startActivity(
                        Intent(activity, HolderActivity::class.java)
                            .putExtra(
                                Constants.GIFT_TYPE,
                                listGiftsType[position].id.toString()
                            )
                            .putExtra(
                                Constants.GIFT_TYPE_NAME,
                                listGiftsType[position].type_name
                            )
                            .putExtra(Constants.SCREEN_NAME, Constants.SCREEN_GIFTS_ANNIVERSARY)
                    )
                } else {
                    when (position) {
                        0 -> {
                            startActivity(
                                Intent(activity, HolderActivity::class.java)
                                    .putExtra(Constants.SCREEN_NAME, Constants.SCREEN_GIFTS)
                            )
                        }

                        else -> {
                            startActivity(
                                Intent(activity, HolderActivity::class.java)
                                    .putExtra(
                                        Constants.GIFT_TYPE,
                                        listGiftsType[position].id.toString()
                                    )
                                    .putExtra(
                                        Constants.GIFT_TYPE_NAME,
                                        listGiftsType[position].type_name
                                    )
                                    .putExtra(
                                        Constants.SCREEN_NAME,
                                        Constants.SCREEN_GIFTS_ANNIVERSARY
                                    )
                            )
                        }

                    }
                }


            }
        }
    }


}
