package com.xperienceclientnurture.ui.home.ui.clientnotes

import android.content.Context
import com.airbnb.mvrx.MavericksState
import com.airbnb.mvrx.MavericksViewModel
import com.airbnb.mvrx.MavericksViewModelFactory
import com.airbnb.mvrx.ViewModelContext


data class ComponentState(
    val parent: ComponentData?
) : MavericksState


class ComponentViewModel(
    val context: Context,
    stateChipCreate: ComponentState
) : MavericksViewModel<ComponentState>(stateChipCreate) {

    companion object : MavericksViewModelFactory<ComponentViewModel, ComponentState> {


        override fun create(
            viewModelContext: ViewModelContext,
            state: ComponentState
        ) = ComponentViewModel(viewModelContext.activity(), state)
    }
}
