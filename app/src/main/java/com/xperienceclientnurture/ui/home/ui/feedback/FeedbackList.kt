package com.xperienceclientnurture.ui.home.ui.feedback

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager

import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentFeedbackListBinding
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.home.ui.HolderActivity
import com.xperienceclientnurture.ui.home.ui.adapter.FeedbackAdapter
import com.xperienceclientnurture.utils.Constants

class FeedbackList : BaseFragment<FragmentFeedbackListBinding, FeedbackListViewModel>(),
    OnItemClickListener, View.OnClickListener {
    private lateinit var list: List<FeedbackDetails>
    private lateinit var adapter: FeedbackAdapter
    override val fragmentLayoutId: Int
        get() = R.layout.fragment_feedback_list
    override val viewModelClass: Class<FeedbackListViewModel>
        get() = FeedbackListViewModel::class.java


    private val feedbackReceiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            viewModel.getFeedbackList()
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        requireActivity().registerReceiver(feedbackReceiver, IntentFilter(Constants.RECEIVER_FEEDBACK))

        setObserver()

        binding.fab.setOnClickListener(this)
        list = arrayListOf()
        adapter = FeedbackAdapter(list)

        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        adapter.setItemSelectedListener(this)
        viewModel.getFeedbackList()
    }

    private fun setObserver() {

        viewModel.updateUI.observe(viewLifecycleOwner) {
            if (it != null) {
                setData(it)
            }
        }

        viewModel.progress.observe(viewLifecycleOwner) {

            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }

        }

        viewModel.noData.observe(viewLifecycleOwner) {

            if (it) {
                binding.noDataFound.root.visibility = View.VISIBLE
            } else {
                binding.noDataFound.root.visibility = View.GONE
            }

        }
    }

    private fun setData(it: List<FeedbackDetails>) {
        list = it
        adapter.updateList(list)
    }

    override fun onDestroy() {
        super.onDestroy()
        requireActivity().unregisterReceiver(feedbackReceiver)
    }

    override fun onClick(position: Int, type: String) {
        when (type) {
            Constants.DELETE_ITEM -> {
                viewModel.deleteFeedback(list[position].feedback_id.toString())
            }

            Constants.EDIT_ITEM -> {
                startActivity(
                    Intent(activity, HolderActivity::class.java)
                        .putExtra(Constants.SCREEN_NAME, Constants.SCREEN_ADD_EDIT_FEEDBACK)
                        .putExtra(Constants.ARGS_IS_EDIT, true)
                        .putExtra(Constants.ARGS_FEEDBACK_ID, list[position].feedback_id.toString())
                        .putExtra(Constants.ARGS_FEEDBACK, list[position].feedback)
                        .putExtra(Constants.ARGS_CLIENT_ID, list[position].client_id)
                )
            }
        }
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.fab -> {
                startActivity(
                    Intent(activity, HolderActivity::class.java)
                        .putExtra(Constants.SCREEN_NAME, Constants.SCREEN_ADD_EDIT_FEEDBACK)
                        .putExtra(Constants.ARGS_IS_EDIT, false)
                )
            }
        }
    }
}
