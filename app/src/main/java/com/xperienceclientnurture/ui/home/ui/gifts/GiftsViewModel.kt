package com.xperienceclientnurture.ui.home.ui.gifts

import android.util.Log
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.ui.commonmodel.CommonDataModel
import com.xperienceclientnurture.ui.commonmodel.UsersConnectionDetails
import com.xperienceclientnurture.ui.home.ui.addeditclient.ClientDetails
import com.xperienceclientnurture.utils.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class GiftsViewModel : BaseViewModel() {

    var isConnection2 = false
    var isConnection1 = false
    var isDefault = false
    var isCustom = false

    var clientID = ""
    var frequency = "Quarterly"
    var gifts = "8"
    var total = ""
    var connection1Value = ""
    var connection2Value = ""
    var selfContri = ""
    var connection1Contri = ""
    var connection2Contri = ""
    var months = ""
    var subscriptionType = ""

    val progress = SingleLiveData<Boolean>()
    val listener = SingleLiveData<String>()
    val updateConnections = SingleLiveData<List<UsersConnectionDetails>>()
    val updateClients = SingleLiveData<List<ClientDetails>>()
    val updatePrices = SingleLiveData<PaymentDetailData>()
    val purchase = SingleLiveData<CommonDataModel>()

    fun getUserConnectionList() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getUsersConnection()
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                updateConnections.postValue(it.data)

                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    fun getMyClients() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getActiveClients(Prefs.getString(Constants.USER_ID, ""))
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                updateClients.postValue(it.data)

                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    fun getTotal() {

        val params = HashMap<String, String>()

        //Need to be updated

        params["user_id"] = Prefs.getString(Constants.USER_ID, "")
        params["gifts"] = gifts

        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.postTotal(params)
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                updatePrices.postValue(it.data)
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

    fun submitRequest() {

        if (isConnection1) {
            if (isDefault) {
                requestConnection1()
            } else if (isCustom) {
                requestConnection1()
            }
        } else if (isConnection2) {
            if (isDefault) {
                requestConnection2()
            } else if (isCustom) {
                requestConnection2()
            }
        }
        if (isDefault) {
            requestConnection1()
        } else if (isCustom) {
            requestConnection2()
        }
    }

    private fun requestConnection1() {

        val params = HashMap<String, String>()
        params["user_id"] = Prefs.getString(Constants.USER_ID, "")
        params["client_id"] = clientID
        params["frequency"] = frequency
        params["gifts"] = gifts
        params["total"] = total
        params["conn"] = connection1Value
        if (isDefault) {
            params["self_contri"] = "50"
            params["conn_contri"] = "50"
        } else {
            params["self_contri"] = selfContri
            params["conn_contri"] = connection1Contri
        }
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.postRequestConnection1(params)
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                purchase.postValue(it)
                                showToast(it.message)
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }

    }

    private fun requestConnection2() {

        val params = HashMap<String, String>()
        params["user_id"] = Prefs.getString(Constants.USER_ID, "")
        params["client_id"] = clientID
        params["frequency"] = frequency
        params["gifts"] = gifts
        params["total"] = total
        params["conn1"] = connection1Value
        params["conn2"] = connection2Value
        params["self_contri"] = selfContri
        params["conn1_contri"] = connection1Contri
        params["conn2_contri"] = connection2Contri
        params["months"] = ""
        if (isDefault) {
            params["self_contri"] = "33.33"
            params["conn1_contri"] = "33.33"
            params["conn2_contri"] = "33.33"
        } else {
            params["self_contri"] = selfContri
            params["conn1_contri"] = connection1Contri
            params["conn2_contri"] = connection2Contri
        }

        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.postRequestConnection2(params)
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                showToast(it.message)
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }

    }

    fun payNow() {

        val params = HashMap<String, String>()
        params["user_id"] = Prefs.getString(Constants.USER_ID, "")
        params["client_id"] = clientID
        params["frequency"] = frequency.substring(0,1)
        params["gifts"] = gifts
        params["total"] = total
        params["subType"] = subscriptionType
        params["months"] = ""

        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.postPayNow(params)
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                listener.postValue(Constants.BACK)
                            }else if (it.status_code == 400) {
                                purchase.postValue(it)
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }

    }

    fun subscribePayment() {

        val params = HashMap<String, String>()
        params["user_id"] = Prefs.getString(Constants.USER_ID, "")
        params["client_id"] = clientID

        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.postSubscribePayment(params)
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                listener.postValue(Constants.BACK)
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }

    }

}
