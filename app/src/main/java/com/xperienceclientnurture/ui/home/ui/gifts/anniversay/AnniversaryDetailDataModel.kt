package com.xperienceclientnurture.ui.home.ui.gifts.anniversay

import com.google.gson.annotations.SerializedName

data class AnniversaryDetailDataModel(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)

data class Data(
    @SerializedName("description")
    val description: String,
    @SerializedName("gift_id")
    val giftId: String,
    @SerializedName("gift_name")
    val gift_name: String,
    @SerializedName("gifts_available")
    val gifts_available: String,
    @SerializedName("height")
    val height: String,
    @SerializedName("images")
    val images: List<String>,
    @SerializedName("length")
    val length: String,
    @SerializedName("price")
    val price: String,
    @SerializedName("weight")
    val weight: String,
    @SerializedName("width")
    val width: String,
    @SerializedName("quantity")
    val quantity: String,
    @SerializedName("tax")
    val tax: String,
    @SerializedName("shipping")
    val shipping: String,
    @SerializedName("total")
    val total: String
)