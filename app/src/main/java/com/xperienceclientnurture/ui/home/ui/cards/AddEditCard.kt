package com.xperienceclientnurture.ui.home.ui.cards

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer

import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentAddEditCardBinding
import com.xperienceclientnurture.utils.Constants

class AddEditCard : BaseFragment<FragmentAddEditCardBinding, AddEditCardViewModel>(),
    AdapterView.OnItemSelectedListener {

    var months = arrayOf("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12")
    var years = arrayOf(
        "2019",
        "2020",
        "2021",
        "2022",
        "2023",
        "2024",
        "2025",
        "2026",
        "2027",
        "2028",
        "2029",
        "2030"
    )

    var monthPosition = 0
    var yearPosition = 0

    override val fragmentLayoutId: Int
        get() = R.layout.fragment_add_edit_card
    override val viewModelClass: Class<AddEditCardViewModel>
        get() = AddEditCardViewModel::class.java


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        viewModel.isEdit = bundle!!.getBoolean(Constants.ARGS_IS_EDIT, false)
        /*viewModel.cardNumber.value = bundle!!.getString(Constants.ARGS_CARD_NUMBER, "")
        viewModel.month = bundle!!.getString(Constants.ARGS_MONTH, "")
        viewModel.year = bundle!!.getString(Constants.ARGS_YEAR, "")*/

        setObservers()

        for (s in months.indices) {
            if (months[s] == viewModel.month) {
                monthPosition = s
                break
            }
        }

        for (s in years.indices) {
            if (years[s] == viewModel.year) {
                yearPosition = s
                break
            }
        }


        val arrayMonthAdapter =
            ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, months)
        arrayMonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerMonth.adapter = arrayMonthAdapter
        binding.spinnerMonth.onItemSelectedListener = this@AddEditCard
        binding.spinnerMonth.setSelection(monthPosition)

        val arrayYearAdapter =
            ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, years)
        arrayYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerYear.adapter = arrayYearAdapter
        binding.spinnerYear.onItemSelectedListener = this@AddEditCard
        binding.spinnerYear.setSelection(yearPosition)
    }

    private fun setObservers() {
        binding.viewmodel!!.progress.observe(this@AddEditCard, Observer {
            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }
        })

        viewModel.listener.observe(this@AddEditCard, Observer {

            if (it != null) {
                when (it) {

                    Constants.BACK -> {
                        activity!!.sendBroadcast(Intent(Constants.RECEIVER_CARDS))
                        activity!!.finish()
                    }
                }

                viewModel.listener.value = null
            }

        })
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent!!.id) {
            R.id.spinner_month -> {
                viewModel.month = months[position]
            }
            R.id.spinner_year -> {
                viewModel.year = years[position]
            }
        }
    }

}
