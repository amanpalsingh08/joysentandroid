package com.xperienceclientnurture.ui.home.ui.clientconnection

import android.util.Log
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.utils.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class ClientConnectionListViewModel : BaseViewModel() {

    val updateUI = SingleLiveData<List<Request>>()
    val progress = SingleLiveData<Boolean>()
    val noData = SingleLiveData<Boolean>()
    var feedbackID = ""

    fun getClientConnections(){

        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getClientConnections(Prefs.getString(Constants.USER_ID, ""))
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                val list = it.data.requests ?: arrayListOf()
                                if (list.isEmpty()) {
                                    noData.postValue(true)
                                } else {
                                    noData.postValue(false)
                                }
                                updateUI.postValue(list)
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }

    }

}
