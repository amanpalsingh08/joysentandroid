package com.xperienceclientnurture.ui.home.ui.epoxyModel

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.core.text.parseAsHtml
import com.airbnb.epoxy.CallbackProp
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.xperienceclientnurture.databinding.ListItemClientNotesBinding
import com.xperienceclientnurture.ui.home.ui.clientnotes.ClientNotes

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
class ListViewClientNotes @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private val binding = ListItemClientNotesBinding.inflate(LayoutInflater.from(context), this)

    @ModelProp
    fun setData(dataObj: ClientNotes) {
        if (dataObj.userID == dataObj.userid) {
            binding.ivEdit.visibility = View.VISIBLE
            binding.ivDelete.visibility = View.VISIBLE
        } else {
            binding.ivEdit.visibility = View.GONE
            binding.ivDelete.visibility = View.GONE
        }
        binding.tvClientName.text = getSpanData(buildString {
            append("Client Name: ")
            append(dataObj.clientFirstname)
            append(" ")
            append(dataObj.clientLastname)
        }, 0, 12)


        binding.tvNotesFrom.text = getSpanData(buildString {
            append("Notes From: ")
            append(dataObj.userFirstname)
            append(" ")
            append(dataObj.userLastname)
        }, 0, 11)

        binding.tvNotes.text = dataObj.notes?.parseAsHtml()?.trim()
    }

    @CallbackProp
    fun setClickListener(listener: OnClickListener?) {
        binding.ivEdit.setOnClickListener(listener)
    }
    @CallbackProp
    fun setViewListener(listener: OnClickListener?) {
        binding.parentView.setOnClickListener(listener)
    }

    @CallbackProp
    fun setDeleteListener(listener: OnClickListener?) {
        binding.ivDelete.setOnClickListener(listener)
    }

    private fun getSpanData(string: String, startIndex: Int, lastIndex: Int): SpannableString {
        val spannable = SpannableString(string)

        spannable.setSpan(StyleSpan(Typeface.BOLD), startIndex, lastIndex, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
        spannable.setSpan(ForegroundColorSpan(Color.BLACK), startIndex, lastIndex, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)

        return spannable
    }

}