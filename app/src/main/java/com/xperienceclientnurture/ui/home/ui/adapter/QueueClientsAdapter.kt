package com.xperienceclientnurture.ui.home.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.xperienceclientnurture.R
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.home.ui.queueclients.QueueClientsDetails
import com.xperienceclientnurture.utils.Constants

class QueueClientsAdapter(var list: List<QueueClientsDetails>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var listener: OnItemClickListener

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).name.text = list[position].client_name
        holder.email.text = list[position].email
        holder.phone.text = list[position].phone
        holder.address.text = list[position].address
        holder.city.text = list[position].city
        holder.state.text = list[position].state
        holder.shippingMonth.text = "N/A"

        holder.setItemSelectedListener(listener)

    }

    fun setItemSelectedListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_queue_client,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun updateList(it: List<QueueClientsDetails>) {
        list = it
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val parentView: View = itemView.findViewById(R.id.parent_view)
        val name: TextView = itemView.findViewById(R.id.tv_client_name)
        val email: TextView = itemView.findViewById(R.id.tv_client_email)
        val phone: TextView = itemView.findViewById(R.id.tv_client_phone)
        val address: TextView = itemView.findViewById(R.id.tv_address_value)
        val city: TextView = itemView.findViewById(R.id.tv_city_value)
        val state: TextView = itemView.findViewById(R.id.tv_state_value)
        val shippingMonth: TextView = itemView.findViewById(R.id.tv_shipping_month_value)
        lateinit var listener: OnItemClickListener

        init {
            phone.setOnClickListener(this)
            email.setOnClickListener(this)
        }

        fun setItemSelectedListener(listener: OnItemClickListener) {
            this.listener = listener
        }

        override fun onClick(p0: View?) {
            when (p0!!.id) {
                R.id.tv_client_email -> {
                    listener.onClick(adapterPosition, Constants.SEND_EMAIL)
                }
                R.id.tv_client_phone -> {
                    listener.onClick(adapterPosition, Constants.SEND_CALL)
                }
            }
        }

    }

}




