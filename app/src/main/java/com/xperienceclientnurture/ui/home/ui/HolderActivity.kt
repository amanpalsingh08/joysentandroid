package com.xperienceclientnurture.ui.home.ui

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.FragmentContainerView
import androidx.navigation.fragment.NavHostFragment
import com.airbnb.mvrx.Mavericks
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseActivity
import com.xperienceclientnurture.ui.home.ui.addeditclient.AddEditClient
import com.xperienceclientnurture.ui.home.ui.addeditclient.RequestConnection
import com.xperienceclientnurture.ui.home.ui.cards.AddEditCard
import com.xperienceclientnurture.ui.home.ui.clientnotes.ClientNotes
import com.xperienceclientnurture.ui.home.ui.feedback.AddEditFeedback
import com.xperienceclientnurture.ui.home.ui.gifts.GiftsFragment
import com.xperienceclientnurture.ui.home.ui.gifts.anniversay.AnniversaryFragment
import com.xperienceclientnurture.ui.home.ui.reminders.Reminders
import com.xperienceclientnurture.ui.home.ui.searchconnection.SearchConnectionFragment
import com.xperienceclientnurture.ui.home.ui.viewclient.ClientInfoFragment
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.Constants.Companion.ADD_EDIT_REMINDERS

class HolderActivity : BaseActivity(), View.OnClickListener {

    private var isAnniversary: Boolean = false
    lateinit var toolbar: Toolbar
    lateinit var ivBack: ImageView
    lateinit var ivEdit: ImageView
    lateinit var ivShare: ImageView
    lateinit var toolbarTitleHolder: TextView
    override val containerId: Int
        get() = R.id.nav_container

    var clientID = ""
    private var giftScreenName = ""

    private val hostContainerID: Int = R.id.nav_host_fragment_new
    lateinit var oldContainer: FrameLayout
    lateinit var newContainer: FragmentContainerView

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_holder)
        toolbar = findViewById(R.id.toolbar)
        ivBack = findViewById(R.id.iv_back)
        ivEdit = findViewById(R.id.iv_edit)
        ivShare = findViewById(R.id.iv_share)
        toolbarTitleHolder = findViewById(R.id.toolbar_title_holder)


        oldContainer = findViewById(R.id.nav_container)
        newContainer = findViewById(R.id.nav_host_fragment_new)

        ivEdit.setOnClickListener(this)
        ivShare.setOnClickListener(this)

        setSupportActionBar(toolbar)

        when (intent.getStringExtra(Constants.SCREEN_NAME)) {
            Constants.ADD_EDIT_CLIENT_NOTES, ADD_EDIT_REMINDERS -> {
                oldContainer.visibility = View.GONE
                newContainer.visibility = View.VISIBLE
            }

            else -> {
                newContainer.visibility = View.GONE
                oldContainer.visibility = View.VISIBLE
            }
        }

        when (intent.getStringExtra(Constants.SCREEN_NAME)) {

            Constants.SCREEN_ADD_CLIENT -> {
                if (intent.getBooleanExtra(Constants.ARGS_IS_EDIT, false)) {
                    toolbarTitleHolder.text = getString(R.string.title_edit_client)
                } else {
                    toolbarTitleHolder.text = getString(R.string.title_add_client)

                }
                replaceFragment(AddEditClient::class.java, null, true, intent.extras)
            }

            Constants.SCREEN_GIFTS -> {
                toolbarTitleHolder.text = getString(R.string.gifts)
                replaceFragment(GiftsFragment::class.java, null, true, intent.extras)
            }

            Constants.SCREEN_GIFTS_ANNIVERSARY -> {
                isAnniversary = true
                giftScreenName = intent.getStringExtra(Constants.GIFT_TYPE_NAME)!!
                toolbarTitleHolder.text = giftScreenName
                replaceFragment(AnniversaryFragment::class.java, null, true, intent.extras)
            }

            Constants.SCREEN_ADD_EDIT_FEEDBACK -> {
                if (intent.getBooleanExtra(Constants.ARGS_IS_EDIT, false)) {
                    toolbarTitleHolder.text = getString(R.string.title_edit_feedback)
                } else {
                    toolbarTitleHolder.text = getString(R.string.title_add_feedback)

                }
                replaceFragment(AddEditFeedback::class.java, null, true, intent.extras)
            }

            Constants.SCREEN_ADD_CARD -> {
                if (intent.getBooleanExtra(Constants.ARGS_IS_EDIT, false)) {
                    toolbarTitleHolder.text = getString(R.string.edit_card)
                } else {
                    toolbarTitleHolder.text = getString(R.string.add_card)

                }
                replaceFragment(AddEditCard::class.java, null, true, intent.extras)
            }

            Constants.SCREEN_REQUEST_CONNECTION -> {
                toolbarTitleHolder.text = getString(R.string.request_connection)

                replaceFragment(RequestConnection::class.java, null, true, intent.extras)
            }

            Constants.SEARCH -> {
                toolbarTitleHolder.text = getString(R.string.search)

                replaceFragment(SearchConnectionFragment::class.java, null, true, intent.extras)
            }

            Constants.VIEW_CLIENT -> {
                ivEdit.visibility = View.VISIBLE
                toolbarTitleHolder.text = getString(R.string.client_information)
                clientID = intent.extras!!.getString(Constants.ARGS_CLIENT_ID, "")
                replaceFragment(ClientInfoFragment::class.java, null, true, intent.extras)
            }

            Constants.ADD_EDIT_CLIENT_NOTES -> {

                val bundle = intent.extras
                bundle?.let {
                    val obj = it.getParcelable(Mavericks.KEY_ARG, ClientNotes::class.java)
                    if (obj?.id == null)
                        toolbarTitleHolder.text = getString(R.string.title_add_notes)
                    else
                        toolbarTitleHolder.text = getString(R.string.title_edit_notes)
                }
                configureNav(R.id.nav_add_edit_clients, bundle)
            }

            ADD_EDIT_REMINDERS -> {

                val bundle = intent.extras
                bundle?.let {
                    val obj = it.getParcelable(Mavericks.KEY_ARG, Reminders::class.java)
                    if (obj?.id == null)
                        toolbarTitleHolder.text = getString(R.string.title_add_reminders)
                    else
                        toolbarTitleHolder.text = getString(R.string.title_edit_reminders)
                }
                configureNav(R.id.nav_add_edit_reminder, bundle)
            }

        }

        ivBack.setOnClickListener(this)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.iv_back -> {
                if (isAnniversary) {
                    ivShare.visibility = View.GONE
                    isAnniversary = false
                    toolbarTitleHolder.text = giftScreenName
                }
                onBackPressed()
            }

            R.id.iv_edit -> {
                toolbarTitleHolder.text = getString(R.string.title_edit_client)
                ivEdit.visibility = View.INVISIBLE
                val bundle = Bundle()
                bundle.putString(Constants.SCREEN_NAME, Constants.SCREEN_ADD_CLIENT)
                bundle.putBoolean(Constants.ARGS_IS_EDIT, true)
                bundle.putString(Constants.ARGS_CLIENT_ID, clientID)
                replaceFragment(AddEditClient::class.java, null, false, bundle)
            }

            R.id.iv_share -> {
                val sendIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(
                        Intent.EXTRA_TEXT,
                        "https://play.google.com/store/apps/details?id=com.xperienceclientnurture&hl=en"
                    )
                    type = "text/plain"
                }
                val shareIntent = Intent.createChooser(sendIntent, null)
                startActivity(shareIntent)
            }
        }
    }

    private fun configureNav(destinationID: Int, extras: Bundle?) {
        val navHostFragment = supportFragmentManager.findFragmentById(hostContainerID) as NavHostFragment
        val navController = navHostFragment.navController
        val inflater = navController.navInflater
        val graph = inflater.inflate(R.navigation.main_navigation)
        graph.startDestination = destinationID
        navController.setGraph(graph, extras)
    }

}
