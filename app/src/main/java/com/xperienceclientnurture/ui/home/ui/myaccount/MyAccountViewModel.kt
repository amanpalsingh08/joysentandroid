package com.xperienceclientnurture.ui.home.ui.myaccount

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.ui.commonmodel.Data
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.Validators
import kotlinx.coroutines.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.HttpException
import java.io.File

class MyAccountViewModel : BaseViewModel() {

    val progress = SingleLiveData<Boolean>()
    val listener = SingleLiveData<String>()
    val updateSpinner = SingleLiveData<List<Data>>()
    val updateUI = SingleLiveData<UserData>()
    val firstName = MutableLiveData<String>("")
    val lastName = MutableLiveData<String>("")
    val email = MutableLiveData<String>("")
    val phone = MutableLiveData<String>("")
    val address = MutableLiveData<String>("")
    val zipCode = MutableLiveData<String>("")
    val city = MutableLiveData<String>("")
    var state = ""
    var statePos = 0
    var userType = ""
    private var accountDetails: HashMap<String, RequestBody> = HashMap()
    var imageFile: File? = null

    fun updateAccount() {
        val fn = firstName.value!!.trim()
        val ln = lastName.value!!.trim()
        val em = email.value!!.trim()
        val ph = phone.value!!.trim()
        val add = address.value!!.trim()
        val zc = zipCode.value!!.trim()
        val ct = city.value!!.trim()
        val st = state.trim()
        val ut = userType.trim()

        if (!Validators().validateMyAccountData(fn, ln, em, ph, add, zc, ct, st, ut)) {
            showToast(Validators.errorMessage.toString())
        } else {
            accountDetails["user_id"] = Prefs.getString(Constants.USER_ID, "").toRequestBody(
                "text/plain".toMediaType()
            )
                /*RequestBody.create(
                    MediaType.parse("text/plain"),
                    Prefs.getString(Constants.USER_ID, "")
                )*/
            accountDetails["firstname"] = fn.toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(MediaType.parse("text/plain"), fn)
            accountDetails["lastname"] = ln.toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(MediaType.parse("text/plain"), ln)
            accountDetails["email"] = em.toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(MediaType.parse("text/plain"), em)
            accountDetails["role"] = ut.toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(MediaType.parse("text/plain"), ut)
            accountDetails["phone"] = ph.toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(MediaType.parse("text/plain"), ph)
            accountDetails["address"] = add.toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(MediaType.parse("text/plain"), add)
            accountDetails["city"] = ct.toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(MediaType.parse("text/plain"), ct)
            accountDetails["state"] = st.toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(MediaType.parse("text/plain"), st)
            accountDetails["zipcode"] = zc.toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(MediaType.parse("text/plain"), zc)


            progress.postValue(true)
            val service = RetrofitFactory.makeRetrofitService()
            CoroutineScope(Dispatchers.IO).launch {

                var response: Any
                if (imageFile != null)
                    response = service.postUpdateUser(
                        accountDetails,
                        getProfileImage()
                    )
                else
                    response = service.postUpdateUserNoImage(accountDetails)
                try {

                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {


                            progress.postValue(false)
//                            navigateToHome()
                            response.body()?.let {


                            }
                        } else {
                            progress.postValue(false)
                            showToast("Error network operation failed with ${response.code()}")
                        }
                    }
                } catch (e: HttpException) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Exception ${e.message}")
                } catch (e: Throwable) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Ooops: Something else went wrong")
                }
            }

        }
    }
    fun updateUserProfile() {



            accountDetails["HomeTown"] = "Mohali".toRequestBody(
                "text/plain".toMediaType()
            )
                /*RequestBody.create(
                    MediaType.parse("text/plain"),"Mohali"
                )*/
            accountDetails["Education"] = "Btech".toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(MediaType.parse("text/plain"), "Btech")
            accountDetails["UserWork"] = "IT Prof".toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(MediaType.parse("text/plain"), "IT Prof")
            accountDetails["RelationShipStatus"] = "Single".toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(MediaType.parse("text/plain"), "Single")
            accountDetails["Hobbies"] = "Singing".toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(MediaType.parse("text/plain"), "Singing")
            accountDetails["UserId"] = "1".toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(MediaType.parse("text/plain"), "1")
            accountDetails["FirstName"] = "Amanpal".toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(MediaType.parse("text/plain"), "Amanpal")
            accountDetails["LastName"] = "Singh".toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(MediaType.parse("text/plain"), "Singh")
            accountDetails["DateOfBirth"] = "21-07-1992".toRequestBody(
                "text/plain".toMediaType()
            )
//                RequestBody.create(MediaType.parse("text/plain"), "21-07-1992")


            progress.postValue(true)
            val service = RetrofitFactory.makeRetrofitService()
            CoroutineScope(Dispatchers.IO).launch {

                var response =service.UpdateUserProfile(
                        accountDetails,
                        getProfileImage())
                try {

                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {


                            progress.postValue(false)
//                            navigateToHome()
                            response.body()?.let {


                            }
                        } else {
                            progress.postValue(false)
                            showToast("Error network operation failed with ${response.code()}")
                        }
                    }
                } catch (e: HttpException) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Exception ${e.message}")
                } catch (e: Throwable) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Ooops: Something else went wrong")
                }
            }

    }


    private fun getProfileImage(): MultipartBody.Part {

        val requestBody = imageFile!!.asRequestBody()
//            RequestBody.create(MediaType.parse("multipart/form-data"), imageFile!!)
        return MultipartBody.Part.createFormData("image", imageFile!!.name, requestBody)

    }


    fun getUserDetails() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getUser(Prefs.getString(Constants.USER_ID, ""))
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                updateUI.value = it.data
                                getStates()
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }


    fun getStates() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getState()
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {

                                for (i in it.data.indices) {
                                    if (it.data[i].code.contentEquals(state)) {
                                        statePos = i
                                        break
                                    }
                                }

                                updateSpinner.postValue(it.data)

                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }
}
