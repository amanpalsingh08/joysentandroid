package com.xperienceclientnurture.ui.home.ui.cards
import com.google.gson.annotations.SerializedName


data class CardsDataModels(
    @SerializedName("data")
    val `data`: CardsDetails,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)

data class CardsDetails(
    @SerializedName("card_num")
    val card_num: String,
    @SerializedName("created_at")
    val created_at: String,
    @SerializedName("customer_id")
    val customer_id: String,
    @SerializedName("exp_month")
    val exp_month: String,
    @SerializedName("exp_year")
    val exp_year: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("updated_at")
    val updated_at: String,
    @SerializedName("userid")
    val userid: String
)