package com.xperienceclientnurture.ui.home.ui.addeditclient


import com.google.gson.annotations.SerializedName

data class PaymentDetail(
    @SerializedName("price_per_gift")
    val pricePerGift: String,
    @SerializedName("tax")
    val tax: String,
    @SerializedName("total")
    val total: Double
)