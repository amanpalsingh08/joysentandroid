package com.xperienceclientnurture.ui.home.ui.home

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentHomeBinding
import com.xperienceclientnurture.interfaces.OnItemClickListener
import com.xperienceclientnurture.ui.home.ui.adapter.DeliveredClientsAdapter
import com.xperienceclientnurture.ui.home.ui.adapter.QueueClientsAdapter
import com.xperienceclientnurture.ui.home.ui.delivered.DeliveredClientDetails
import com.xperienceclientnurture.ui.home.ui.queueclients.QueueClientsDetails

class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(), OnItemClickListener {


    override val fragmentLayoutId: Int
        get() = R.layout.fragment_home
    override val viewModelClass: Class<HomeViewModel>
        get() = HomeViewModel::class.java

    private lateinit var listDelivered: List<DeliveredClientDetails>
    private lateinit var adapterDelivered: DeliveredClientsAdapter

    private lateinit var listQueue: List<QueueClientsDetails>
    private lateinit var adapterQueue: QueueClientsAdapter


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.viewmodel = viewModel

        setObservers()

        listDelivered = arrayListOf()
        adapterDelivered = DeliveredClientsAdapter(listDelivered)

        binding.recyclerViewDelivered.adapter = adapterDelivered
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.orientation = RecyclerView.HORIZONTAL
        binding.recyclerViewDelivered.layoutManager = linearLayoutManager
        adapterDelivered.setItemSelectedListener(this)
        viewModel.getDeliveredClients()


        listQueue = arrayListOf()
        adapterQueue = QueueClientsAdapter(listQueue)

        binding.recyclerViewQueue.adapter = adapterQueue
        val linearLayoutManager1 = LinearLayoutManager(context)
        linearLayoutManager1.orientation = RecyclerView.HORIZONTAL
        binding.recyclerViewQueue.layoutManager = linearLayoutManager1
        adapterQueue.setItemSelectedListener(this)

        viewModel.getQueueClients()


    }

    private fun setObservers() {
        viewModel.updateUIDelivered.observe(viewLifecycleOwner) {
            if (it != null) {
                setDataDelivered(it)
                viewModel.updateUIDelivered.value = null
            }
        }

        viewModel.updateUIQueue.observe(viewLifecycleOwner) {
            if (it != null) {
                setDataQueue(it)
                viewModel.updateUIQueue.value = null
            }
        }

        viewModel.progressDelivered.observe(viewLifecycleOwner) {

            if (it) {
                binding.progressDelivered.root.visibility = View.VISIBLE
            } else {
                binding.progressDelivered.root.visibility = View.GONE
            }

        }

        viewModel.noDataDelivered.observe(viewLifecycleOwner) {

            if (it) {
                binding.noDataFoundDelivered.root.visibility = View.VISIBLE
            } else {
                binding.noDataFoundDelivered.root.visibility = View.GONE
            }

        }

        viewModel.noDataQueue.observe(viewLifecycleOwner) {

            if (it) {
                binding.noDataFoundQueue.root.visibility = View.VISIBLE
            } else {
                binding.noDataFoundQueue.root.visibility = View.GONE
            }

        }
    }

    private fun setDataDelivered(it: List<DeliveredClientDetails>) {
        listDelivered = if (it.size > 6)
            it.take(6)
        else
            it
        adapterDelivered.updateList(listDelivered)
    }

    private fun setDataQueue(it: List<QueueClientsDetails>) {
        listQueue = if (it.size > 6)
            it.take(6)
        else
            it
        adapterQueue.updateList(listQueue)
    }

    override fun onClick(position: Int, type: String) {

    }

}