package com.xperienceclientnurture.ui.home.ui.changepassword

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer

import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentChangePasswordBinding
import com.xperienceclientnurture.ui.home.HomeScreenActivity

class ChangePasswordFragment :
    BaseFragment<FragmentChangePasswordBinding, ChangePasswordViewModel>() {
    override val fragmentLayoutId: Int
        get() = R.layout.fragment_change_password
    override val viewModelClass: Class<ChangePasswordViewModel>
        get() = ChangePasswordViewModel::class.java


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        binding.viewmodel = viewModel

        (activity as HomeScreenActivity).toolbarTitle.text = getString(R.string.change_password)

        setObservers()

    }

    private fun setObservers() {
        viewModel.progress.observe(this@ChangePasswordFragment, Observer {

            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }

        })
    }

}
