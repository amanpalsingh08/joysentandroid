package com.xperienceclientnurture.ui.home.ui.addeditclient


import com.google.gson.annotations.SerializedName

data class PaymentDetailsResponseModel(
    @SerializedName("data")
    val paymentData: PaymentData,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val statusCode: Int
)