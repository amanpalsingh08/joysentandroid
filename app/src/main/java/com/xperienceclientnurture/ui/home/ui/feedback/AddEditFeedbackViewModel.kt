package com.xperienceclientnurture.ui.home.ui.feedback

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.ui.home.ui.addeditclient.ClientDetails
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.Validators
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class AddEditFeedbackViewModel : BaseViewModel() {

    val feedback = MutableLiveData<String>("")
    var clientID = ""
    var feedbackID = ""
    var isEdit = false
    val updateClients = SingleLiveData<List<ClientDetails>>()
    val progress = SingleLiveData<Boolean>()
    val listener = SingleLiveData<String>()
    val updateUI = SingleLiveData<ClientDetails>()

    fun submit() {

        val fb = feedback.value!!.trim()

        if (!Validators().validateFeedback(
                clientID, fb
            )
        ) {
            showToast(Validators.errorMessage.toString())
        } else {
            val params = HashMap<String, String>()

            //Need to be updated

            if (isEdit)
                params["feedback_id"] = feedbackID
            params["user_id"] = Prefs.getString(Constants.USER_ID, "")
            params["client_id"] = clientID
            params["feedback"] = fb



            progress.postValue(true)
            val service = RetrofitFactory.makeRetrofitService()
            CoroutineScope(Dispatchers.IO).launch {
                val response = service.postAddEditFeedback(params)
                try {

                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {
                            progress.postValue(false)
                            response.body()?.let {
                                listener.postValue(Constants.BACK)

                            }
                        } else {
                            progress.postValue(false)
                            showToast("Error network operation failed with ${response.code()}")
                        }
                    }
                } catch (e: HttpException) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Exception ${e.message}")
                } catch (e: Throwable) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Ooops: Something else went wrong")
                }
            }
        }
    }

    fun getUserClients() {
        progress.postValue(true)
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getClients(Prefs.getString(Constants.USER_ID,""))
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        progress.postValue(false)
                        response.body()?.let {
                            if (it.status_code == 200) {
                                updateClients.postValue(it.data)
                            }
                        }
                    } else {
                        progress.postValue(false)
                        showToast("Error network operation failed with ${response.code()}")
                    }
                }
            } catch (e: HttpException) {
                progress.postValue(false)
                Log.e("REQUEST", "Exception ${e.message}")
            } catch (e: Throwable) {
                progress.postValue(false)
                Log.e("REQUEST", "Ooops: Something else went wrong")
            }
        }
    }

}
