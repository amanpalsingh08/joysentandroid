package com.xperienceclientnurture.ui.home.ui.searchconnection

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer

import com.xperienceclientnurture.R
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.databinding.FragmentSearchConnectionBinding

class SearchConnectionFragment :
    BaseFragment<FragmentSearchConnectionBinding, SearchConnectionViewModel>() {


    override val fragmentLayoutId: Int
        get() = R.layout.fragment_search_connection
    override val viewModelClass: Class<SearchConnectionViewModel>
        get() = SearchConnectionViewModel::class.java


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewmodel = viewModel

        setObservers()
    }

    private fun setObservers() {
        binding.viewmodel!!.progress.observe(this@SearchConnectionFragment, Observer {
            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }
        })

    }


}
