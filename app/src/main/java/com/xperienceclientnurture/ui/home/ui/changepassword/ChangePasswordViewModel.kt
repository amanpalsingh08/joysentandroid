package com.xperienceclientnurture.ui.home.ui.changepassword

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.Validators
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class ChangePasswordViewModel : BaseViewModel() {
    val password = MutableLiveData<String>("")
    val confirmPassword = MutableLiveData<String>("")
    val progress = SingleLiveData<Boolean>()

    fun updatePassword() {
        val ps = password.value!!.trim()
        val cps = confirmPassword.value!!.trim()
        if (!Validators().validateChangePassword(ps, cps)) {
            showToast(Validators.errorMessage.toString())
        } else {

            val params = HashMap<String, String>()

            params["user_id"] = Prefs.getString(Constants.USER_ID, "")
            params["password"] = ps
            params["password_confirmation"] = cps


            progress.postValue(true)
            val service = RetrofitFactory.makeRetrofitService()
            CoroutineScope(Dispatchers.IO).launch {
                val response = service.postUpdatePassword(params)
                try {

                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {
                            progress.postValue(false)
                            response.body()?.let { showToast(it.message) }
                        } else {
                            progress.postValue(false)
                            showToast("Error network operation failed with ${response.code()}")
                        }
                    }
                } catch (e: HttpException) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Exception ${e.message}")
                } catch (e: Throwable) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Ooops: Something else went wrong")
                }
            }
        }

    }

}
