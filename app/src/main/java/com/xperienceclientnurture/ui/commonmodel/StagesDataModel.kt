package com.xperienceclientnurture.ui.commonmodel
import com.google.gson.annotations.SerializedName


data class StagesDataModel(
    @SerializedName("data")
    val `data`: List<StagesData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)

data class StagesData(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String
)