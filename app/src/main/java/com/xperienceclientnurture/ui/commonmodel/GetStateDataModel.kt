package com.xperienceclientnurture.ui.commonmodel
import com.google.gson.annotations.SerializedName


data class GetStateDataModel(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)

data class Data(
    @SerializedName("code")
    val code: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("state_name")
    val state_name: String
)