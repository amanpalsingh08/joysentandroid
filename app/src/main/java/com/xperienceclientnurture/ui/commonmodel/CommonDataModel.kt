package com.xperienceclientnurture.ui.commonmodel
import com.google.gson.annotations.SerializedName


data class CommonDataModel(
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)