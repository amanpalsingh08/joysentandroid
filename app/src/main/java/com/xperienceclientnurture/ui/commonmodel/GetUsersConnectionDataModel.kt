package com.xperienceclientnurture.ui.commonmodel
import com.google.gson.annotations.SerializedName


data class GetUsersConnectionDataModel(
    @SerializedName("data")
    val `data`: List<UsersConnectionDetails>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val status_code: Int
)

data class UsersConnectionDetails(
    @SerializedName("email")
    val email: String,
    @SerializedName("firstname")
    val firstname: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("lastname")
    val lastname: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("user_type")
    val user_type: String
)