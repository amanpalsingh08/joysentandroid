package com.xperienceclientnurture.ui.splash

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.xperienceclientnurture.base.BaseActivity
import com.xperienceclientnurture.sharedpreference.Prefs
import com.xperienceclientnurture.MainActivity
import com.xperienceclientnurture.R
import com.xperienceclientnurture.databinding.ActivitySplashBinding
import com.xperienceclientnurture.ui.home.HomeScreenActivity
import com.xperienceclientnurture.utils.Constants


class SplashActivity : BaseActivity(), View.OnClickListener {
    override
    fun onClick(view: View?) {
        when (view!!.id) {
            R.id.rl_login -> {
                startActivity(
                    Intent(this, MainActivity::class.java)
                        .putExtra(Constants.IS_LOGIN, true)
                )
            }
            R.id.rl_register -> {
                startActivity(
                    Intent(this, MainActivity::class.java)
                        .putExtra(Constants.IS_LOGIN, false)
                )
            }
        }
    }

    lateinit var binding: ActivitySplashBinding

    override val containerId: Int
        get() = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)



        if (Prefs.getBoolean(Constants.IS_SESSION, false)) {
            startActivity(
                Intent(this, HomeScreenActivity::class.java)
            )
            finish()
        } else {
            binding.rlLogin.setOnClickListener(this)
            binding.rlRegister.setOnClickListener(this)
        }

    }
}