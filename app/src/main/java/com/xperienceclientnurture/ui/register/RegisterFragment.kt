package com.xperienceclientnurture.ui.register

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import com.xperienceclientnurture.base.BaseFragment
import com.xperienceclientnurture.R
import com.xperienceclientnurture.databinding.RegisterFragmentBinding
import com.xperienceclientnurture.ui.login.LoginFragment
import com.xperienceclientnurture.utils.Constants

class RegisterFragment : BaseFragment<RegisterFragmentBinding, RegisterViewModel>(),
    AdapterView.OnItemSelectedListener {

    var types = arrayOf("lender", "agent", "team", "brokerage", "company", "vendor", "sales rep","customer")

    override val fragmentLayoutId: Int
        get() = R.layout.register_fragment
    override val viewModelClass: Class<RegisterViewModel>
        get() = RegisterViewModel::class.java


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewModel = viewModel

        binding.viewModel!!.progress.observe(this@RegisterFragment, Observer {
            if (it) {
                binding.progress.root.visibility = View.VISIBLE
            } else {
                binding.progress.root.visibility = View.GONE
            }
        })

        binding.viewModel!!.navigation.observe(this@RegisterFragment, Observer {

            if (it != null) {
                when (it) {
                    Constants.BACK -> {
                        activity!!.onBackPressed()
                    }
                    Constants.LOGIN -> {
                        replaceFragment(LoginFragment::class.java)
                    }
                }
                binding.viewModel!!.navigation.value=null
            }
        })

        val arrayAdapter = ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, types)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerType.adapter = arrayAdapter
        binding.spinnerType.onItemSelectedListener = this@RegisterFragment
        binding.spinnerType.setSelection(0)
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        print("Type:  " + types[p2])
        viewModel.role = types[p2]
    }


}
