package com.xperienceclientnurture.ui.register

import com.google.gson.annotations.SerializedName


data class RegisterDataModel(
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_code")
    val statusCode: Int
)