package com.xperienceclientnurture.ui.register

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.base.BaseViewModel
import com.xperienceclientnurture.livedata.SingleLiveData
import com.xperienceclientnurture.utils.Constants
import com.xperienceclientnurture.utils.Validators
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class RegisterViewModel : BaseViewModel() {

    val firstName = MutableLiveData<String>("")
    val lastName = MutableLiveData<String>("")
    val email = MutableLiveData<String>("")
    val password = MutableLiveData<String>("")
    val confirmPassword = MutableLiveData<String>("")
    var role = "";
    val progress = SingleLiveData<Boolean>()
    val navigation = SingleLiveData<String>()


    fun register() {
        val em = email.value!!.trim()
        val fn = firstName.value!!.trim()
        val ln = lastName.value!!.trim()
        val ps = password.value!!.trim()
        val cps = confirmPassword.value!!.trim()
        if (!Validators().validateSignUpData(fn, ln, em, ps, cps)) {
            showToast(Validators.errorMessage.toString())
        } else {

            val params = HashMap<String, String>()

            params["email"] = em
            params["firstname"] = fn
            params["lastname"] = ln
            params["password"] = ps
            params["role"] = role


            progress.postValue(true)
            val service = RetrofitFactory.makeRetrofitService()
            CoroutineScope(Dispatchers.IO).launch {
                val response = service.postRegister(params)
                try {

                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {
                            progress.postValue(false)
                            navigateToLogin()
                            response.body()?.let { showToast(it.message) }
                        } else {
                            progress.postValue(false)
                            showToast("Error network operation failed with ${response.code()}")
                        }
                    }
                } catch (e: HttpException) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Exception ${e.message}")
                } catch (e: Throwable) {
                    progress.postValue(false)
                    Log.e("REQUEST", "Ooops: Something else went wrong")
                }
            }
        }
    }

    fun backPress() {
        navigation.postValue(Constants.BACK)
    }

    fun navigateToLogin() {
        navigation.postValue(Constants.LOGIN)
    }

}
