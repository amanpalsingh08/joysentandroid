package com.xperienceclientnurture.base

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.AnimRes
import androidx.annotation.AnimatorRes
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.baseviewmodelproject.base.OnFragmentInteractionListener
import com.xperienceclientnurture.R
import pub.devrel.easypermissions.EasyPermissions


abstract class BaseActivity : AppCompatActivity(), OnFragmentInteractionListener {

    protected abstract val containerId: Int
    private lateinit var imm: InputMethodManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        log("OnCreate ${this::class.java.simpleName}")
        imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    /**
     * Override Back Press For BackStack Fragment Handling
     */
    override fun onBackPressed() {
        backPress()
    }

    private fun backPress() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            supportFragmentManager.popBackStack()
        } else {
            //super.onBackPressed();
            finish()
        }
    }

    /**
     * Use this function for simple one by one transaction with reverse
     *
     *
     * @addToBackStack : provide boolean to save the transaction in backstack
     *  on backPress the back stack pop these transaction in reverse
     *  send false if you do not want to add to backstack
     *  by default value is true so as to maintain back press
     *
     *  @extraTag if you want to open a new fragment(same fragment is already open) but with different values use it.
     *  Like in profile ifProfileA is open and you want ProfileB then add extraTag for it
     *
     *  @views list of views for shared Transition.Make sure these views should have transition Name
     *  Also the source view and destination view both should have same transition name either in xml or in java
     *  NOTE: if you are using listview or recyclerview make sure to provide transition name different to each item of that list or recyclerview
     *  if using list then set transition Name dynamically because of above note
     *  if using simple design and not list item or repeat items then use android:transitionName in xml on both source and destination side
     *
     *
     *  @newFragmentEnterAnimation
     *  @currentFragmentExitAnimation
     *  @currentFragmentEnterAnimation
     *  @newFragmentExitAnimation
     *  @replaceWithAnimation -> allow replace with animation,if set to true default animation will work
     *  use these above animation to animate fragment enter and exit .
     *  Default animation already set
     *  if no animation required set value 0 in all
     *
     *
     *  @revealAnimationSetting to perform replace Fragment with circular Reveal Animation
     *
     *
     */
    fun replaceFragment(
        fragmentClass: Class<*>,
        extraTag: String? = null,
        addToBackStack: Boolean = true,
        bundle: Bundle? = null,
        views: ArrayList<View>? = null, @AnimatorRes @AnimRes newFragmentEnterAnimation: Int = 0, @AnimatorRes @AnimRes currentFragmentExitAnimation: Int = 0, @AnimatorRes @AnimRes currentFragmentEnterAnimation: Int = 0, @AnimatorRes @AnimRes newFragmentExitAnimation: Int = 0,
        targetFragment: Fragment? = null,
        targetRequestCode: Int = 333,
        replaceWithNavigation: Boolean = false
    ) {
        if (containerId == 0) throw NullPointerException("containerId cannot be null")
        val finalTag = fragmentClass.simpleName + (extraTag ?: "")
        val isPopBackStack = supportFragmentManager.popBackStackImmediate(finalTag, 0)
        when {
            !isPopBackStack -> {
                if (supportFragmentManager.findFragmentByTag(finalTag) == null) {
                    log("Fragment Not in Fragment Manager $finalTag")
                }
                val fragment = supportFragmentManager.findFragmentByTag(finalTag)
                    ?: fragmentClass.newInstance()
                performTaskToFragment(
                    fragment,
                    bundle,
                    targetFragment,
                    targetRequestCode,
                    views,
                    newFragmentEnterAnimation,
                    newFragmentExitAnimation,
                    currentFragmentEnterAnimation,
                    currentFragmentExitAnimation,
                    finalTag,
                    addToBackStack,
                    replaceWithNavigation
                )
            }
        }
    }

    private fun performTaskToFragment(
        fragment: Any,
        bundle: Bundle?,
        targetFragment: Fragment?,
        targetRequestCode: Int,
        views: ArrayList<View>?,
        newFragmentEnterAnimation: Int,
        newFragmentExitAnimation: Int,
        currentFragmentEnterAnimation: Int,
        currentFragmentExitAnimation: Int,
        finalTag: String,
        addToBackStack: Boolean,
        replaceWithNavigation: Boolean
    ) {
        when (fragment) {
            is BaseFragment<*, *> -> {
                fragment.replaceWithNavigation = replaceWithNavigation
                if (bundle != null) fragment.bundle = bundle

                if (targetFragment != null) {
                    fragment.setTargetFragment(targetFragment, targetRequestCode)
                }

                val transaction = supportFragmentManager.beginTransaction()

                transaction.setCustomAnimations(
                    newFragmentEnterAnimation,
                    currentFragmentExitAnimation,
                    currentFragmentEnterAnimation,
                    newFragmentExitAnimation
                )
                /*   if (currentFragment != null)
                       transaction.remove(currentFragment)
                   transaction.add(containerId, fragment, finalTag)*/
                transaction.replace(containerId, fragment as Fragment, finalTag)
                if (addToBackStack) transaction.addToBackStack(finalTag)
                transaction.commit()
                supportFragmentManager.executePendingTransactions()
            }

            else -> showToast(getString(R.string.not_base_fragment_instance))
        }
    }

    /**
     * Add Back Button To Toolbar with click @onBackPressed
     *
     * @icon : Provide Color Id in Resource
     */
    fun setToolbarWithBackButton(@DrawableRes icon: Int? = null) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        if (icon != null) supportActionBar?.setHomeAsUpIndicator(icon)
    }

    /**
     * Use it to show Keyboard
     */
    fun showKeyboard() {
        try {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Use it to hide Keyboard
     * @editText provide if hide for particular editText
     */
    fun hideKeyboard(editText: EditText? = null) {
        if (editText != null && editText.windowToken != null)
            imm.hideSoftInputFromWindow(editText.windowToken, 0)
        if (currentFocus != null && currentFocus!!.windowToken != null)
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }

    /**
     * Log for printing(will be shown)
     */
    fun log(message: String?) {
        when {
            message != null -> Log.d("Log Print:", message)
        }
    }

    private var toast: Toast? = null

    /**
     * showToast with message
     *  @view: for custom toast provide view perform inflater and other task at your end
     *  @gravity: provide gravity
     */
    fun showToast(
        message: String?,
        view: View? = null,
        duration: Int = Toast.LENGTH_LONG,
        gravity: Int = Gravity.BOTTOM or Gravity.CENTER_VERTICAL, @DimenRes x: Int = 0, @DimenRes y: Int = 0,
        cancelPrevious: Boolean = false
    ) {
        if (cancelPrevious) toast?.cancel()

        // set custom view
        when {
            view != null -> {
                toast = Toast(applicationContext)
                toast?.view = view
            }

            else -> toast = Toast.makeText(applicationContext, message, duration)
        }
        // set duration
        toast?.duration = duration
        // set position
        var marginY = 0
        var marginX = 0
        if (y != 0) marginY = resources.getDimensionPixelSize(y)
        if (x != 0) marginX = resources.getDimensionPixelSize(x)
        if (marginX > 0 || marginY > 0) toast?.setGravity(gravity, marginX, marginY)
        // show toast
        toast?.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * remove all the fragment transactions from the backstack
     */
    fun clearBackStack() {
        val fragmentManager = supportFragmentManager
        //this will clear the back stack and displays no animation on the screen
        // fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        val backStackCount = fragmentManager.backStackEntryCount
        (0 until backStackCount)
            .map {
                // Get the back stack fragment id.
                fragmentManager.getBackStackEntryAt(it).id
            }
            .forEach { fragmentManager.popBackStack(it, FragmentManager.POP_BACK_STACK_INCLUSIVE) }
    }

    fun sendEmail(recipient: String, subject: String, message: String) {
        /*ACTION_SEND action to launch an email client installed on your Android device.*/
        val mIntent = Intent(Intent.ACTION_SEND)
        /*To send an email you need to specify mailto: as URI using setData() method
        and data type will be to text/plain using setType() method*/
        mIntent.data = Uri.parse("mailto:")
        mIntent.type = "text/plain"
        // put recipient email in intent
        /* recipient is put as array because you may wanna send email to multiple emails
           so enter comma(,) separated emails, it will be stored in array*/
        mIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(recipient))
        //put the Subject in the intent
        mIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        //put the message in the intent
        mIntent.putExtra(Intent.EXTRA_TEXT, message)


        try {
            //start email intent
            startActivity(Intent.createChooser(mIntent, "Choose Email Client..."))
        } catch (e: Exception) {
            //if any thing goes wrong for example no email client application or any exception
            //get and show exception message
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
        }

    }

    fun call(mobile: String) /*= runWithPermissions(Manifest.permission.CALL_PHONE)*/ {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$mobile"))
        startActivity(intent)
    }

    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                }

                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                }

                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }
}