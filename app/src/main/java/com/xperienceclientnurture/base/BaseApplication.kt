package com.xperienceclientnurture.base

import android.app.Application
import android.content.Context
import android.content.ContextWrapper
import android.net.ConnectivityManager
import android.os.StrictMode
import com.airbnb.mvrx.Mavericks
import com.xperienceclientnurture.sharedpreference.Prefs

/**
 * Created by Shivam Verma on 03/10/17.
 * Author: Shivam Verma
 * Project: ExpensesApp
 */
open class BaseApplication : Application() {

    var instance: BaseApplication? = null

    override fun onCreate() {
        super.onCreate()

        //For Picker to avoid FIleUri Exposed Exception
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        /**
         *  SharedPreferences Initialization
         */

        Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(packageName)
                .setUseDefaultSharedPreference(true)
                .build()

        Mavericks.initialize(this)
    }
    open fun hasNetwork(): Boolean {
        return instance!!.checkIfHasNetwork()
    }

    open fun checkIfHasNetwork(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }
}