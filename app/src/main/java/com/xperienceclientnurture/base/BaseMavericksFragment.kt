package com.xperienceclientnurture.base

import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.getSystemService
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.xperienceclientnurture.utils.SafeClickListener

abstract class BaseMavericksFragment : Fragment() {

    fun View.hideSoftInput() = context.getSystemService<InputMethodManager>()?.hideSoftInputFromWindow(this.windowToken, 0)

    fun showSnack(view: View, message: String) = Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show()

    fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit){
        val safeClickListener = SafeClickListener{
            onSafeClick(it)
        }
        setOnClickListener(safeClickListener)
    }
    companion object{
        var onUpdatedNotes: ((Boolean) -> Unit)? = null
        var onUpdatedReminders: ((Boolean) -> Unit)? = null
    }

}