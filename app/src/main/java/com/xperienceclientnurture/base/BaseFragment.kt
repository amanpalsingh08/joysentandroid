package com.xperienceclientnurture.base


import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ScrollView
import android.widget.Toast
import androidx.annotation.AnimRes
import androidx.annotation.AnimatorRes
import androidx.annotation.DimenRes
import androidx.annotation.StringRes
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.example.baseviewmodelproject.base.OnFragmentInteractionListener
import pub.devrel.easypermissions.EasyPermissions

abstract class BaseFragment<DB : ViewDataBinding, VM : BaseViewModel> : Fragment() {

    protected abstract val fragmentLayoutId: Int
    protected abstract val viewModelClass: Class<VM>
    protected open var factory: ViewModelProvider.NewInstanceFactory? = null

    var replaceWithNavigation: Boolean = false

    lateinit var binding: DB
    protected lateinit var viewModel: VM

    /**
     * To send Data From Fragment to its parent Activity
     */
    var mListener: OnFragmentInteractionListener? = null

    /**
     * Additional bundle to avoid setArgument(only once) issue in fragment
     */
    var bundle: Bundle? = null

    /**
     * Check If the device has permission
     */
    fun hasPermission(vararg permissions: String): Boolean {
        return EasyPermissions.hasPermissions(requireContext(), *permissions)
    }

    /**
     * Implement interface #EasyPermissions.PermissionCallbacks
     * to override the fun onPermissionsGranted and onPermissionsDenied
     * OR
     *
     * Use Annotation
     *     @AfterPermissionGranted(permissionRequestCode)
     */
    fun requestPermission(
        permissionRequestCode: Int,
        @StringRes message: Int = 0,
        vararg permissions: String
    ) {
        EasyPermissions.requestPermissions(
            this,
            when (message) {
                0 -> "Permission Required"
                else -> getString(message)
            },
            permissionRequestCode,
            *permissions
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    /**
     * Call only one time when attach to activity
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        log("Create ${this::class.java.simpleName}")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        log("OnCreateView ${this::class.java.simpleName}")
        binding = DataBindingUtil.inflate(inflater, fragmentLayoutId, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.setLifecycleOwner(this)
        log("View Created ${this::class.java.simpleName}")

        viewModel = when (factory) {
            null -> when (replaceWithNavigation && activity != null) {
                true -> ViewModelProviders.of(requireActivity()).get(viewModelClass)
                else -> ViewModelProviders.of(this).get(viewModelClass)
            }

            else -> when (replaceWithNavigation && activity != null) {
                true -> ViewModelProviders.of(requireActivity(), factory!!).get(viewModelClass)
                else -> ViewModelProviders.of(this, factory!!).get(viewModelClass)
            }

        }


        viewModel.toast.observe(viewLifecycleOwner, Observer<Int> {
            if (it != null && it != 0) {
                showToast(getString(it))
            }
        })
        viewModel.toastMsg.observe(viewLifecycleOwner, Observer<String> {
            if (it != null) {
                showToast(it)
            }
        })
        viewModel.keyboard.observe(viewLifecycleOwner, Observer<Boolean> {
            if (it != null) {
                if (it) showKeyboard() else hideKeyboard()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        log("Resume ${this::class.java.simpleName}")
        restoreState()
    }

    override fun onPause() {
        super.onPause()
        log("Pause ${this::class.java.simpleName}")
        saveState()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        when (context) {
            is OnFragmentInteractionListener -> mListener = context
        }
        log("Attach ${this::class.java.simpleName}")
    }

    override fun onDetach() {
        super.onDetach()
        log("Detach ${this::class.java.simpleName}")
        mListener = null
    }

    /**
     *  handing the scroll state of RecyclerView
     */
    private fun saveState() {
        val parent = view as? ViewGroup
        if (parent != null) {
            for (i in 0..parent.childCount) {
                val vw = parent.getChildAt(i)
                getStateOfView(vw)
            }
            getStateOfView(parent)
        }
    }

    private fun getStateOfView(vw: View?) {
        if (vw is RecyclerView) {
            val mListState = vw.layoutManager!!.onSaveInstanceState()
            viewModel.recyclerViewScrollState = mListState
        } else if (vw is ScrollView || vw is NestedScrollView) {
            viewModel.scrollPosition = vw.scrollY
        }
    }

    /**
     *  handing the scroll state of RecyclerView
     */
    private fun restoreState() {
        val parent = view as? ViewGroup
        if (parent != null)
            for (i in 0..parent.childCount) {
                val vw = parent.getChildAt(i)
                setStateOfView(vw)
            }
        setStateOfView(parent)
    }

    private fun setStateOfView(vw: View?) {
        (vw as? RecyclerView)?.layoutManager?.onRestoreInstanceState(viewModel.recyclerViewScrollState)
        (vw as? ScrollView)?.post { (vw as? ScrollView)?.scrollTo(0, viewModel.scrollPosition) }
        (vw as? NestedScrollView)?.post {
            (vw as? NestedScrollView)?.scrollTo(
                0,
                viewModel.scrollPosition
            )
        }
    }

    protected fun log(message: String?) {
        (activity as? BaseActivity)?.log(message)
    }

    protected fun replaceFragment(
        fragmentClass: Class<*>,
        extraTag: String? = null,
        addToBackStack: Boolean = true,
        bundle: Bundle? = null,
        views: ArrayList<View>? = null,
        @AnimatorRes @AnimRes newFragmentEnterAnimation: Int = 0,
        @AnimatorRes @AnimRes currentFragmentExitAnimation: Int = 0,
        @AnimatorRes @AnimRes currentFragmentEnterAnimation: Int = 0,
        @AnimatorRes @AnimRes newFragmentExitAnimation: Int = 0,
        targetFragment: Fragment? = null,
        targetRequestCode: Int = 333
    ) {
        (activity as? BaseActivity)?.replaceFragment(
            fragmentClass,
            extraTag,
            addToBackStack,
            bundle,
            views,
            newFragmentEnterAnimation,
            currentFragmentExitAnimation,
            currentFragmentEnterAnimation,
            newFragmentExitAnimation,
            targetFragment,
            targetRequestCode
        )
    }

    protected fun showToast(
        message: String?,
        view: View? = null,
        duration: Int = Toast.LENGTH_LONG,
        gravity: Int = Gravity.BOTTOM or Gravity.CENTER_VERTICAL,
        @DimenRes x: Int = 0,
        @DimenRes y: Int = 0,
        cancelPrevious: Boolean = false
    ) {
        (activity as? BaseActivity)?.showToast(
            message,
            view,
            duration,
            gravity,
            x,
            y,
            cancelPrevious
        )
    }

    protected fun showKeyboard() {
        (activity as? BaseActivity)?.showKeyboard()
    }

    protected fun hideKeyboard(editText: EditText? = null) {
        (activity as? BaseActivity)?.hideKeyboard(editText)
    }

    protected fun clearBackStack() {
        (activity as? BaseActivity)?.clearBackStack()
    }

    fun isOnline(): Boolean {
        return (requireActivity() as BaseActivity).isOnline(requireActivity())
    }
}