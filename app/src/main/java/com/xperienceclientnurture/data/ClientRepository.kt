package com.xperienceclientnurture.data

import android.content.Context
import com.xperienceclientnurture.networks.RetrofitApi
import com.xperienceclientnurture.networks.RetrofitFactory
import com.xperienceclientnurture.networks.RetrofitService

class ClientRepository(private val retrofitApi: RetrofitApi = RetrofitApi(), val context: Context? = null) {


    private val service: RetrofitService by lazy {
        retrofitApi.createService(RetrofitService::class.java)
    }

    suspend fun clientNotes(userID: String) = service.getClientNotes(userID)

    suspend fun deleteClientNote(userID: String, notesID: String) = service.deleteClientNote(userID, notesID)

    suspend fun userClients(userID: String) = RetrofitFactory.makeRetrofitService().getClients(userID)

    suspend fun addNoteData(params: HashMap<String, String>) = service.addNoteData(params)

    suspend fun updateNote(params: HashMap<String, String>) = service.updateNote(params)

    suspend fun reminders(userID: String) = service.getReminders(userID)

    suspend fun deleteReminder(userID: String, reminderID: String) = service.deleteReminder(userID, reminderID)

    suspend fun addReminderData(params: HashMap<String, String>) = service.addReminderData(params)


    suspend fun updateReminders(params: HashMap<String, String>) = service.updateReminders(params)

    suspend fun homeBuyerSubscriptions(userID: String) = service.homeBuyerSubscriptions(userID)
    suspend fun clientNurtureSubscriptions(userID: String) = service.clientNurtureSubscriptions(userID)

}

