package com.example.baseviewmodelproject.databinding.video

import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import java.io.File


object VideoBindingAdapter {


    @JvmStatic
    @BindingAdapter("videoPath", "error")
    fun setVideoThumbnailUrl(imageView: ImageView?, url: String?, drawable: Drawable) {
        if (imageView != null) {
            val context = imageView.context
            if (url == null)
                imageView.setImageDrawable(drawable)
            else {
//                val bitmapPool = GlideApp.get(context).bitmapPool
//                val microSecond = 2000000// 2th second as an example
//                val videoBitmapDecoder = VideoBitmapDecoder(microSecond)
//                val fileDescriptorBitmapDecoder = FileDescriptorBitmapDecoder(videoBitmapDecoder, bitmapPool, DecodeFormat.PREFER_ARGB_8888)
                /*GlideApp.with(context)
                        .asBitmap()
                        .load(Uri.fromFile(File(url)))
                        .into(imageView)*/

            }
        }
    }
}