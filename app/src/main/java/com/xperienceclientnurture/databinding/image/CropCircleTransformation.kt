package com.example.baseviewmodelproject.databinding.image


import android.graphics.Bitmap
import com.bumptech.glide.load.Key
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation
import com.bumptech.glide.load.resource.bitmap.TransformationUtils
import java.security.MessageDigest


class CropCircleTransformation : BitmapTransformation() {

    override fun transform(pool: BitmapPool, toTransform: Bitmap, outWidth: Int, outHeight: Int): Bitmap {
        return TransformationUtils.circleCrop(pool, toTransform, outWidth, outHeight)
    }


    override fun toString(): String {
        return "CropCircleTransformation()"
    }

    override fun equals(o: Any?): Boolean {
        return o is CropCircleTransformation
    }

    override fun hashCode(): Int {
        return ID.hashCode()
    }

    override fun updateDiskCacheKey(messageDigest: MessageDigest) {
        messageDigest.update(ID_BYTES)
    }

    companion object {

        private val VERSION = 1
        private val ID = "jp.wasabeef.glide.transformations.CropCircleTransformation.$VERSION"
        private val ID_BYTES = ID.toByteArray(Key.CHARSET)
    }
}