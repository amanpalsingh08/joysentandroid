package com.xperienceclientnurture.databinding.image


import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton


object ImageBindingAdapter {

    @JvmStatic
    @BindingAdapter("imageUrl", "error")
    fun setImageUrl(imageView: ImageView?, url: String?, drawable: Drawable) {
        /*if (imageView != null) {
            val context = imageView.context
            if (url == null)
                imageView.setImageDrawable(drawable)
            else {
                val file = File(url)
                if (file.exists())
                    GlideApp.with(context)
                            .load(url)
                            .signature(ObjectKey("${file.length()}@${file.lastModified()}"))
                            .placeholder(drawable)
                            .dontAnimate().into(imageView)
                else
                    GlideApp.with(context)
                            .load(Uri.parse(url))
                            .signature(ObjectKey("${file.length()}@${file.lastModified()}"))
                            .placeholder(drawable)
                            .listener(object : RequestListener<Drawable> {
                                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                    Log.d("IMAGE", "Error $url")
                                    e?.printStackTrace()
                                    return false
                                }

                                override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                    Log.d("IMAGE", "Success $url")
                                    return false
                                }

                            })
                            .dontAnimate().into(imageView)
            }
        }*/
    }

    @JvmStatic
    @BindingAdapter("imageCircleUrl", "error")
    fun setCircleImageUrl(imageView: ImageView?, url: String?, drawable: Drawable) {
        if (imageView != null) {
            val context = imageView.context
            if (url == null)
                imageView.setImageDrawable(drawable)
            /*else
                GlideApp.with(context).load(url).placeholder(drawable).transform(
                    CropCircleTransformation()
                ).dontAnimate().into(imageView)*/
        }
    }

    @JvmStatic
    @BindingAdapter("fabSrc")
    fun setImageRes(fab: FloatingActionButton?, @DrawableRes drawable: Int) {
        if (fab != null && drawable != 0)
            fab.setImageResource(drawable)
    }

    @JvmStatic
    @BindingAdapter("imageBitmap", "error")
    fun setImageBitmap(imageView: ImageView?, bitmap: Bitmap?, drawable: Drawable) {
        if (imageView != null) {
            val context = imageView.context
            if (bitmap == null)
                imageView.setImageDrawable(drawable)
           /* else
                GlideApp.with(context).load(bitmap).placeholder(drawable).into(imageView)*/
        }
    }

    @JvmStatic
    @BindingAdapter("imageSvg", "error")
    fun setImageSvg(imageView: ImageView?, bitmap: Bitmap?, drawable: Drawable) {
        if (imageView != null) {
            val context = imageView.context
            if (bitmap == null)
                imageView.setImageDrawable(drawable)
           /* else
                GlideApp.with(context)*/


        }
    }

}