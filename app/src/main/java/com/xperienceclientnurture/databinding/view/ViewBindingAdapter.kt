package com.xperienceclientnurture.databinding.view

import android.R
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.databinding.BindingAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputLayout



object ViewBindingAdapter {


    @JvmStatic
    @BindingAdapter("visibleInvisible")
    fun setVisible(view: View, visible: Boolean) {
        view.visibility = if (visible) View.VISIBLE else View.INVISIBLE
    }

    @JvmStatic
    @BindingAdapter("visibleGone")
    fun setVisibleGone(view: View, visible: Boolean) {
        view.visibility = if (visible) View.VISIBLE else View.GONE
    }


    @JvmStatic
    @BindingAdapter("fabShow")
    fun setFabVisible(fab: FloatingActionButton, visible: Boolean) {
        when {
            visible -> fab.show()
            else -> fab.hide()
        }
    }

    @JvmStatic
    @BindingAdapter("fabSrc")
    fun setImageRes(fab: FloatingActionButton, drawable: Drawable) {
        fab.setImageDrawable(drawable)
    }

    @JvmStatic
    @BindingAdapter("entries")
    fun setSpinnerStringAdapter(spinner: Spinner, list: List<*>?) {
        if (list != null) {
            val adapter = ArrayAdapter(spinner.context, R.layout.simple_spinner_item, list)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
    }

    @JvmStatic
    @BindingAdapter("textError")
    fun setTextErrorOnEditText(view: TextInputLayout?, errorMessage: String?) {
        view?.error = errorMessage
        view?.isErrorEnabled = errorMessage != null
    }

}