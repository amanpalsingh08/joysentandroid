package com.slider.sliderimage.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.facebook.drawee.backends.pipeline.Fresco
import com.slider.R
import com.slider.databinding.LayoutActivityFullScreenImageBinding
import com.slider.sliderimage.adapters.ViewPagerAdapter

class FullScreenImageActivity : AppCompatActivity() {

    private lateinit var binding : LayoutActivityFullScreenImageBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        Fresco.initialize(applicationContext)
        binding = LayoutActivityFullScreenImageBinding.inflate(layoutInflater)
        setContentView(R.layout.layout_activity_full_screen_image)
        getExtras()
    }

    /**
     * Get extras from external call
     */
    private fun getExtras() {
        val items = intent.extras!!.getStringArray("items")
        val position = intent.extras!!.getInt("position")

        binding.viewPagerFullScreen.adapter = ViewPagerAdapter(context = this, items = items!!.toList())
        binding.viewPagerFullScreen.setCurrentItem(position, true)
        binding.indicatorScreen.setViewPager(binding.viewPagerFullScreen)
    }

    /**
     * On click action from UI
     * @param view
     */
    fun onclickComponents(view: View) {
        Log.d("Library", "${view.id}")
        onBackPressed()
    }


}
