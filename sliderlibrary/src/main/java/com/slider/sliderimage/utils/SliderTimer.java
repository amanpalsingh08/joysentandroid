package com.slider.sliderimage.utils;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;

import androidx.viewpager.widget.ViewPager;

import java.util.TimerTask;

public class SliderTimer extends TimerTask {
    private ViewPager viewPager;
    private int size;
    private Handler handler;
    public SliderTimer(ViewPager viewPager, int size) {
        this.viewPager = viewPager;
        this.size = size;
    }

    @Override
    public void run() {

        handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                if (viewPager.getCurrentItem() < size - 1) {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
                } else {
                    viewPager.setCurrentItem(0, true);
                }
            }
        });
    }
}