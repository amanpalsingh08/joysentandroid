package com.custom.sliderimage.logic

import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.viewpager.widget.ViewPager
import com.slider.R
import com.slider.databinding.LayoutSliderImageBinding
import com.slider.sliderimage.activities.FullScreenImageActivity
import com.slider.sliderimage.adapters.ViewPagerAdapter
import me.relex.circleindicator.CircleIndicator

/**
 * Created by Ivan on 16/08/18.
 */

@SuppressWarnings("all")
class SliderImage : LinearLayout {

    // Array of Image URLs
    private var items: List<String> = listOf()
    private lateinit var binding : LayoutSliderImageBinding

    // Timer schedule
    private var inTimer = false
    private var timeLong = 2000.toLong()

    @JvmOverloads
    constructor(
            context: Context,
            attrs: AttributeSet? = null,
            defStyleAttr: Int = 0)
            : super(context, attrs, defStyleAttr)

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
            context: Context,
            attrs: AttributeSet?,
            defStyleAttr: Int,
            defStyleRes: Int)
            : super(context, attrs, defStyleAttr, defStyleRes)

    init {
        binding = LayoutSliderImageBinding.inflate(LayoutInflater.from(context),this, true)
//        LayoutInflater.from(context).inflate(R.layout.layout_slider_image, this, true)
        orientation = VERTICAL

        binding.viewPagerSlider.adapter = ViewPagerAdapter(context = context, items = items)
        binding.indicator.setViewPager(binding.viewPagerSlider)
    }

    /**
     * Get Circle indicator component
     * @return CircleIndicator
     */
    fun getIndicator(): CircleIndicator {
        return binding.indicator
    }

    /**
     * Get items of view pager (Image URLs)
     * @return list of string
     */
    fun getItems(): List<String> {
        return items
    }

    /**
     * Add new items "Image URLs"
     * @param items
     */
    fun setItems(items: List<String>) {
        (binding.viewPagerSlider.adapter as? ViewPagerAdapter)?.setItemsPager(items)
        binding.viewPagerSlider.adapter?.notifyDataSetChanged()
        this@SliderImage.items = items
        binding.indicator.setViewPager(binding.viewPagerSlider)
    }

    /**
     * Page events of view pager
     * @param onPageScroll ->
     * @param onPageSelected ->
     * @param onPageStateChange ->
     */
    fun onPageListener(onPageScroll: (position: Int, offSet: Float, offSetPixels: Int) -> Unit,
                       onPageSelected: (position: Int) -> Unit,
                       onPageStateChange: (state: Int) -> Unit) {
        val onPageChangeListener = object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                onPageStateChange(state)
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                onPageScroll(position, positionOffset, positionOffsetPixels)
            }

            override fun onPageSelected(position: Int) {
                onPageSelected(position)
            }
        }
        binding.viewPagerSlider.addOnPageChangeListener(onPageChangeListener)
    }

    /**
     * Add timer task for change the current page
     * @param time
     */
    fun addTimerToSlide(time: Long) {
        this.timeLong = time
        this.inTimer = true
        goNextPage()
    }

    /**
     * Remove timer task
     */
    fun removeTimerSlide() {
        this.inTimer = false
    }

    /**
     * Action go to next page of view pager
     */
    private fun goNextPage() {
        Handler().postDelayed({
            if (inTimer) {
                if (binding.viewPagerSlider.currentItem == (items.size-1)) {
                    binding. viewPagerSlider.setCurrentItem(0, true)
                } else {
                    binding.viewPagerSlider.setCurrentItem(binding.viewPagerSlider.currentItem + 1, true)
                }
            }
            goNextPage()
        }, timeLong)
    }

    /**
     * Open full screen activity
     * @param position
     */
    fun openfullScreen(position: Int = 0) {
        val intent = Intent(context, FullScreenImageActivity::class.java)
        intent.putExtra("items", items.toTypedArray())
        intent.putExtra("position", position)
        context.startActivity(intent)
    }

    companion object {

        /**
         * Open full screen activity from external form
         * @param context
         * @param items
         * @param position
         */
        fun openfullScreen(context: Context, items: List<String>, position: Int) {
            val intent = Intent(context, FullScreenImageActivity::class.java)
            intent.putExtra("items", items.toTypedArray())
            intent.putExtra("position", position)
            context.startActivity(intent)
        }
    }

}